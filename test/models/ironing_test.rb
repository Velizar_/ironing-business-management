require 'test_helper'

class IroningTest < ActiveSupport::TestCase
  def setup
    @service1 = FactoryBot.create(:service,
                                   price: 1.0,
                                   name: 'One pound fish')

    @service2 = FactoryBot.create(:service,
                                   price: 2.0,
                                   name: 'Two pound fish')

    Rails.application.load_seed

    @min_charge_svc = Service.minimum_charge
    @min_charge = @min_charge_svc.price
  end

  def teardown; end

  #
  # builds booking_services for an ironing,
  #     calls apply_minimum_charge, and makes asserts
  #
  # total_cost_fraction is the total cost (as fraction of @minimum_charge)
  #     before applying the minimum charge
  #
  # services is an array of pairs of service and count to be constructed
  #
  #
  def minimum_charge_asserts(total_cost_fraction, services)
    # create
    ironing = FactoryBot.create(:ironing)
    services.each do |service, fraction|
      ironing.booking_services.create(service: service,
                                      count: fraction * @min_charge)
    end

    ironing.apply_minimum_charge

    # assign
    total_service_cost = @min_charge * total_cost_fraction
    bs = ironing.booking_services.reload
    min_charge_bs = bs.where(service_id: @min_charge_svc.id)
    normal_services_count =
      services.select { |svc, _| svc != @min_charge_svc }.size
    delta = 1e-8

    # assert
    if total_service_cost < @min_charge
      assert_equal 1, min_charge_bs.count
      assert_in_delta (1 - total_cost_fraction), min_charge_bs[0].count, delta
      assert_in_delta @min_charge, ironing.total_cost, delta
      assert_equal normal_services_count + 1, bs.count
    else
      assert_equal 0, min_charge_bs.count
      assert_in_delta total_service_cost, ironing.total_cost, delta
      assert_equal normal_services_count, bs.count
    end
  end

  context 'apply_minimum_charge' do
    context 'applies charge properly when price is below minimum' do
      should 'case 1' do
        minimum_charge_asserts(0.5, [[@service1, 0.5]])
      end

      should 'case 2' do
        minimum_charge_asserts(
          0.7,
          [
            [@service1, 0.3],
            [@service2, 0.2]
          ]
        )
      end
    end

    context 'applies charge properly when price is at/above minimum' do
      should 'case 1' do
        minimum_charge_asserts(1.0, [[@service1, 1.0]])
      end

      should 'case 2' do
        minimum_charge_asserts(
          1.1,
          [
            [@service1, 0.3],
            [@service2, 0.4]
          ]
        )
      end
    end

    context 'when a minimum charge service is already applied previously' do
      # price below minimum

      should 'case 1' do
        minimum_charge_asserts(
          0.3,
          [
            [@service1, 0.3],
            # total cost here is 0.2 * min_charge
            [@min_charge_svc, 0.2 / @min_charge]
          ]
        )
      end

      # minimum charge above minimum
      #   (this might happen because its value is editable)
      should 'case 2' do
        minimum_charge_asserts(
          0.4,
          [
            [@service2, 0.2],
            [@min_charge_svc, 1.2 / @min_charge]
          ]
        )
      end

      # price at/above minimum

      should 'case 3' do
        minimum_charge_asserts(
          2.6,
          [
            [@service2, 1.3],
            [@min_charge_svc, 0.2 / @min_charge]
          ]
        )
      end

      should 'case 4' do
        minimum_charge_asserts(
          1.3,
          [
            [@service1, 0.9],
            [@service2, 0.2],
            [@min_charge_svc, 0.7 / @min_charge]
          ]
        )
      end
    end
  end
end
