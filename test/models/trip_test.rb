require 'test_helper'

class TripTest < ActiveSupport::TestCase
  def setup
    Rails.application.load_seed # for System address
    @trip = FactoryBot.create(:trip)
  end

  def teardown; end

  test 'get_optimal_order! passes the correct data to sh and returns its output' do
    N = 4
    start_time = Time.current
    @trip.start_time = start_time

    trip_items = (1..N).map do |_|
      FactoryBot.build(:trip_item)
    end

    addresses = mock('addresses')
    pre_adj = mock('adjacency matrix of address distances')
    adj = [
      [0,20,40,10],
      [21,0,50,20],
      [30,70,0,30],
      [11,22,33,0]
    ]
    adj_to_pass = 'java -jar -Xmx320m TSPSolver.jar exact' \
                                 + ' 0,20,40,10/-,-' \
                                 + ' 21,0,50,20/-,-' \
                                 + ' 30,70,0,30/-,-' \
                                 + ' 11,22,33,0/-,-'
    optimal_str = '0,3,1,2'
    optimal = [3, 1, 2]

    trip_items.stubs(:addresses).returns(addresses)
    @trip.stubs(:trip_items).returns(trip_items)
    AddressDistance.stubs(:adjacency_matrix!)
                   .with(addresses)
                   .returns([pre_adj, {}])
    AddressDistance.stubs(:object_matrix_to_travel_times)
                   .with(pre_adj)
                   .returns(adj)
    @trip.stubs(:`).with(adj_to_pass).returns(optimal_str)

    result, = @trip.get_optimal_order!(true)
    assert_equal optimal, result
  end

  test 'Automatically creates the base trip item' do
    assert_equal @trip.trip_items.count, 1
    assert_equal @trip.trip_items.first.address, Address.main_for_base
  end

  test 'fix_sequences ensures that sequences are from 0 to n' do
    (1..5).each { |i| FactoryBot.create(:trip_item_with_booking, trip: @trip, sequence: i % 3) }
    @trip.fix_sequences

    new_sequences = @trip.trip_items.map(&:sequence).to_set
    assert_equal (0..5).to_set, new_sequences
  end

  test 'fix_sequences copes with sequences lower than 0' do
    base_item = @trip.trip_items.first
    next_items = (1..4).map { |i| FactoryBot.create(:trip_item_with_booking, trip: @trip, sequence: i * 2) }
    first_item = FactoryBot.create(:trip_item_with_booking, trip: @trip, sequence: -1)
    expected_ordering = [base_item.id, first_item.id] + next_items.map(&:id)
    @trip.fix_sequences

    new_ordering = @trip.trip_items.sort_by(&:sequence).map(&:id)
    assert_equal expected_ordering, new_ordering
  end

  #  constraint passing not tested yet
  #  test "passes constraints in a correct format"
end
