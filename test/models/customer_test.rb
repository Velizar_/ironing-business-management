require 'test_helper'

class CustomerTest < ActiveSupport::TestCase
  # This test contains DB records because it is necessary
  # Some of the tested methods (search and exists_by_nickname) are SQL queries
  def setup
    # warning: some addresses contain '.' in their names
    @c1 = FactoryBot.create(:customer, nickname: 'foo', active: false)
    @c2 = FactoryBot.create(:customer, nickname: 'bar', email: 'bar@foo.com')
    @c3 = FactoryBot.create(:customer, nickname: 'baz')
    @c4 = FactoryBot.create(:customer, nickname: 'foofoo')
    @c5 = FactoryBot.create(:customer, nickname: 'barBaz')

    @c1.addresses.create(post_code: 'PostCode1', geocode: '.')
    @c1.addresses.create(post_code: 'postcode2', geocode: '.')
    @c2.addresses.create(post_code: 'PosTCodE3', geocode: '.')
  end

  def teardown
    [@c1, @c2, @c3, @c4, @c5].each do |c|
      c.addresses.destroy_all
      c.destroy
    end
  end

  # Tests SQL code, uses the DB.
  context 'search' do
    should 'return all with no parameters' do
      result = Customer.search ''
      assert_equal result, Customer.all
    end

    should 'match nickname' do
      result = Customer.search 'bar'
      assert_same_elements result.to_a, [@c2, @c5]
    end

    should 'match nickname OR email' do
      result = Customer.search 'foo'
      assert_same_elements result.to_a, [@c1, @c2, @c4]
    end

    should "match case-insensitive in an address's post code" do
      result = Customer.search 'postc'
      assert_same_elements result.to_a, [@c1, @c2]
    end
  end

  context 'exists_by_nickname' do
    should 'be true sometimes' do
      assert Customer.exists_by_nickname('foo')
      assert Customer.exists_by_nickname('foofoo')
      assert Customer.exists_by_nickname('baz')
    end

    should 'be false sometimes' do
      assert_not Customer.exists_by_nickname('foe')
      assert_not Customer.exists_by_nickname('foof')
      assert_not Customer.exists_by_nickname('fool')
    end

    should 'be case insensitive' do
      assert Customer.exists_by_nickname('barbaz')
      assert Customer.exists_by_nickname('bARBaz')
      assert Customer.exists_by_nickname('BaZ')
    end
  end
end
