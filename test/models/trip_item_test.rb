require 'test_helper'

class TripItemTest < ActiveSupport::TestCase
  def setup; end

  def teardown; end

  def adj_for_neighboring(distances)
    n = distances.size + 1
    # non-neighboring elements are ridiculously high
    #     because they should not be used
    l = 1_000_000
    adj = Array.new(n) { Array.new(n) { l } }
    distances.each_with_index { |d, i| adj[i][i + 1] = d }
    adj.map do |row|
      row.map do |distance|
        m = mock('address distance')
        m.stubs(:distance_seconds).returns(distance)
        m
      end
    end
  end

  test 'visit_times! follows mock adj matrix' do
    # constants
    start_time = Time.zone.parse('18:00 UTC')
    seconds_between_trip_items = 300
    distances = [220, 303, 62, 540, 670, 1020, 350]
    n = distances.size + 1

    # mocks
    adj = adj_for_neighboring(distances)
    addresses = mock('addresses')
    gathered = mock('gathered')
    trip_item = mock('trip_item')
    trip = mock('trip')

    # stubs
    TripItem.stubs(:addresses)
            .returns(addresses)
    AddressDistance.stubs(:adjacency_matrix!)
                   .with(addresses)
                   .returns([adj, gathered])
    TripItem.stubs(:first)
            .returns(trip_item)
    trip_item.stubs(:trip).returns(trip)
    trip.stubs(:fix_sequences)
    # Note: when it calls .size on n, it returns 8, which is the size of an integer and it also happens to be our n
    # If we change n then we need to also stub .size
    TripItem.stubs(:all)
            .returns(n)
    TripItem.stubs(:seconds_between_trip_items)
            .returns(seconds_between_trip_items)

    # expected responses
    time = start_time + distances[0]
    expected_times = [time]
    distances.drop(1).each do |distance|
      time += distance + seconds_between_trip_items
      expected_times << time
    end

    # assert
    time_actual, gathered_actual = TripItem.visit_times!(start_time)
    assert_equal gathered, gathered_actual
    assert_equal expected_times, time_actual
  end
end
