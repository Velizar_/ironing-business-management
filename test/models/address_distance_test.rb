require 'test_helper'

class AddressDistanceTest < ActiveSupport::TestCase
  def setup; end

  def teardown; end

  # gives the input and expects a set of updates
  test 'gather_distance_matrix saves the correct values' do
    stub_replace(MapsWrapper, :distance_matrix) do |origins, destinations|
      origins.map { |o| destinations.map { |d| [[o, d].hash, [o, d].hash + 1] } }
    end
    # Note: NUMBER_OF_ADDRESSES must be
    #   always > AddressDistance.MAX_ELEMENTS_PER_QUERY
    NUMBER_OF_ADDRESSES = 204
    addresses = (1..NUMBER_OF_ADDRESSES).map do |i|
      address = mock('address')
      address.stubs(geocode: i, id: i)
      address
    end
    # triples of (from, to, distance_mock)
    all_updates_expected = Set.new
    (1..NUMBER_OF_ADDRESSES).each do |i|
      (1..NUMBER_OF_ADDRESSES).each do |j|
        all_updates_expected << [i, j, [i, j].hash * 60]
      end
    end
    all_updates_actual = Set.new
    stub_replace(AddressDistance, :find_or_create_by) do |args|
      i = args[:from_address_id]
      j = args[:to_address_id]
      # m is a mock with stubbed update
      m = Object.new
      m.define_singleton_method(:update) do |update_args|
        d = update_args[:distance_seconds]
        all_updates_actual << [i, j, d]
      end
      m
    end

    AddressDistance.stubs(:sleep)
    AddressDistance.gather(addresses)

    assert_equal NUMBER_OF_ADDRESSES**2, all_updates_actual.size
    assert_equal all_updates_expected, all_updates_actual

    # cleanup
    undo_stub_replace(AddressDistance, :find_or_create_by)
    undo_stub_replace(MapsWrapper, :distance_matrix)
  end

  def addresses_mock(address_ids)
    address_ids.map do |id|
      address = mock('address')
      address.stubs(:id).returns(id)
      address
    end
  end

  # origins and destinations are arrays of address ids
  def mock_where_response(origins, destinations = origins, load = false)
    query_res = origins.uniq.shuffle.product(destinations.uniq).map do |from, to|
      ad = mock('address distance')
      ad.stubs(from_address_id: from, to_address_id: to)
      ad
    end

    if load
      distances = mock('address distances')
      distances.stubs(:load).returns(query_res)
      distances
    else
      query_res
    end
  end

  test 'adjacency_matrix with duplicates' do
    address_ids = [-1, 2, 3, 4, 3, 6, 7, 5]
    addresses = addresses_mock address_ids
    address_distances = mock_where_response(address_ids,
                                            address_ids,
                                            true)
    AddressDistance.stubs(:where)
                   .with(from_address: address_ids,
                         to_address: address_ids)
                   .returns(address_distances)
    adj = AddressDistance.adjacency_matrix(addresses)
    assert_equal address_ids.size**2, adj.flatten.size

    all_addresses_match = true
    adj.each_with_index do |row, i|
      row.each_with_index do |ad, j|
        all_addresses_match &&= ad.from_address_id == address_ids[i]
        all_addresses_match &&= ad.to_address_id   == address_ids[j]
      end
    end
    assert all_addresses_match, 'adj[i][j] must be from i to j'
  end

  test 'adjacency_matrix! returns the outputs of the correct methods' do
    addresses = mock('Addresses')
    result1 = mock('Result 1')
    result2 = mock('Result 2')
    seq = sequence('Gather matrix then get it')
    AddressDistance.expects(:gather_missing_distances)
                   .in_sequence(seq)
                   .with(addresses)
                   .returns(result2)
    AddressDistance.expects(:adjacency_matrix)
                   .in_sequence(seq)
                   .with(addresses, addresses)
                   .returns(result1)
    assert_equal([result1, result2],
                 AddressDistance.adjacency_matrix!(addresses))
  end

  test 'missing_records' do
    address_ids = [-1, 2, 3, 4, 5, 6, 7, 7]
    addresses = addresses_mock address_ids
    uniq_ids = address_ids.uniq
    missing = {
      -1 => [2, 3, 5, 7],
      2  => [5],
      3  => [5],
      4  => [4, 5, 6],
      5  => uniq_ids,
      6  => [4, 5],
      7  => [-1, 3, 5]
    }
    # AddressDistance.where returns non-missing records
    ad_mock = mock('all address distances')
    ad_hash = {}
    uniq_ids.each do |from|
      to_address_ids = uniq_ids - missing[from]
      ad = mock('address distance')
      ad.stubs(:to_address_id)
      ad.stubs(:map).returns(to_address_ids)
      ad.stubs(:size).returns(to_address_ids.size)
      ad_hash[from] = ad
    end
    ad_mock.stubs(:group_by).returns(ad_hash)

    AddressDistance.stubs(:where)
                   .with(from_address_id: uniq_ids,
                         to_address_id: uniq_ids)
                   .returns(ad_mock)

    assert_equal missing, AddressDistance.missing_records(addresses)
  end

  # does not test efficiency (many addresses per request)
  # does not test with >100 records missing
  test 'gather_records gathers everything' do
    address_ids = [-1, 2, 3, 4, 5, 6, 7]
    missing = {
      -1 => [2, 3, 5, 7],
      2  => [5],
      3  => [5],
      4  => [4, 5, 6],
      5  => address_ids,
      6  => [4, 5],
      7  => [-1, 3, 5]
    }
    missing_stubs = missing.to_a.flatten.uniq.map do |id|
      mock_address = mock('address with id ' + id.to_s)
      mock_address.stubs(:id).returns(id)
      mock_address
    end
    # Set([-1, 2], [-1, 3], ..., [7, 3], [7, 5])
    expected_gathered = missing.to_a.map do |origin, destinations|
      [origin].product(destinations)
    end.reduce(&:concat).to_set

    gathered = Set.new
    duplicates_gathered = false
    stub_replace(AddressDistance, :gather) do |src_ids, dst_ids|
      new_gathered = src_ids.product(dst_ids).to_set
      duplicates_gathered = true if gathered.intersect?(new_gathered)
      gathered |= new_gathered
    end

    AddressDistance.gather_records(missing, missing_stubs)
    assert_equal expected_gathered,
                 gathered.map { |src, dst| [src.id, dst.id] }.to_set
    assert !duplicates_gathered, 'duplicates gathered'

    # cleanup
    undo_stub_replace(AddressDistance, :gather)
  end

  test 'object_matrix_to_travel_times' do
    SECONDS_BETWEEN_ADDRESSES = 100
    # adj[x][y] = the two-digit xy number (adj[2][7] = 27)
    # +100 is added unless the first digit is 0
    adj = []
    expected = []
    (0...10).each do |from|
      row_adj = []
      row_exp = []
      (0...10).each do |to|
        dist = from * 10 + to

        m = mock('address distance')
        m.stubs(:distance_seconds).returns(dist)
        row_adj << m

        row_exp << dist
      end
      adj << row_adj

      unless from.zero?
        row_exp = row_exp.map { |d| d + SECONDS_BETWEEN_ADDRESSES }
      end
      expected << row_exp
    end

    TripItem.stubs(:seconds_between_trip_items)
            .returns(SECONDS_BETWEEN_ADDRESSES)

    actual = AddressDistance.object_matrix_to_travel_times(adj)
    assert_equal expected, actual
  end
end
