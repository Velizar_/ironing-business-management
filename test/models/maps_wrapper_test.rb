require 'test_helper'

class MapsWrapperTest < ActiveSupport::TestCase
  def setup; end

  def teardown
    undo_stub_replace(MapsWrapper, :request_radar)
  end

  FakeHttpResponse = Struct.new(:status, :body)

  radar_response = {
    "meta": {
      "code": 200
    },
    "origins": [
      {
        "latitude": 51.311228,
        "longitude": -0.017701
      }
    ],
    "destinations": [
      {
        "latitude": 51.342574,
        "longitude": -1.955654
      }
    ],
    "matrix": [
      [
        {
          "distance": {
            "value": 109096,
            "text": "109.1 km"
          },
          "duration": {
            "value": 80.83333333333333,
            "text": "1 hr 21 mins"
          },
          "originIndex": 0,
          "destinationIndex": 0
        }
      ]
    ]
  }

  test 'distances works with a simple request' do
    stub_replace(MapsWrapper, :request_radar) do |_, _|
      FakeHttpResponse.new('success', radar_response.to_json)
    end

    result = MapsWrapper.distance_matrix(['51.311228,-0.017701'], ['51.342574,-1.955654'])
    matrix = radar_response[:matrix][0][0]
    assert_equal [[[matrix[:duration][:value], matrix[:distance][:value]]]], result
  end

  test 'distances handles HTTP request failure' do
    stub_replace(MapsWrapper, :request_radar) do |_, _|
      FakeHttpResponse.new(nil, 'Lorem ipsum')
    end

    begin
      MapsWrapper.distance_matrix(['1.0,-2.34'], ['2.1,2.5'])
      fail('Should have raised an error')
    rescue DistanceGatheringError => e
      assert_equal 'Lorem ipsum', e.message
    end
  end
end
