require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  def setup
    @addr = FactoryBot.build(:address, post_code: 'PC1')
  end

  def teardown; end

  test 'searchable columns' do
    searchable = %w(address_name post_code notes)
    assert_equal Address.searchable_columns, searchable
  end
end
