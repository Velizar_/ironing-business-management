require 'test_helper'

class ServiceTest < ActiveSupport::TestCase
  def setup
    @date_time = DateTime.current
    DateTime.stubs(:now).returns(@date_time)

    @category = FactoryBot.build(:category)
    @service = FactoryBot.build(:service,
                                 name: 'Service',
                                 price: 1.0,
                                 active: true,
                                 category: @category)
  end

  def teardown; end
  test 'update_price should rename correctly' do
    @service.expects(:update)
            .with(name: "Service (expired at: #{@date_time})",
                  active: false)
            .returns(true)
    Service.any_instance.stubs(:save).returns(true)
    assert_equal @service.update_price(3.5), true
  end

  test 'update_price should create new with correct parameters' do
    @service.stubs(:update).returns(true)
    new_service = mock 'new service', save: true
    Service.expects(:new)
           .with(name: 'Service',
                 price: 2.0,
                 active: true,
                 category: @category)
           .returns(new_service)
    assert_equal @service.update_price(2.0), true
  end
end
