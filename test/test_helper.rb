ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)
require 'factory_bot_rails'
require 'shoulda'

class ActiveSupport::TestCase
  ActiveRecord::Migration.maintain_test_schema!

  ActiveSupport.on_load(:active_support_test_case) do
    include ActiveRecord::TestDatabases
    include ActiveRecord::TestFixtures

    self.fixture_path = "#{Rails.root}/test/fixtures/"
    self.file_fixture_path = fixture_path + "files"
  end

  include FactoryBot::Syntax::Methods

  # Setup all fixtures in test/fixtures/*.yml
  #   for all tests in alphabetical order.
  #
  # Note: You'll currently still have to
  #   declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
#  fixtures :all

  # Add more helper methods to be used by all tests here...
end

# Replace klass's method method_name with method_implementation
def stub_replace(klass, method_name, &method_implementation)
  klass.singleton_class.send(:alias_method,
                             "#{method_name}_mock_backup",
                             method_name)
  klass.define_singleton_method(method_name, method_implementation)
end

def undo_stub_replace(klass, method_name)
  klass.singleton_class.send(:alias_method,
                             method_name,
                             "#{method_name}_mock_backup")
end

require 'mocha/minitest'
