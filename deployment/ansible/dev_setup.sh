#!/bin/bash

# Run this script on Arch Linux to configure your local environment to run the app
# On another Linux distro replace the next line with sudo apt/yum/whatever install ansible
sudo pacman -S ansible --needed
ANSIBLE_CONFIG=ansible_pipelining.cfg ansible-playbook dev_setup.yml -i inventory/local.yaml --connection=local --extra-vars "opt_user_name=$USER"
