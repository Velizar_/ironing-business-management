# Resources which should not be destroyed

resource "aws_s3_bucket" "certificates" {
  bucket = "certificates-${var.environment}"
}

resource "aws_backup_vault" "database_backup_vault" {
  name = "database_backup_vault_${var.environment}"
}
