#!/usr/bin/env bash

set -e

if [ "$#" -lt 2 ]; then
    echo "Invoke with at least two arguments - environment (e.g. staging), and command (e.g. plan)"
    exit 0
fi

ENVIRONMENT=$1
shift
COMMAND=$1
shift

terraform init -backend-config=../environments/$ENVIRONMENT-perm-backend.tfvars -reconfigure

terraform "$COMMAND" -var-file="../environments/$ENVIRONMENT.tfvars" "$@"
