variable "environment" {
  description = "What is the name of the environment (staging/production)"
}

variable "aws_region" {
  description = "AWS region to launch instances in"
  default     = "eu-west-2"
}
