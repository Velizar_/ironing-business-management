terraform {
  required_providers {
    aws = {
      version = "~> 4.58"
    }
  }
  backend "s3" {
    encrypt = true
    bucket  = "ironing-backend-tfstate"
    # key = [provided in environments/*-backend.tfvars]
    region  = "eu-west-2"
  }
  required_version = "1.4.0"
}

provider "aws" {
  region = var.aws_region
}
