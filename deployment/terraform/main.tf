# Infrastructure surrounding the main instance goes in this file

# Built and managed with Packer - see aws-rocky.pkr.hcl
data "aws_ami" "main_ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "tag:Name"
    values = ["Backend ${var.environment}"]
  }
}

# Built with Packer - see ebs-empty-database.pkr.hcl
data "aws_ebs_volume" "database_volume" {
  most_recent = true
  filter {
    name   = "tag:Name"
    values = ["Backend DB ${var.environment}"]
  }
}

# Manually added to AWS
data "aws_route53_zone" "secret_zone" {
  name = var.secret_zone_name
}

### Managed in the permanent_resources folder ###
data "aws_s3_bucket" "certificates" {
  bucket = "certificates-${var.environment}"
}

data "aws_backup_vault" "database_backup_vault" {
  name = "database_backup_vault_${var.environment}"
}
### End pf permanent_resources ###

resource "aws_default_vpc" "default" {
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol         = -1
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_instance" "main_server" {
  ami           = data.aws_ami.main_ami.id
  instance_type = var.instance_size
  availability_zone = data.aws_ebs_volume.database_volume.availability_zone
  iam_instance_profile = aws_iam_instance_profile.main_server_profile.name

  tags = {
    Name     = "Backend-${var.environment}"
    # Make it easier for someone looking in AWS to find out what created the resource
    Provider = "Terraform"
    Filename = "main.tf"
  }
}

resource "aws_volume_attachment" "db_attachment" {
  device_name = "/dev/sdb"
  volume_id   = data.aws_ebs_volume.database_volume.id
  instance_id = aws_instance.main_server.id
}

resource "aws_cloudwatch_log_group" "main_log" {
  name = "Main_log_${var.environment}"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_stream" "main_log_stream" {
  name           = "Main"
  log_group_name = aws_cloudwatch_log_group.main_log.name
}

resource "aws_eip" "main_eip" {
  instance = aws_instance.main_server.id
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.secret_zone.zone_id
  name    = data.aws_route53_zone.secret_zone.name
  type    = "A"
  ttl     = 300
  records = [aws_eip.main_eip.public_ip]
}

resource "aws_backup_plan" "database_backup_plan" {
  name = "database_backup_plan_${var.environment}"

  rule {
    rule_name         = "daily"
    target_vault_name = data.aws_backup_vault.database_backup_vault.name
    schedule          = "cron(0 4 * * ?)"

    lifecycle {
      delete_after = 3
    }
  }

  rule {
    rule_name         = "weekly"
    target_vault_name = data.aws_backup_vault.database_backup_vault.name
    schedule          = "cron(0 7 ? * MON)"

    lifecycle {
      delete_after = 21
    }
  }

  # Every 1st February
  rule {
    rule_name         = "yearly"
    target_vault_name = data.aws_backup_vault.database_backup_vault.name
    schedule          = "cron(0 10 1 2 ?)"

  # Not supported for EBS: https://docs.aws.amazon.com/aws-backup/latest/devguide/whatisbackup.html#features-by-resource
#    lifecycle {
#      cold_storage_after = 1
#    }
  }
}

resource "aws_backup_selection" "db_backup_selection" {
  iam_role_arn = aws_iam_role.backup_role.arn
  name         = "backup_selection_${var.environment}"
  plan_id      = aws_backup_plan.database_backup_plan.id
  resources    = [data.aws_ebs_volume.database_volume.arn]
  count        = var.database_backups_on
}
