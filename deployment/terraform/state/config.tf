terraform {
  required_providers {
    aws = {
      version = "~> 4.28"
    }
  }
  required_version = "1.2.9"
}

provider "aws" {
  region = "eu-west-2"
}
