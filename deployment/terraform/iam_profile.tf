# Everything about permissions goes in this file:
# IAM roles, policies, profiles

resource "aws_iam_role" "main_server_role" {
  name = "main_server_role_${var.environment}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_policy" "allow_writing_to_logs_policy" {
  name        = "main_server_role_policy_${var.environment}"
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "logs:PutLogEvents",
          "logs:DescribeLogStreams"
        ],
        "Resource": [
          "arn:aws:logs:*:*:*"
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "access_to_s3_bucket_policy" {
  name   = "access_to_s3_bucket_policy_${var.environment}"
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "AllObjectActions",
        "Effect": "Allow",
        "Action": [
          "s3:*Object",
          "s3:ListBucket",
          "s3:ListObjectsV2",
        ],
        "Resource": [
          "arn:aws:s3:::certificates-${var.environment}",
          "arn:aws:s3:::certificates-${var.environment}/*",
        ]
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "logs_policy_attachment" {
  role       = aws_iam_role.main_server_role.name
  policy_arn = aws_iam_policy.allow_writing_to_logs_policy.arn
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = aws_iam_role.main_server_role.name
  policy_arn = aws_iam_policy.access_to_s3_bucket_policy.arn
}

resource "aws_iam_instance_profile" "main_server_profile" {
  name = "main_server_profile_${var.environment}"
  role = aws_iam_role.main_server_role.name
}

resource "aws_iam_role" "backup_role" {
  name               = "backup_role_${var.environment}"
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": ["sts:AssumeRole"],
        "Effect": "allow",
        "Principal": {
          "Service": ["backup.amazonaws.com"]
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "backup_role_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup_role.name
}
