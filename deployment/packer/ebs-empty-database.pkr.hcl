# Initialize a database ELB with the minimal data required for the app to work
# If this is for production, you need to manually change some values in the DB: see README.md

packer {
  required_plugins {
    amazon = {
      version = "= 1.2.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "admin_password" {
  type =  string
  sensitive = true
}

variable "env" {
  type = string
}

source "amazon-ebsvolume" "empty-Postgres-database" {
  instance_type = "t3a.small"
  region        = "eu-west-2"
  source_ami_filter {
    filters = {
      name                = "backend-${var.env}"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["self"]
  }
  ssh_username = "rocky"

  ebs_volumes {
    volume_type           = "gp3"
    device_name           = "/dev/sda1"
    delete_on_termination = true
    volume_size           = 10
  }

  ebs_volumes {
    volume_type = "gp3"
    device_name = "/dev/sdb"
    delete_on_termination = false
    tags = {
      Name     = "Backend DB ${var.env}"
      # Make it easier for someone looking in AWS to find out what created the resource
      Provider = "Packer"
      Filename = "ebs-empty-database.pkr.hcl"
    }
    volume_size = 1
  }
}

build {
  sources = [
    "source.amazon-ebsvolume.empty-Postgres-database"
  ]

  provisioner "shell" {
    script = "./create_empty_database.sh"
    environment_vars = ["ADMIN_PASSWORD=${var.admin_password}"]
  }
}
