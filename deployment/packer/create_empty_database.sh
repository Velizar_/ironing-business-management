#!/bin/bash
# Packer runs Bash in a non-login mode so we need to manually load the env variables
set -a
source /etc/environment
set +a
DEV_NAME=$(lsblk | grep disk | grep 1G | awk '{print $1}')
sudo mkfs.ext4 /dev/$DEV_NAME -U 84631459-3465-42ec-86e7-ce3f481311a7
sudo mount /dev/$DEV_NAME /db_volume
# Note: the db_volume and postgres folders are unused
# They exist because Postgres strongly recommends to create them.
sudo mkdir -p /db_volume/postgres/data
sudo chown -R postgres:postgres /db_volume/postgres

cd / # Avoid being in /home/rocky as the postgres user
sudo postgresql-15-setup initdb postgresql-15
sudo systemctl start postgresql-15
sudo -u postgres createuser rocky -s
# The line below creates the role with a password, but that is not necessary when using postgres locally
# sudo -u postgres psql -c "CREATE ROLE \"rocky\" SUPERUSER LOGIN PASSWORD '${POSTGRES_USER_PASSWORD}';"
cd /home/rocky/app || exit 1
export ADMIN_PASSWORD
/home/rocky/.rvm/wrappers/default/rails db:setup
