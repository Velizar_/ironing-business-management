#!/bin/fish

function variable_exists --no-scope-shadowing
    if set -q "$argv[1]"
        echo 'true'
    else
        echo 'false'
    end
end

argparse --min-args 1 --max-args 1 'h/help' 'r/recreate' 's/skip_tests' 'a/use_fresh_ami' -- $argv
set argparse_exit_code $status

if [ $argparse_exit_code != 0 ]; or set -q _flag_help
    echo 'Usage: deploy.fish environment_name [options]'
    echo 'Options:'
    echo '  -h or --help          - print this message and exit'
    echo '  -r or --recreate      - deploy to a new AMI instead of using Ansible to update the instances in-place'
    echo '  -s or --skip_tests    - deploy without running the tests'
    echo "  -a or --use_fresh_ami - create the AMI from Rocky's image instead of building on top of our latest AMI - slower, but starts clean (avoids leftovers) and uses their latest AMI. Best used with recreate."
    echo 'Example: deploy.fish staging --recreate'
    exit $argparse_exit_code
end

set recreate (variable_exists _flag_recreate)
set skip_tests (variable_exists _flag_skip_tests)
set use_fresh_ami (variable_exists _flag_use_fresh_ami)

./deploy.sh $argv[1] $recreate $skip_tests $use_fresh_ami
