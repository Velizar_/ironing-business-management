This is a software for running an ironing company. It is stable and used in production. You can find a live host with synthetic data [here](https://cryptic-reef-6530.herokuapp.com). The username is admin, the password is 123456.

Table of Contents
=================

   * [Company description](#markdown-header-company-description)
   * [Software usage](#markdown-header-software-usage)
      * [Feature locations](#markdown-header-feature-locations)
      * [Detailed page instructions](#markdown-header-detailed-page-instructions)
         * [Shared](#markdown-header-shared)
         * [Customers](#markdown-header-customers)
         * [Bookings](#markdown-header-bookings)
         * [Ironing](#markdown-header-ironing)
         * [Trips](#markdown-header-trips)
         * [Services](#markdown-header-services)
         * [Staff](#markdown-header-staff)
         * [Expenses](#markdown-header-expenses)
         * [Statistics](#markdown-header-statistics)
         * [Driver application](#markdown-header-driver-application)
   * [Technical](#markdown-header-technical)
      * [Deployment](#markdown-header-deployment)
         * [Driver app](#markdown-header-driver-app)
      * [Version control](#markdown-header-version-control)
      * [Architecture](#markdown-header-architecture)
         * [Tests](#markdown-header-tests)
         * [Components](#markdown-header-components)
      * [Style conventions](#markdown-header-style-conventions)
   * [Design decisions](#markdown-header-design-decisions)
      * [Approach](#markdown-header-approach)
      * [Testing](#markdown-header-testing)
      * [Roadmap](#markdown-header-roadmap)

# Company description

An ironing company by this model is responsible for collecting a customer's clothes, ironing or washing them, and then delivering them back. This is the lifetime of a single order:

1. The customer contacts the company to order an ironing
2. The driver goes on a trip and passes through that customer to collect the clothes and later drop them off at the company's building
3. Ironers handle the customer's ironing
4. The driver delivers the clothes to the customer

Steps 2 and 4 are actually the same kind of trip, handling both collections and deliveries.

The way the fee is calculated depends on the payment scheme, which can be either itemized or hourly (chosen by the customer). The itemized scheme provides a table of individual prices for each possible item, for example shirts; the hourly scheme taxes on the time it would take to iron the items. A minimum flat fee might be applied, also washing is always calculated by the number of laundries used. 
The fee is known after step 3, and paid at step 4 (on delivery).

Step 1 is currently done via the phone or email.  

For step 3, there is a login feature for ironers which allows them to enter the data into the system.  

Driver trips (steps 2 and 4) are planned by the administrator with the aid of an optimizer for the order of addresses in an individual vehicle trip.  

The driver uses an Android application to download the data, display it, and navigate with GPS between customers (the application uses Waze to handle navigation).

# Software usage

## Feature locations

This section describes how to use the application to perform the tasks from the section 'company description'. There is usually also a setup step (e.g. a customer must be registered before bookings from them can be saved). For details, see the instructions for the individual page.

### 1. 

Setup: The customer must be registered from the Customers page

Instructions: The order is recorded from Bookings -> New Booking

### 2.

Setup:

  - Before the driver can go on a trip, the trip must be ordered from the Trips page;
  - The driver must have an account registered from Staff;
  - The password will be asked in the Android application.

### 3.
Setup:

  - Ironers must be registered (from Staff);
  - All services offered by the company must be entered (from Services).

Instructions: A registered ironer must log in to enter all the data.

Alternative: An admin can also do that from the Ironing page.

### 4.
Instructions: Booking delivery is done from Bookings -> Book delivery, the rest is identical to 2.

## Detailed page instructions

This section describes in detail all the features which are not obvious how to use. This is a comprehensive guide, every single button and field are considered.

### Shared

There are common themes shared by many of the pages:

- The 'Delete' button does not appear for most items, and is only intended to undo a recent mistake.
- Instead, there is the option to mark as inactive, which is effectively the same as reversible deletion. This is to keep track of historical records.
- Anything marked as inactive will not be listed on most pages, as if it were deleted - inactive customers will not show up on new order, inactive drivers will not show up when selecting a driver for a trip, etc.
- References to an object are usually a clickable link - for example, on the main Bookings page, the date for each booking's collection links to the trip for that date.
- Some fields are "timepickers", for inputting time or duration. The dropdown only lists round times, but you can still manually enter a time like "18:03".
- Occasionally, a feature might be missing, but it might be possible to solve the problem using a combination of the existing features, as long as you make sure there are no side-effects.

### Customers

- Each customer using the company should be registered via New Customer.
- Notes about customer information:
    - Nickname is how a customer is referred to internally in the company - it must be unique and will show up at some places.
    - The discount is automatically applied to every customer's order. You can also manage individual order discounts from Bookings to override.
    - The geocode is important, its usage is documented on the page itself. Having a precise geocode has a number of benefits:
        - The driver will be navigated to the geocode. It might take them a while to find the customer if that was not precise (they will have to search for the address in the vicinity). This is especially important for new drivers, who haven't yet remembered the exact locations of the customers.
        - The calculated distances between addresses is based on geocodes. See Trips to understand why this matters.
    - All the optional fields are just for the administrator's convenience and not required for the application to function.
- Notice and remember the "Add address" button, because occasionally a customer will get another address.
- Use Search to find a specific customer. Search looks not only in the name, but in all other fields, even in the addresses - for example, typing the last few digits of a customer's phone number would still work.
- Customer debts is a system of keeping track of customers which did not pay the driver immediately, e.g. if they promised to pay online (the driver should take a note and show it to the administrator).

### Bookings

(Also referred to as orders)

- Bookings have 4 states: booked, collected, ironed, delivered:
    - New Booking creates a new booking in the state _booked_.
    - Collecting the clothes transfers its state to _collected_.
    - Entering its ironing information transfers it to _ironed_.
    - Delivering it transfers it to _delivered_.
- Discount is valid only for a single booking; it can be overridden from Ironing if you wish to set discount for individual items.
- The "Visit after" and "Visit before" fields enable time windows - used when a customer is not available to visit during the driver's work hours, for example filling in visit after with 20:00 indicates that they are only available after 20:00. The Trips optimizer makes use of that information and will always satisfy all time windows simultaneously.

### Ironing

Information about the ironing in a booking. This documentation is also relevant to the ironers, who can access most of this page.

- "Ready for ironing" lists clothing which are _collected_, but their ironing information is not yet entered.
- "Ironed" lists clothing which are _ironed_, but not yet delivered.
- When entering ironing information, there is an option to add multiple ironers per ironing, in case it was the work of multiple people.
- By the request of an ironing company, the application allows taxation of a different amount of hours of ironing from the one which is paid to the ironers. Hence, those two are separate fields - time ironed is used for the wages, ironing per hour selected from the services is used for taxation.
- Bag count and has hangers are for the driver's information when loading and unloading clothes.
- The "Apply minimum charge" checkbox is in case your company has a minimum fee (recommended if collection and delivery are for free). Uncheck if you want to ignore minimum charge. If you wish to turn the feature off, go to Services and set minimum charge to £0, then it won't matter whether the box is checked or not.
- The service "Extra charge" is used to artificially increase/decrease the fee. The count is equal to the money. If you wish to tax extra £4.25, add Extra charge with count of 4.25; if you wish to add a flat discount of £1.75, add Extra charge with count of -1.75.
- The "Need extra information" items appear when an ironer has entered ironing information, but there is taxation information which needs to be entered by an admin. Currently, the only type of taxation information is the "Ironing per hour" service, as it may differ from "Time ironed", as noted above.
- Discounts can only be modified by the administrator.
- The discount is carried from the booking's discount, but writing a different number overrides it.
- There are three places to enter the discount in total: for a customer, for a booking, and for a booking's individual service; Only the third influences the end price, the first two are used for convenience:
    - if everything in a booking is discounted, instead of entering the same discount for every individual service in a booking, one could enter it for the whole booking for the same effect;
    - if everything for a customer is discounted, instead of entering the same discount for every single booking, one could enter it for the customer for the same effect.
    - A discount for a customer or booking only sets the default price for the layer below; it can always be overridden freely.

### Trips

- Add Vehicle - there is an option to use multiple vehicles in a day, don't forget to choose each driver.
- Start time - this is the time the driver should start the car, after they have finished loading the clothes.
- Driver account - this must be set properly for the Android application to work; it is automatically set to the previous day's driver.
  Note that there is currently a user interface issue with the driver account - the displayed driver is not always what the system has stored, but clicking on "Update trip" stores is; Optimizing the trip also stores it, unless a driver was already stored. You would be ok if you click Update trip or Optimize at least once before the driver starts their application. 
- At Bing Maps - links to a Bing maps with all the customers entered there, in case you want to see a visualization or possibly reorder it manually.
- For print - produces a backup list for the driver, in case the driver application doesn't work or if the driver doesn't use an Android device at all.
- Optimize - reorders the trip to make it as short as possible while always satisfying the time windows. You do not need to read the rest to be able to use it.
    - Tip: while it will always give a short trip, if you have specific extra requirements, consider using time windows to force it to satisfy them.
    - Exact optimization calculates the exact best route of all the possible ones (limited by the precision of the distance information). However, it might be less reliable or slower. It might even fail to compute routes with more than 25 items or with time windows which shape the route too much; early 'visit before' constraints, which force you to visit a customer early, are an exception and do not make calculation difficult.
    - Exact optimization at 18 items or less uses a reliable and faster algorithm which tends to be better than the approximate optimization.
    - Hint: to remember 18, think of age of majority - if your trip is a "minor", it will use the reliable optimization; if it is at 18, it will ALSO pass. At 19 or more, it will not.
    - Approximate optimization could yield a very, very ineffective trip in theoretical situations, but in practice tends to be optimal or within seconds of the optimal. It might also theoretically take a very long time to compute, but in practice tends to be faster and much more reliable than the exact optimizer.
    - Optimization might take a while, especially for larger routes. Your page might even get the timed out error.
    - Be careful when visiting the page while an optimization is in progress. The route gets rearranged when it finishes optimizing, which might be unexpected.
    - If both optimizers fail, contact the programmer who can run the program on a high-end computer.
    - Things which the optimizer CAN NOT do for you:
        - In a multiple-vehicle case, decide which items to assign to which vehicle.
        - Choose a trip date for an item, based on the proximity of the other items on that date.
- Wipe address distances - click to erase all address distance and gather new ones.
    - This only improves the accuracy of the visit times, it does not affect the actual time the driver takes between addresses, which is calculated separately by Waze; Waze doesn't know the distances, only the addresses (more precisely, the geocodes).
    - When a distance is gathered, the distances provider (probably Google Maps) is asked to give the distance for [that trip's start time] on the same day. This might get outdated:
        - Later in the same day, the provider might get better information for the traffic at that time.
        - A lot of the address distances can be months old. The roads might change over time.
    - Use this feature with care, because distance providers have usage limits. Exceeding the limit costs $0.05 per 1000 distances (you need one distance per pair of addresses, so for 19 customers you have 20 locations, including the start point, which is 20 * 20 = 400 pairs, or 400 distances).

### Services

- Service categories are just for your convenience, for example you could offer ironing or washing & ironing for each item type, which suggests making those two as categories. It will work just fine if all services are in the same category, just with longer names to clarify if there was washing.
- The 'System' category contains items with unique behavior.
- Changing the price of a service does NOT affect past orders, even if that order is ironed, but not yet delivered. After changing the price, you can visit the outstanding orders and edit them to use the new price by changing the service from the dropdown.

### Staff

- Staff members must be registered before they can be made ironer or driver (or both).
- A staff member must be appointed as a driver in order to be able to use the driver application.
- Staff members who are ironers can login to access a version of the Ironing page, see that page for more details.
- Changing wage remembers previously earned wages, but makes them invisible. Please make note of any unpaid amounts before changing the wage.
- Be careful with changing wages mid-week - anything entered before the wage change will use the old wage; you can still edit it to use the new wage, but don't forget about the previous point.
- Driver wages are daily, ironer wages are hourly.
- Administrators are currently not implemented.

### Expenses

Description: an interface to enter expense data in order for finance statistics to work.

- The name of the expense (e.g. "car insurance") is entered as a new Expense type.
- Example for periodic expense: If car insurance was £200 every 2 months (or £1200 yearly) for 2015, this is entered as:
    - Expense type: Car insurance
    - Cost per period: 200
    - From: 2015-01-01
    - To: 2016-01-01
    - Period: 2
    - Months
- Anything which is not a periodic expense should be entered as one-off fees.

### Statistics

Description: Usage should be obvious. Includes accountancy (expenses, revenues & profits with breakdown). Try setting period duration to 1 to see what happens.

### Driver application

This documents the use of the driver application.

- After downloading the trip data, selecting a trip item is done by clicking on it in the list.
- Selecting an item and clicking Go launches the Waze navigation towards that item and brings up the notification, which contains useful information about it.
- Clicking 'Next' from the notification leads Waze to the next customer.
- At the last customer, the 'Next' button doesn't work, but Waze has a feature to navigate you home, which should be used instead.
- Clicking an item twice allows you to enter information about it.
    - It is possible to mark a customer as missed.
    - If a delivery is completed, it is possible to record how much they paid in case they did not pay in full.
    - Data entered here is permanently stored on the device and will not be lost even if the application crashes.
- To stop the application, use the Restart or Exit button from the menu.
- If the trip has changed after the application was already started (with Start), the new data can be downloaded by clicking Restart and clicking Start again, note that the stored data (from clicking an item twice) will NOT be lost, even if the changed trip is very different.

# Technical

## Deployment

Environment: Heroku is used with the buildpacks Ruby and Java. This is to provide the needed Ruby version and Java 8.

System requirements: Recommended at least 512 MB RAM. If more is available, the exact optimizer will be able to handle bigger problem sizes.
Steps:
- Set the SECRET_KEY_BASE environment variable to a secret key (generated by 'rake secret').
- Set the DEVISE_JWT_SECRET_KEY environment variable to a secret key (generated by 'rake secret').
- Obtain a Radar key and set the RADAR_API_KEY environment variable to that key. For the dev environment, I recommend setting environment variables in /config/environment_variables.rb or in .bashrc.
- To enable SMS-based account linking via the user app:
  - Set the TWILLIO_ACCOUNT_SID env variable (an account can be obtained from twillio.com).
  - Set the TWILLIO_AUTH_TOKEN env variable.
  - Set the SENDER_PHONE_NUMBER env variable.
  - Set the CUSTOMER_APP_URL env variable, e.g. to 'http://localhost:3001'.
  - Set the VAPID_PUBLIC_KEY env variable, can generate one with WebPush.generate_key.public_key
  - Set the VAPID_PRIVATE_KEY env variable, can generate one with WebPush.generate_key.private_key
- Look at db/seeds.rb and notice any value declared in the beginning which is wrong then run rails db:seed; after the app is set up, log in as an admin to change those values manually.
- (For production only) Create an owner staff account from the REPL. The code should be:
```ruby
  Staff.create(username: 'admin', # can be something else
               first_name: 'Admin',
               password: ...,
               password_confirmation: ...,
               permissions: 1,
               active: true)
```
- Ask the user of that account to change their password from the Staff tab.
- (For staging only) Run rake db:populate to generate synthetic data including an owner account with username 'admin' and password '123456'

### Driver app

- WARNING: Don't forget that the device's location will be sent to the server and displayed on the Trips page.
- [Download the .apk](https://bitbucket.org/Velizar_/driver-navigation/src/70479d7d57f23ffca449bf8b27fde139f215eeff/app/build/outputs/apk/app-debug.apk?at=master) or compile with the compilation instructions from the [Driver app documentation](https://bitbucket.org/Velizar_/driver-navigation/overview).
- Point the home URL to the project root.
- Enter the driver username and password, which are entered/edited from Staff.
- Android version of 5 or above is recommended for the notification enhancements in Android 5.
- Source repository: https://bitbucket.org/Velizar_/driver-navigation/src
Then the app can be used, provided there is a trip on that date.

## Version control

Used with git, remotely hosted on Bitbucket. No branches are used, since this is a single person project. No CI used.

## Architecture

ERD and class diagram:

![alt tag](http://i.imgur.com/m8LtRH0.jpg)

### Tests

High-level tests go in spec/ while unit tests go in test/

spec/features - high-level tests that exercise functionality through its UI
spec/requests - tests that use the app via the endpoints while ignoring the UI

### Components

While the driver application and the ironer login actively interact with the DB, the optimizer acts like a subroutine and doesn't know about its environment.

The optimizer has complicated internals, but only exposes aTSPTW solving given input data.

More documentation for the optimizer, including input and output format, can be found [here](https://bitbucket.org/Velizar_/tsptw/overview).

![alt tag](http://i.imgur.com/BuYcK16.png)

## Style conventions

The following standard style guides are applied to the code:

  - https://github.com/bbatsov/ruby-style-guide
  - https://github.com/bbatsov/rails-style-guide

Deviations:

- "Thin controllers, fat models" is replaced with keeping reusable code in models. If something is used only once, it probably does not deserve to be moved another place. A method in a model cannot exist just to be called once inside a single controller method.
- Code line limitations for methods and the single-responsibility rule are usually not enforced if that means splitting a method into multiple parts, where the inner parts are called only once in the entire codebase. This is strictly worse than making them into inner methods; but the preferred approach is to keep it a single method, because if it would benefit from splitting into simple clearly named parts, then the same effect can be achieved by putting those parts in the same method, separating them with indentation, and putting a comment before each part.
- An exception to both deviations is made for parts which are likely to be reused in the future. Two examples:
    - Ironing.pending is used only once in a controller, but an array of all pending ironings is obviously fundamental to Ironing;
    - AddressDistance.gather_distance_matrix is only a part of AddressDistance.gather, but the query limitations from Google Maps are handled at gather. That handling might be reused.

Note: the controller and model for Statistics are NOT an example of those deviations; it is pending refactoring.

# Design decisions

## Approach

- Aesthetical design for the company-facing interface is unimportant, only ease of use is important.
- The features are based on demand: for example, only daily wage is supported for drivers and hourly for ironers, although alternative wages are likely to occur.
- Every feature is built with the intent to be able to make it optional later, as this project aims to become usable by a broader range of companies.
- The bug database and a detailed list of planned future features are kept in a document (not available).

## Testing

Test coverage is far from complete, instead the main goal is to run each page at least once and check that it behaves correctly, with more emphasis on pages which are used more often. No strict TDD.

Unit tests are written only for more complicated model logic.

Once in a while, a bug slips past the tests, but new features currently give better value for time than more tests.

## Roadmap

- Finish the debt system, which automatically sends customer debts from the driver application to the DB, adds all debts to the next fee, and along with GPS signals sends all debt data and trip item state data to the DB as an extra safety backup.
- Implement a customer application which allows registering, making bookings (including delivery), and reading relevant data.
- Add feature for the algorithm to allow optimization with multiple vehicles, with time limit on each vehicle (solve VRP, or rather, aVRPTW) (augment the local search method to make the slices or holes across tours, for example 3Holes might make 2 holes in tour 1 and 1 hole in tour 2, then potentially place the node from tour 2 at a hole at tour 1).

Longer-term goals, if the project finds the human resources:

- Generalize the project, allowing different kinds of companies (first find specific companies which are interested).
- Support for multiple bases.
- Route optimization with multiple bases, some of which might belong to different companies.
