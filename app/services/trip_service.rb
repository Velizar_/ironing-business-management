class TripService
  # The main purpose of this is to avoid n queries
  def self.index(date)
    trips = Trip.at_date_all(date).includes(trip_items: [
        :visit_constraints,
        address: :customer
    ])
    trips.each(&:fix_sequences)
    trip_address_distances, any_gathered, gathering_error = trips_adj_matrices(trips)

    trip_drivers = []
    visit_times = []
    notifiable = []
    trips.each do |trip|
      # Show inactive driver accounts if relevant
      drivers = DriverAccount.all_active.to_a
      drivers << trip.driver_account if trip.driver_account&.active == false
      trip_drivers << drivers

      # select default option for trip driver
      if trip.driver_account.nil? && !trip.completed
        yesterday_driver = Trip.at_date(date - 1).driver_account
        trip.driver_account = yesterday_driver || drivers.first
      end
      visit_times << trip.trip_items.visit_times!(trip.start_time)[0]
      notifiable << trip.trip_items.notifiable
    end

    [any_gathered, gathering_error, trips, trip_drivers, trip_address_distances, visit_times, notifiable]
  end

  def self.trips_adj_matrices(trips)
    trip_address_distances = trips.map { |_| [] }
    any_gathered = false
    gathering_error = nil
    begin
      trips.each_with_index do |trip, i|
        adj, gathered = AddressDistance.adjacency_matrix!(trip.trip_items.map(&:address))
        trip_address_distances[i] = adj.flatten
        any_gathered ||= gathered.present?
      end
    rescue DistanceGatheringError => e
      gathering_error = e.message
    end
    [trip_address_distances, any_gathered, gathering_error]
  end

  private_class_method :trips_adj_matrices
end
