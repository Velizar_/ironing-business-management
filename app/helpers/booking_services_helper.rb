module BookingServicesHelper
  # example input: 2.5
  # would output: '02:30'
  def hours_float_to_s(hours_float)
    if hours_float.nil?
      ''
    else
      hours = hours_float.truncate
      minutes = ((hours_float - hours) * 60).round(1)
      format '%02d:%02d', hours, minutes
    end
  end
end
