module StaffHelper
  # example input: 210
  # would output: '03:30'
  def minutes_int_to_s(minutes)
    hours_float_to_s(minutes && minutes / 60.0)
  end
end
