module CustomersHelper
  def display_addresses(customer)
    customer.addresses.map(&:display).join(' / ')
  end
end
