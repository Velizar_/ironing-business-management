class Float
  def prettify
    if to_i == self # 5.0 == 5
      to_i
    else
      self
    end
  end
end

class Time
  # For now, this method ignores time zones and uses UTC for everything
  def to_string_localtime(date)
    change(year: date.year, day: date.day, month: date.month)
      .strftime('%H:%M')
#      .localtime.strftime('%H:%M')
  end
end

module ApplicationHelper
  def to_gbp(number)
    number_to_currency(number, unit: '£')
  end

  def yes_no(boolean)
    boolean ? 'Yes' : 'No'
  end

  def trailing_text(text, condition = true)
    if text.present? && condition
      ', ' + text
    else
      ''
    end
  end

  # a submit button with a corresponding glyphicon
  def glyphicon_submit(f)
    action = f.object.persisted? ? 'update' : 'create'
    f.button :button, class: "#{action}-glyphicon"
  end
end
