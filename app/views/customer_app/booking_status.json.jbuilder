json.has_notification_address @has_notification_address

json.requests @requests do |request|
  json.id request.id
  json.dates request.dates
  json.delivery_dates request.delivery_dates
  json.additional_requirements request.additional_requirements
  json.created_at request.created_at.in_time_zone("GB").to_fs(:db)
  json.address request.address.display unless @has_one_active_address
end

geocodes = {}
json.bookings @bookings do |booking|
  pending_trip_item = booking.pending_trip_item

  json.id booking.id
  json.state booking.state.capitalize
  json.collection_date booking.collection_trip_item.trip_date
  json.delivery_date booking.delivery_trip_item&.trip_date
  if booking.request&.delivery_dates&.present?
    json.delivery_dates booking.request.delivery_dates
  end
  if pending_trip_item.present?
    visit_time = pending_trip_item.accurate_visit_time![1]
    if visit_time.present?
      json.visit_time visit_time
      if @current_driver_location&.trip_item_id == pending_trip_item.id
        geocodes[:geocode] = pending_trip_item.address.geocode
        geocodes[:driver_geocode] = @current_driver_location.geocode
      end
    else
      json.sched_deadline pending_trip_item.trip.sched_deadline.to_fs(:time)
    end
  end
  json.total_cost number_to_currency(booking.ironing.total_cost, unit: '£')
  json.cost_breakdown booking.ironing.booking_services do |svc|
    json.name svc.service.name
    json.name 'Added to reach minimum charge' if svc.service.id == Service.minimum_charge.id
    if svc.service_id != Service.minimum_charge.id
      if svc.service_id == Service.ironing_per_hour.id
        json.cost_per_hour number_to_currency(svc.service.price, unit: '£')
        json.hours hours_float_to_s(svc.count)
      else
        json.cost_per_item number_to_currency(svc.service.price, unit: '£')
        json.count svc.count.prettify
      end
    end
    if svc.discount != 0
      json.discount number_to_percentage(svc.discount, { strip_insignificant_zeros: true })
    end
    json.total_cost number_to_currency(svc.total_cost, unit: '£')
  end
  json.address pending_trip_item&.address&.display unless @has_one_active_address
  json.visit_constraints pending_trip_item&.visit_constraints&.first&.display&.capitalize
end

if geocodes.present?
  json.geocode geocodes[:geocode]
  json.driver_geocode geocodes[:driver_geocode]
end
