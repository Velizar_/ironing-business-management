json.available @available # Redundant but it saves work
json.unconfirmed @unconfirmed # Redundant but it saves work
json.not_confirmed_as_available @not_confirmed_as_available
json.unavailable @unavailable
