json.id @trip.id
json.tripItems @trip_data do |ti, b, visit_time|
  json.id           ti.id
  json.geocode      ti.address.geocode
  json.visitAt      visit_time.to_string_localtime(@trip.date)
  json.customerName ti.address.customer.nickname
  json.fullAddress  ti.address.display
  json.totalCost   b.ironing.total_cost
  json.numberOfBags(b.ironing.bag_count || 0)
  json.hasHangers(  b.ironing.has_hangers || false)
  json.notes(       ti.notes) if ti.notes.present?
end
