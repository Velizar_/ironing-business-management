class Customer < ActiveRecord::Base
  enum payment_scheme: [:flat_rate, :itemized]

  CODE_DIGITS = 6
  CODE_RANGE = (10 ** (CODE_DIGITS - 1))...(10 ** CODE_DIGITS)

  has_many :addresses, dependent: :destroy
  has_many :bookings
  accepts_nested_attributes_for :addresses
  has_one :customer_registration, dependent: :destroy
  has_many :push_notification_addresses, dependent: :destroy

  before_save { self.discount = 0.0 if discount.blank? }
  before_save { self.active = true if active.nil? }
  before_save { self.money_owed = 0 if money_owed.blank? }
  before_save { self.email = email&.downcase }

  validates :nickname, presence: true, uniqueness: { case_sensitive: false }
  validates :email, uniqueness: { case_sensitive: false, allow_nil: true, allow_blank: true }
  validates :payment_scheme, presence: true

  scope :all_active, -> { where active: true }
  scope :non_system, -> { where.not(nickname: 'System') }

  devise :database_authenticatable, :jwt_authenticatable,
         jwt_revocation_strategy: JwtDenylist

  def deletable?
    bookings.empty? && nickname != 'System'
  end

  def get_link_url(code, phone_number)
    "#{ENV['CUSTOMER_APP_URL']}/registration/set_password?code=#{code}&phone=#{phone_number.sub('+', '%2b')}"
  end

  def can_generate_new_code?
    last = confirmation_code_generated_at
    return true if last.nil?
    remaining_seconds = (last + 5.minutes - DateTime.now).to_i
    if remaining_seconds <= 0
      true
    else
      remaining_seconds
    end
  end

  def create_and_send_code(phone_number)
    remaining_seconds = can_generate_new_code?
    return remaining_seconds unless remaining_seconds == true
    url = create_code
    client = Twilio::REST::Client.new(ENV['TWILLIO_ACCOUNT_SID'], ENV['TWILLIO_AUTH_TOKEN'])
    client.messages.create(
      from: ENV['SENDER_PHONE_NUMBER'],
      to: phone_number,
      body: "Welcome to our app. To continue linking your account, please visit the following url: #{url}")
    true
  end

  def create_code
    code = SecureRandom.random_number(CODE_RANGE).to_s
    update(confirmation_code: code, confirmation_code_generated_at: DateTime.now)
    get_link_url(code, [phone_mobile, phone_home].find(&:present?))
  end

  def get_phone_number
    if phone_mobile.present?
      phone_mobile
    elsif phone_home.present?
      phone_home
    else
      nil
    end
  end

  def self.find_by_phone_number(phone)
    uk = international = phone
    if phone.starts_with?('0')
      international = '+44' + phone.delete_prefix('0')
    elsif phone.starts_with?('+44')
      uk = '0' + phone.delete_prefix('+44')
    end
    # for non-UK phone numbers, both `uk` and `international` get assigned to `phone`
    Customer.find_by('phone_mobile = ? OR phone_home = ? OR phone_mobile = ? OR phone_home = ?',
                     uk, uk, international, international)
  end

  def self.system
    Customer.find_by nickname: 'System'
  end

  def self.search(keyword)
    if !keyword.blank?
      # customers containing the keyword, a collection for each attribute
      matches = searchable_columns.map { |col| substring(keyword, col) } +
                with_address_col(keyword)
      # merge the collections
      # .uniq prevents multiple inclusion of a customer
      #   that matches multiple criteria
      matches = matches.flatten.uniq
      # convert to ActiveRecord::Relation
      Customer.where(id: matches.map(&:id))
    else
      all
    end
  end

  def self.with_address_col(keyword)
    addresses = Address.searchable_columns.map do |col|
      Address.where("CAST(#{col} AS TEXT) ILIKE ?", "%#{keyword}%")
    end.flatten
    addresses.map(&:customer).uniq
  end

  # Returns all customers for which keyword is substring of their attribute
  # Assumes attribute is safe, only escapes keyword
  def self.substring(keyword, column)
    where("CAST(#{column} AS TEXT) ILIKE ?", "%#{keyword}%")
  end

  # checks if a customer with #{nickname} exists in the DB
  def self.exists_by_nickname(nickname)
    !where('lower(nickname) = ?', nickname.downcase).empty?
  end

  def self.searchable_columns
    %w(nickname email first_name last_name
       phone_home phone_mobile notes)
  end
end
