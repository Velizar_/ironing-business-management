# A single trip location
class TripItem < ActiveRecord::Base
  enum state: [:unmarked, :completed, :missed]
  default_scope { order :sequence }

  attribute :trip_date

  belongs_to :address
  belongs_to :trip
  has_many :visit_constraints

  accepts_nested_attributes_for :visit_constraints, allow_destroy: true

  # alternative to providing a trip (overrides trip if both are provided)
  before_validation :update_trip_with_params

  before_save :remove_empty_constraints
#  before_create { trip&.touch }
#  before_update :update_trip

  scope(:addresses, lambda do
    Address.joins(:trip_items)
           .where(trip_items: { id: all.map(&:id) })
           .order('trip_items.sequence ASC')
  end)

  def remove_empty_constraints
    self.visit_constraints = visit_constraints.select(&:not_blank?)
  end

  def trip_date_needs_update?
    @trip_date.present? && @trip_date != trip&.date
  end

  def trip_date
    @trip_date || trip&.date&.to_s
  end

  def trip_date=(new_date)
    @trip_date = new_date
    self.attribute_will_change!(:trip_id)
  end

  def update_trip_with_params
    return unless trip_date_needs_update?
    self.attribute_will_change!(:trip_id)
    self.trip = Trip.at_date(trip_date)
  end

#  def update_trip
#    if trip_id_changed?
#      Trip.find(trip_id_was).touch
#      Trip.find(trip_id    ).touch
#    else
#      trip.touch if sequence_changed?
#    end
#  end

  def associated_booking
    Booking.where(collection_trip_item_id: id).or(
      Booking.where(delivery_trip_item_id: id)
    ).take
  end

  def collection_or_delivery(booking = associated_booking)
    if id == booking.collection_trip_item_id
      'collection'
    else
      'delivery'
    end
  end

  # Currently doesn't support multiple disjoint availability periods
  def parseable_constraints(start_time)
    visit_constraints.first&.to_parseable_string(start_time) || '-,-'
  end

  # scheduled visit time
  # may trigger distance gathering
  def visit_time!
    trip.trip_items.visit_times!(trip.start_time)[0][sequence - 1] # don't count the base
  end

  # use DriverLocation to estimate the actual time the driver will visit
  # may trigger distance gathering
  # output is an array of two elements: the visit time as Time and formatted as a String
  def accurate_visit_time!
    estimate = [nil, nil]
    if trip.sched_deadline_passed
      ti = trip.trip_items
      visit_times, _, gathering_error = ti.visit_times!(trip.start_time)
      self.reload # Update after fix_sequences!
      return estimate unless gathering_error.nil?
      scheduled_time = visit_times[sequence - 1]
      location = DriverLocation.where(trip: trip).where.not(distance_seconds: nil).last
      if location.present?
        seq_i = ti.find { |item| item.id == location.trip_item_id }.sequence
        if seq_i > sequence
          estimate = [scheduled_time, "Completed (scheduled at #{scheduled_time.to_fs(:time)})"]
        else
          scheduled_time_i = visit_times[seq_i - 1]
          actual_time_i = location.created_at + location.distance_seconds.seconds
          delay = actual_time_i - scheduled_time_i # can be negative
          est = (scheduled_time + delay)
          estimate = [est, est.to_fs(:time)]
        end
      else
        estimate = [scheduled_time, scheduled_time.to_fs(:time)]
      end
    end
    estimate
  end

  # Called on a trip's all TripItems, returns each one's visit_at
  # Skips the 0th TripItem (base)
  # Collects any missing distances
  # Output is [visit_ats array, gathered, gathering_error]
  def self.visit_times!(start_time)
    begin
      first.trip.fix_sequences
      adj, gathered = AddressDistance.adjacency_matrix!(addresses)
      adj = adj.map { |inner| inner.map(&:distance_seconds) }

      time = start_time
      time += adj[0][1] unless adj.size < 2
      times = [time]
      # drop the base (shouldn't be included) and the 1st (already included)
      (2...(all.size)).each do |next_item|
        time += seconds_between_trip_items + adj[next_item - 1][next_item]
        times << time
      end
      [times, gathered, nil]
    rescue DistanceGatheringError => e
      [['00:00'.to_time] * all.size, [], e.message]
    end
  end

  def self.notifiable
    customer_ids = addresses.offset(1).pluck(:customer_id)
    notifiable = PushNotificationAddress.where(customer_id: customer_ids).pluck(:customer_id).to_set
    customer_ids.map do |id|
      notifiable.include?(id)
    end
  end

  # Spend in waiting for, and serving, each customer
  def self.seconds_between_trip_items
    300
  end
end
