# Customer-sent request for a booking
class Request < ActiveRecord::Base
  belongs_to :address
  belongs_to :booking, optional: true

  delegate :customer, to: :address

  before_save do
    self.dates = dates.sort
    self.delivery_dates = delivery_dates.sort
  end

  # Some requests are on a confirmed date and automatically create a booking
  # Check if this request is such
  def can_automatically_book?
    dates.length == 1 &&
      additional_requirements.blank? &&
      BookingAvailability.available_on?(dates.first) &&
      address.customer.addresses.all_active.count == 1 &&
      valid?
  end

  def can_book_delivery?
    can_automatically_book? &&
      delivery_dates&.length == 1 &&
      BookingAvailability.available_on?(delivery_dates.first)
  end

  def save_with_booking(automatically_created: false)
    booking = Booking.create_from_request(self, automatically_created: automatically_created)
    self.booking = booking
    self.active = false
    save
  end
end
