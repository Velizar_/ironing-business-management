require 'net/https'
include ApplicationHelper

class Address < ActiveRecord::Base
  belongs_to :customer
  has_many :trip_items
  has_many :outgoing_distances,
           class_name:  'AddressDistance',
           foreign_key: 'from_address_id'
  has_many :outgoing_addresses,
           through: :outgoing_distances,
           source: :to_address
  has_many :incoming_distances,
           class_name:  'AddressDistance',
           foreign_key: 'to_address_id'
  has_many :incoming_addresses,
           through: :incoming_distances,
           source: :from_address
  has_many :requests, dependent: :destroy

  accepts_nested_attributes_for :trip_items

  validates :post_code, presence: true
  validates :geocode, presence: true

  scope :all_active,    -> { where active: true }
  scope :all_inactive,  -> { where active: false }
  scope :for_customers, -> { where.not customer_id: Customer.system.id }

  after_save do |address|
    if address.saved_change_to_attribute?(:geocode)
      outgoing_distances.destroy_all
      incoming_distances.destroy_all
    end
  end

  def self.for_base
    Customer.system.addresses
  end

  def self.main_for_base
    self.for_base.all_active.first
  end

  def self.searchable_columns
    column_names - %w(id customer_id created_at updated_at active geocode)
  end

  def display
    if address_name.blank?
      post_code
    else
      address_name + ', ' + post_code
    end
  end
end
