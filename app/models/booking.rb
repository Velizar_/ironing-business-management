# An order for a customer
class Booking < ActiveRecord::Base
  belongs_to :customer, counter_cache: true
  belongs_to :collection_trip_item, class_name: 'TripItem', dependent: :destroy
  belongs_to :delivery_trip_item, class_name: 'TripItem', optional: true, dependent: :destroy
  has_one :ironing, dependent: :destroy
  has_one :request

  attr_accessor :automatically_created

  accepts_nested_attributes_for :collection_trip_item
  accepts_nested_attributes_for :delivery_trip_item

  before_save { self.discount = customer.discount if discount.blank? }
  before_create { self.ironing = Ironing.new if ironing.blank? }

  after_create { PushNotificationAddress.notify_customer_new_booking(self) unless automatically_created }

  delegate :ironed?, to: :ironing
  delegate :payment_scheme, to: :customer
  delegate :flat_rate?, to: :customer
  delegate :itemized?, to: :customer

  def initialize(args)
    @automatically_created = false
    super(args)
  end

  def state
    case
    when !collected? then 'booked'
    when !ironed? then 'collected'
    when !delivered? then 'ironed'
    else 'delivered'
    end
  end

  def collected?
    collection_trip_item.trip&.completed || false
  end

  def delivered?
    delivery_trip_item&.trip&.completed || false
  end

  def pending_trip_item
    if !collected?
      collection_trip_item
    elsif !delivered?
      delivery_trip_item
    else # for delivered bookings
      delivery_trip_item
    end
  end

  self.per_page = 20

  def self.create_with_trip_items(collection_date, delivery_date,
                                  address, automatically_created: false)
    trip = Trip.at_date(collection_date)
    trip_item = TripItem.create(address: address, trip: trip)
    del_trip_item = nil
    if delivery_date.present?
      del_trip = Trip.at_date(delivery_date)
      del_trip_item = TripItem.create(address: address, trip: del_trip)
    end
    create(customer: address.customer, collection_trip_item: trip_item,
           delivery_trip_item: del_trip_item, automatically_created: automatically_created)
  end

  def self.create_from_request(req, automatically_created: false)
    delivery_date = req.can_book_delivery? ? req.delivery_dates.first : nil
    create_with_trip_items(req.dates.first, delivery_date, req.address,
                           automatically_created: automatically_created)
  end

  def self.create_from_registration(reg)
    create_with_trip_items(reg.collection_dates.first, reg.delivery_dates.first,
                           reg.customer.addresses.first)
  end

  def self.all_past(date_range)
    joins(delivery_trip_item: :trip)
      .where(trips: { completed: true, date: date_range })
      .order('trips.date DESC, created_at DESC')
  end
end
