# Coordinates at a point in time for a trip
class DriverLocation < ActiveRecord::Base
  default_scope { order :id }

  belongs_to :trip
  belongs_to :trip_item, optional: true

  def geocode = lat + ',' + long

  def self.received(params)
    dl = DriverLocation.new(params)
    # the Android application sends duplicate locations sometimes
    return if dl.duplicate?
    last_distance_gathered = DriverLocation.where.not(distance_seconds: nil).max_by(&:id)&.created_at
    if last_distance_gathered.nil? || (Time.zone.now - last_distance_gathered > 3.minutes)
      dl.distance_seconds = MapsWrapper.distance(dl.geocode, dl.trip_item.address.geocode)
    end
    dl.save
    if dl.distance_seconds.present? && dl.distance_seconds < 300 && !DriverLocation.exists?(trip: dl.trip, trip_item: dl.trip_item, distance_seconds: ..300)
      PushNotificationAddress.notify_customer_driver_nearby(dl.trip_item.address.customer)
    end
  end

  def duplicate?
    other = DriverLocation.last
    return false if other.nil? # if no DriverLocations are in the DB
    (
      lat == other.lat &&
      long == other.long &&
      trip_id == other.trip_id
    )
  end
end
