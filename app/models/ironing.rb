# Ironing-related parts of a booking
class Ironing < ActiveRecord::Base
  belongs_to :booking
  has_many   :booking_services, dependent: :destroy
  has_many   :ironing_tasks

  accepts_nested_attributes_for :booking_services, allow_destroy: true
  accepts_nested_attributes_for :ironing_tasks, allow_destroy: true

  def save_form(params, minimum_charge, ironer_mode)
    self.attributes = params
    save_success = save
    if save_success
      if minimum_charge
        apply_minimum_charge
      else
        remove_minimum_charge
      end

      if ironer_mode && booking.flat_rate?
        # mark as needing_extra_input unless it's already marked or filled
        unless booking_services.exists?(service: Service.ironing_per_hour)
          booking_services.create(service: Service.ironing_per_hour, count: 0.0)
        end
      end
    end
    save_success
  end

  # Total cost of the booking_services
  def total_cost
    booking_services.select { |bs| bs.id.present? }.map(&:total_cost).sum
  end

  # returns true only if was eligible but not applied
  def minimum_charge_skipped?
    mc = Service.minimum_charge
    (total_cost < mc.price &&
     booking_services.where(service: mc).empty?)
  end

  def apply_minimum_charge
    remove_minimum_charge

    min_charge_svc = Service.minimum_charge
    # exclude other minimum charge services
    cost = booking_services.where.not(service_id: min_charge_svc.id)
                           .map(&:total_cost)
                           .sum
    remaining = min_charge_svc.price - cost
    min_svc_count = remaining / min_charge_svc.price
    return unless remaining > 0
    booking_services.create(service: min_charge_svc,
                            count: min_svc_count,
                            discount: 0)
  end

  def remove_minimum_charge
    booking_services.where(service: Service.minimum_charge).destroy_all
  end

  def customer_name
    booking.customer.nickname
  end

  def ironed?
    !date_ironed.nil?
  end

  # query ironings which are not yet delivered
  # returns two arrays of Ironing [ironed, ready_for_ironing]
  # sorted ascending by date of creation
  def self.pending(*incl)
    ti = TripItem.joins(:trip).where(trips: { completed: false }).pluck(:id)
    ironings = Ironing.joins(:booking)
                      .includes(*incl)
                      .where(bookings: { delivery_trip_item_id: (ti + [nil]) })
                      .where.not(bookings: { collection_trip_item_id: ti })
                      .order(:id)
    ironings.partition(&:ironed?)
  end

  # part of ironed are returned as needing_extra_input
  def self.ironed_to_administrative(ironed)
    nei_ids = BookingService.where(ironing_id: ironed.map(&:id),
                                   count: 0,
                                   service_id: Service.ironing_per_hour.id)
                            .pluck(:ironing_id)
                            .uniq
    ironed.partition { |i| nei_ids.include?(i.id) }
  end

  self.per_page = 20

  def self.all_past(date_range)
    joins(booking: { delivery_trip_item: :trip })
      .where(trips: { completed: true, date: date_range })
      .order('trips.date DESC, created_at DESC')
  end
end
