class Concerns
  # returns the indexes of all elements which == el
  def self.indexes_of(array, el)
    array.each_with_index.select { |e| e[0] == el }.map(&:second)
  end

  # Time difference between two Time objects, ignoring their dates
  # Output is in seconds
  def self.time_between(start_time, end_time)
    normalized_start_time = start_time.change(year: end_time.year,
                                              month: end_time.month,
                                              day: end_time.day)
    end_time - normalized_start_time
  end
end
