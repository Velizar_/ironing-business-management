# Services offered by the company
class Service < ActiveRecord::Base
  has_many :booking_services
  belongs_to :category

  validates :name, presence: true, length: { maximum: 300 }
  validates :name, uniqueness: {
    message: 'already exists for that category',
    scope: :category_id,
    case_sensitive: false
  }
  validates :price, presence: true

  scope :all_system, -> { where category: Category.system }
  scope :non_system, -> { where.not category: Category.system }
  scope :all_active, -> { where active: true }

  def system?
    category == Category.system
  end

  # creates a new service with the same name to replace it; renames the old one
  # return value is boolean for success
  def update_price(new_price)
    new_svc = Service.new(name: name,
                          price: new_price,
                          active: active,
                          category: category)
    timestamp_name = name + " (expired at: #{DateTime.current})"

    update(name: timestamp_name, active: false) && new_svc.save
  end

  def deletable?
    booking_services.empty? && !system?
  end

  # special services, should always exist in the DB
  def self.ironing_per_hour
    Service.find_by!(name: 'Ironing per hour')
  end

  def self.missed_driver_fine
    Service.find_by!(name: 'Missed driver fine')
  end

  def self.washing_per_laundry
    Service.find_by!(name: 'Washing per laundry')
  end

  def self.minimum_charge
    Service.find_by!(name: 'Minimum charge')
  end

  def self.extra_charge
    Service.find_by!(name: 'Extra charge')
  end
end
