# A single unit of work by an ironer - a part or a whole ironing
class IroningTask < ActiveRecord::Base
  belongs_to :ironer_account
  belongs_to :ironing

  # In the future, every task might have its own date
  # This is to allow an order to be done throughout multiple days
  delegate :date_ironed, to: :ironing
end
