# A single trip involving deliveries and collections to multiple addresses
# This class is abstract, no code creates Trip instances
class Trip < ActiveRecord::Base
  belongs_to :driver_account, optional: true
  has_many :trip_items
  has_many :driver_locations

  accepts_nested_attributes_for :trip_items

  before_create do
    self.completed = Date.today > date if completed.nil?
    self.start_time = Trip.default_start_time if start_time.blank?
    self.sched_deadline = Trip.default_sched_deadline if sched_deadline.blank?
    # starting address
    trip_items.build sequence: 0, address: Address.main_for_base
  end

  after_save do
    TripNotificationJob.create_or_update(self)
  end

  # destroy base trip item
  before_destroy { trip_items.first.destroy }

  def self.default_sched_deadline
    Time.zone.parse('17:30')
  end

  def self.default_start_time
    Time.zone.parse('18:10')
  end

  def sched_deadline_datetime
    Time.zone.parse("#{date.strftime('%F')} #{sched_deadline.strftime('%T')}")
  end

  def sched_deadline_passed
    Time.zone.now > sched_deadline_datetime
  end

  # may return nil
  def self.today_for_driver(driver_account)
    today_all.select { |trip| trip.driver_account == driver_account }[0]
  end

  # Returns all trips at the date
  # Creates a Trip if there wasn't one at that date
  def self.at_date_all(date)
    trips = where(date: date).order(:created_at)
    if trips.empty?
      Trip.create(date: date)
      trips = where(date: date)
    end
    trips
  end

  # Returns an arbitrary trip at the date
  def self.at_date(date)
    at_date_all(date).first
  end

  def self.today_all
    at_date_all(Time.zone.today)
  end

  def self.today
    today_all.first
  end

  def after(days)
    Trip.at_date(date + days)
  end

  def deletable?
    trip_items.size <= 1
  end

  def had_no_driver?
    changes['driver_account_id'].present? && changes['driver_account_id'][0].nil?
  end

  def accepts_bookings?
    Concerns.time_between(Time.zone.now, sched_deadline_datetime) > 1 * 3600 &&
      trip_items.count <= 20
  end

  # Ensures that all sequences are in order:
  #     no duplicate numbers, missing numbers, or nils.
  def fix_sequences
    base_address_ids = Address.for_base.pluck(:id)
    base_ti = trip_items.select do |ti|
      base_address_ids.include? ti.address_id
    end.first
    ord_ti = [base_ti] + (trip_items.select { |ti| ti.id != base_ti.id }
                                    .sort_by { |ti| ti.sequence || 100 })
    ord_ti.each_with_index do |item, idx|
      item.update sequence: idx unless item.sequence == idx
    end
  end

  # returns the optimal reordering of your trip items: see reorder's input
  # may gather missing distances
  def get_optimal_order!(is_exact)
    ti = trip_items
    adj, gathered = AddressDistance.adjacency_matrix!(ti.addresses)
    adj = AddressDistance.object_matrix_to_travel_times(adj)

    constraints = ti.map { |i| i.parseable_constraints(start_time) }
    solve_type_arg = is_exact ? 'exact' : 'approximate'
    adj_arg = adj.zip(constraints)
                 .map { |dists_row, constr| dists_row.join(',') + '/' + constr }
                 .join(' ')
    solver_args = solve_type_arg + ' ' + adj_arg

    FileUtils.cd Rails.root.to_s
    result = `java -jar -Xmx320m TSPSolver.jar #{solver_args}`
    if result == 'Error: unsatisfiable constraints'
      raise OptimizationError.new('Unsatisfiable constraints')
    end

    order = result.split(',')
                  .map(&:to_i)
                  .drop(1) # drop the 0

    [order, gathered]
  end

  # input: an array defining the new order of trip items
  # length of input must match exactly the number of non-base trip items
  # (base trip item is always 0)
  # example 1: [1, 2, 3, 4]
  # result: no changes
  # example 2: [5, 4, 3, 2, 1]
  # result: would reverse the order of any trip with 5 trip_items
  def reorder(order)
    expected_len = trip_items.length - 1
    actual_len = order.length
    if expected_len != actual_len
      raise ArgumentError, "Received order of length #{actual_len}, " \
                         + "expected #{expected_len}"
    end

    prev_trip_items = trip_items
    TripItem.transaction do
      order.each_with_index do |o, i|
        prev_trip_items[o].update!(sequence: i + 1)
      end
    end
  end

  # may gather missing distances
  def optimize!(is_exact)
    order, gathered = get_optimal_order!(is_exact)
    reorder(order)
    gathered
  end

  def reverse_visit_order
    trip_items.drop(1).reverse.each_with_index do |ti, i|
      ti.update(sequence: i + 1)
    end
  end

  def total_cost(bk = bookings(true))
    trip_items.offset(1).zip(bk).map do |ti, b|
      if ti.collection_or_delivery(b) == 'delivery'
        b.ironing.total_cost
      else
        0.0
      end
    end.sum
  end

  # returns the bookings in the trip
  # bookings[i] is associated with trip_items[i]
  # The purpose of this is to avoid making N queries for N trip item bookings
  def bookings(include_services = false)
    ti_ids = trip_items.offset(1).pluck(:id)
    bt = Booking.arel_table
    # Note: the same booking's two trip items are always in different trips
    bk = Booking.where(bt[:collection_trip_item_id].in(ti_ids)
                .or(bt[:delivery_trip_item_id].in(ti_ids)))
    if include_services
      bk = bk.includes(ironing: { booking_services: :service })
    end
    # sort so that bk[i] is for trip_item[i]
    col_bk = bk.group_by(&:collection_trip_item_id)
    dlv_bk = bk.group_by(&:delivery_trip_item_id)
    ti_ids.map { |ti| (col_bk[ti] || dlv_bk[ti])[0] }
  end

  def notify(trip_item_ids)
    customer_ids = trip_items.offset(1).addresses.pluck(:customer_id)
    visit_times = trip_items.visit_times!(start_time)[0]
    bookings = self.bookings
    trip_items.drop(1).each_with_index do |trip_item, i|
      if trip_item_ids.include?(trip_item.id)
        PushNotificationAddress.notify_customer_scheduled_visit(
          customer_ids[i], visit_times[i], trip_item.collection_or_delivery(bookings[i]))
      end
    end
  end
end
