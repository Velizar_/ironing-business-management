# A category for services - for interface organization purposes
class Category < ActiveRecord::Base
  has_many :services

  validates :name, presence: true, uniqueness: { case_sensitive: false }

  scope :non_system, -> { where.not name: 'System' }

  def deletable?
    services.count.zero? && name != 'System'
  end

  # returns the special System category
  def self.system
    Category.find_by! name: 'System'
  end
end
