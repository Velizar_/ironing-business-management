# Atomic unit for generating profit in a booking
class BookingService < ActiveRecord::Base
  belongs_to :ironing
  belongs_to :service

  validates :count, numericality: true

  # Default values
  before_save { self.discount = ironing.booking.discount if discount.blank? }
  before_save { self.count    = 0                        if count.blank? }

  def total_cost
    service.price * count * (1 - (discount / 100.0))
  end
end
