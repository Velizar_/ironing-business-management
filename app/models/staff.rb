# A staff member, may have one or more occupations (see _account classes)
#   also may hold information about them including login details
class Staff < ActiveRecord::Base
  has_many :ironer_accounts
  has_many :driver_accounts

  validates :username, presence: true, uniqueness: { case_sensitive: false }
  # Staff are typically displayed with first_name + last_name
  # That combination shouldn't be empty
  validates :first_name, presence: true
  validates :password, presence: true, on: :create, confirmation: true
  validates :password_confirmation, presence:
    { if: ->(staff) { staff.password.present? } }

  before_save { self.permissions = 0 unless permissions == 1 }
  before_update :update_accounts_active

  devise :database_authenticatable, :timeoutable, :trackable, timeout_in: 24.hours

  def full_name
    first_name.to_s + ' ' + last_name.to_s
  end

  # Permissions is binary (for now)
  def owner?
    permissions == 1
  end

  def ironer_account
    ironer_accounts.find_by active: true
  end

  def driver_account
    driver_accounts.find_by active: true
  end

  def only_ironer?
    (!owner? && ironer_account&.active)
  end

  def driver?
    driver_account&.active
  end

  def update_accounts_active
    return unless active_changed?
    ironer_account&.update(active: active)
    driver_account&.update(active: active)
  end
end
