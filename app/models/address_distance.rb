require 'net/https'

# Distance between two addresses
class AddressDistance < ActiveRecord::Base
  MAX_ELEMENTS_PER_QUERY = 100
  MAX_PER_DIRECTION = 25
  SECONDS_PER_ELEMENT = 0.004

  belongs_to :from_address, class_name: 'Address'
  belongs_to :to_address, class_name: 'Address'

  validates :from_address_id, uniqueness: { scope: :to_address_id }
  validates :from_address_id, :to_address, :distance_seconds, presence: true

  scope(:for_address_id, lambda do |id|
    ad = AddressDistance.arel_table
    # Note: the same booking's two trip items are always in different trips
    AddressDistance.where(
      ad[:from_address_id].eq(id).or(ad[:to_address_id].eq(id))
    )
  end)

  # Origins and destinations are Address arrays
  # Retrieves and saves a distance matrix
  def self.gather_distance_matrix(srcs, dsts)
    matrix = MapsWrapper.distance_matrix(srcs.map(&:geocode), dsts.map(&:geocode))

    # save response
    matrix.each_with_index do |origin, i|
      origin.each_with_index do |vals, j|
        ad = AddressDistance.find_or_create_by(from_address_id: srcs[i].id,
                                               to_address_id: dsts[j].id)
        minutes, meters = vals
        ad.update(distance_seconds: minutes * 60, meters: meters)
      end
    end
  end

  # Handles the max element/direction constraints
  # Delegates the rest to gather_distance_matrix
  def self.gather(srcs, dsts = srcs.deep_dup)
    # recursive calls don't hit the originating while loop
    while srcs.size > MAX_PER_DIRECTION
      gather(srcs.pop(MAX_PER_DIRECTION), dsts.deep_dup)
    end
    while dsts.size > MAX_PER_DIRECTION
      gather(srcs.deep_dup, dsts.pop(MAX_PER_DIRECTION))
    end

    # e.g. if origins and destinations have sizes [19, 13],
    # they will become [19, 13], [19, 8], [7, 8]; then 7 * 8 < 100
    while srcs.size * dsts.size > MAX_ELEMENTS_PER_QUERY
      if MAX_ELEMENTS_PER_QUERY % srcs.size < MAX_ELEMENTS_PER_QUERY % dsts.size
        gather(srcs.deep_dup, dsts.pop(MAX_ELEMENTS_PER_QUERY / srcs.size))
      else
        gather(srcs.pop(MAX_ELEMENTS_PER_QUERY / dsts.size), dsts.deep_dup)
      end
    end

    gather_distance_matrix(srcs, dsts)

    sleep(srcs.size * dsts.size * SECONDS_PER_ELEMENT)
  end

  # input: origins and destinations are Address enumerables
  # output: array of arrays of AddressDistance or nil
  #         matrix[n][m] is from origins[n] to destinations[m]
  #         if some address distances are not found returns nil
  def self.adjacency_matrix(origins, destinations = origins)
    origin_ids = origins.map(&:id)
    destination_ids = if destinations == origins
                        origin_ids
                      else
                        destinations.map(&:id)
                      end
    n = origin_ids.size
    m = destination_ids.size
    flat_address_distances = AddressDistance.where(
      from_address: origin_ids,
      to_address: destination_ids
    ).load

    matrix = Array.new(n) { Array.new(m) }
    flat_address_distances.each do |ad|
      Concerns.indexes_of(origin_ids, ad.from_address_id).each do |from|
        Concerns.indexes_of(destination_ids, ad.to_address_id).each do |to|
          matrix[from][to] = ad
        end
      end
    end
    matrix
  end

  # like adjacency_matrix(), but also gathers any missing
  #   distances and passes gathered as output
  def self.adjacency_matrix!(origins, destinations = origins)
    all_addresses = if origins == destinations
                      origins
                    else
                      Address.where(id: origins + destinations)
                    end
    gathered = gather_missing_distances all_addresses
    matrix = adjacency_matrix(origins, destinations)
    [matrix, gathered]
  end

  # input: 2D matrix of AddressDistance objects
  #        like the one returned by self.adjacency_matrix
  # output: 2D matrix of floats, takes into account wait time
  def self.object_matrix_to_travel_times(adj)
    adj = adj.map { |o| o.map { |ad| ad.distance_seconds.to_i } }
    [adj.shift] + adj.map do |row|
      row.map do |distance|
        if distance.zero?
          0
        else
          distance + TripItem.seconds_between_trip_items
        end
      end
    end
  end

  # returns a list of each { from_id: [asc_sorted_missing_to_ids] }
  def self.missing_records(addresses)
    missing = {}
    address_ids = addresses.map(&:id).uniq
    total_count = address_ids.size

    all_distances = AddressDistance.where(from_address_id: address_ids,
                                          to_address_id:   address_ids)
                                   .group_by(&:from_address_id)
    address_ids.each do |from|
      present = (all_distances[from] || [])
      if present.size < total_count
        present_ids = present.map(&:to_address_id)
        missing[from] = address_ids - present_ids
      end
    end

    missing
  end

  def self.invert_records(records)
    dsts = records.map(&:second).flatten.uniq
    dsts.map do |dst|
      srcs = records.select { |_k, v| v.include? dst }.keys
      [dst, srcs]
    end.to_h
  end

  # input records is { address_id: [list of missing address_ids] }
  # gathers the missing addresses while trying to use
  #   a small number of requests to the maps API
  # considers all requests which have only one source or destination,
  #   takes the one with the maximum elements, then repeats
  def self.gather_records(records, addresses)
    if records.empty?
      return {}
    end

    inverted_records = invert_records(records)
    max_dsts = records.max_by { |r| r.second.size }
    max_srcs = inverted_records.max_by { |r| r.second.size }
    if max_dsts.second.size > max_srcs.second.size
      to_gather = [[max_dsts.first], max_dsts.second]
      remaining = records.except(max_dsts.first)
    else
      to_gather = [max_srcs.second, [max_srcs.first]]
      remaining = invert_records inverted_records.except(max_srcs.first)
    end

    # key: address_id, value: address
    addresses_hash = addresses.map { |a| [a.id, a] }.to_h
    AddressDistance.gather(
      to_gather.first.map { |src_id| addresses_hash[src_id] },
      to_gather.second.map { |dst_id| addresses_hash[dst_id] }
    )

    gather_records(remaining, addresses)
    to_gather
  end

  def self.gather_missing_distances(addresses)
    gather_records(missing_records(addresses), addresses)
  end
end
