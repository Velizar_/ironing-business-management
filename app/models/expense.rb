# Can be either one-time or periodic
# Periodic happens every #{period} days/months/years
#   in the #{from}, #{to} period
# period = nil in the DB if it is one-time
class Expense < ActiveRecord::Base
  PROFIT = Struct.new('Profit', :date, :amount)

  enum period_type: [:days, :months, :years]

  belongs_to :expense_type

  validates :cost, :from, presence: true
  validate :periodic_spans_multiple_days, :to_is_after_from

  scope :one_time, -> { where period: nil }

  def one_time?
    from == to
  end

  def periodic_spans_multiple_days
    return unless period.present? && from == to
    errors.add(:to, "can't be on the same day as `from` in a periodic expense")
  end

  def to_is_after_from
    return unless to.present? && from > to
    errors.add(:to, "can't be before from")
  end

  def to_profits(start_date = from)
    if one_time?
      [expense_type.name, [PROFIT.new(from, -cost)]]
    else
      # cost distributed evenly in the period
      to ||= Time.zone.today
      current_date = from
      number_of_payments = 0
      while current_date <= to
        number_of_payments += 1
        #               period.[days / months / years]
        current_date += period_type.to_sym.to_proc.call(period)
      end

      total_cost = number_of_payments * cost
      number_of_days = (to - from + 1).to_f
      daily_cost = total_cost / number_of_days
      start_date = [from, start_date].max
      [expense_type.name, (start_date..to).map do |date|
        PROFIT.new(date, -daily_cost)
      end]
    end
  end

  #
  # returns a list of [profit_name, [all_profits]]
  # each profit in all_profits is a PROFIT
  # its date is distinct from the remaining profits of that type
  #
  # Includes not only Expense records, but also
  #   other sources of expenses such as wages
  # Also includes all profits
  #
  def self.profits_in_period(start_date = nil, end_date = Time.zone.today)
    start_date ||= Date.new(1970)

    profits = {
      # expenses (one key per ExpenseType)
      'Driver'  => Hash.new(0.0),
      'Ironers' => Hash.new(0.0),
      'Ironing' => Hash.new(0.0)
    }

    # expenses
    et = Expense.arel_table
    expenses = Expense.where(et[:from].lteq(end_date))
                      .where(et[:to].gteq(start_date).or(et[:to].eq(nil)))
                      .includes(:expense_type)
    expenses.map { |e| e.to_profits(start_date) }.each do |type, exp_profits|
      profits[type] = Hash.new(0.0) unless profits[type]
      exp_profits.each do |profit|
        profits[type][profit.date] += profit.amount
      end
    end

    # driver wages
    driver_payments = Trip.where(date: start_date..end_date)
                          .where.not(driver_account: nil)
                          .includes(:driver_account)
                          .pluck(:date, :pay_rate)

    driver_payments.each do |date, pay_rate|
      profits['Driver'][date] -= pay_rate
    end

    # ironer wages
    ironing_tasks = IroningTask.includes(:ironer_account).joins(:ironing)
                               .where(ironings:
                                        { date_ironed: start_date..end_date })
                               .pluck(:ironing_id,
                                      :date_ironed,
                                      :pay_rate,
                                      :minutes_ironed)

    ironing_tasks.each do |_, date, pay_rate, minutes_ironed|
      profits['Ironers'][date] -= pay_rate * (minutes_ironed / 60.0)
    end
    ironing_ids_in_period = ironing_tasks.map(&:first)

    # ironing profits
    ironing_groups = BookingService.includes(:ironing, :service)
                                   .where(ironing_id: ironing_ids_in_period)
                                   .group_by(&:ironing)
    ironing_groups.each do |ironing, bs|
      profits['Ironing'][ironing.date_ironed] += bs.sum(&:total_cost)
    end

    profits.map do |name, values|
      [name, values.map { |date, amount| PROFIT.new(date, amount) }]
    end.to_h
  end
end
