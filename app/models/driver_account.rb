class DriverAccount < ActiveRecord::Base
  belongs_to :staff

  validates :pay_rate, presence: true

  before_save { self.active = true if active.nil? }

  scope :all_active, -> { where active: true }

  def full_name
    name = staff.full_name

    if active
      name
    elsif old_wage?
      name + ' (old wage)'
    else # staff is inactive or no longer an ironer
      name + ' (inactive)'
    end
  end

  def update_wage(new_wage)
    if pay_rate == new_wage
      false
    else
      update(active: false)
      staff.driver_accounts.create(active: true, pay_rate: new_wage)
    end
  end

  def old_wage?
    !active && staff.driver_account.present?
  end
end
