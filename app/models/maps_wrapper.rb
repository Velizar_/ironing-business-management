require 'net/https'

module MapsWrapper
  # input: an array of geocodes
  # output: a 2D array of [distance_in_minutes, meters]
  def self.distance_matrix(origins, destinations)
    response = request_radar(origins, destinations)
    begin
      json = JSON.parse(response.body)
      if json['meta']['code'] != 200
        raise DistanceGatheringError.new("Status #{json['meta']['code']}, JSON is #{json}")
      else
        json['matrix'].map { |row| row.map { |cell| [cell['duration']['value'], cell['distance']['value']] } }
      end
    rescue JSON::ParserError
      if response.body.present?
        raise DistanceGatheringError.new(response.body)
      else
        raise DistanceGatheringError.new(response['status'])
      end
    end
  end

  def self.request_radar(origins, destinations)
    uri = URI("https://api.radar.io/v1/route/matrix?origins=#{origins.join('|')}&destinations=#{destinations.join('|')}&mode=car&units=metric")
    Net::HTTP.get_response(uri, 'Authorization' => ENV['RADAR_API_KEY'])
  end

  def self.distance(src, dst)
    distance_matrix([src], [dst])[0][0][0] * 60
  end
end
