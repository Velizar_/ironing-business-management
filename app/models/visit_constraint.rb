# Time period which must be satisfied for a trip item
# Has either a start, end, or both
# Examples:
#    after   19:00
#    before  22:00
#    between 20:00 and 23:00
# NOTE: A trip item's visit constraints have an OR relationship
# This means that at least one must be satisfied, not all.
class VisitConstraint < ActiveRecord::Base
  belongs_to :trip_item

  validate :non_overlapping_constraints

  # prevent unsatisfiable constraints
  def non_overlapping_constraints
    return unless visit_after.present? && visit_before.present?
    return unless visit_after > visit_before
    errors.add :visit_after, 'Overlapping constraints'
  end

  def not_blank?
    !(visit_after.nil? && visit_before.nil?)
  end

  # Used to pass to the optimizer
  # Outputs visit_after_token,visit_before_token where each is either
  #   number of minutes represented as a decimal
  #   or '-' to represent no constraint
  # Example output: "3000.0,-"
  def to_parseable_string(start_time)
    [visit_after, visit_before].map do |time|
      if time.nil?
        '-'
      else
        Concerns.time_between(start_time, time).to_i.to_s
      end
    end.join(',')
  end

  def display(leading_comma = false)
    str = ''
    str += ', after '  + visit_after.to_fs(:time) if visit_after
    str += ', before ' + visit_before.to_fs(:time) if visit_before
    str.delete_prefix!(', ') unless leading_comma
    str
  end
end
