class PushNotificationAddress < ApplicationRecord
  belongs_to :customer, optional: true
  belongs_to :staff, optional: true

  validate :has_customer_xor_staff

  def has_customer_xor_staff
    total_present = 0
    total_present += 1 if customer.present?
    total_present += 1 if staff.present?
    if total_present != 1
      error_str = 'Exactly one of customer_id and staff_id must be present'
      errors.add :customer, error_str
      errors.add :staff, error_str
    end
  end

  # options: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification#parameters
  def send_msg(title, options = {})
    begin
      WebPush.payload_send(
        message: {
          title: title
        }.merge(options).to_json,
        endpoint: endpoint,
        p256dh: p256dh_key,
        auth: auth_key,
        vapid: {
          public_key: ENV['VAPID_PUBLIC_KEY'],
          private_key: ENV['VAPID_PRIVATE_KEY']
        })
    rescue WebPush::ExpiredSubscription
      destroy
      nil
    end
  end

  def self.notify_admins_new_booking(request)
    title = "Booking by #{request.customer.nickname}"
    body = "Date: #{request.dates.first}"
    body += ", delivery on: #{request.delivery_dates.join(' | ')}" if request.delivery_dates.present?
    options = {
      body: body,
      icon: 'calendar_glyphicon.png',
      requireInteraction: true,
      data: {
        url: Rails.application.routes.url_helpers.bookings_path
      }
    }
    send_to_owners(title, options)
    Notification.create(text: "#{title}, #{body}")
  end

  def self.notify_admins_new_request(request)
    title = "Booking request by #{request.customer.nickname}"
    options = {
      body: describe_request(request),
      icon: 'calendar_glyphicon.png',
      requireInteraction: true,
      data: {
        url: Rails.application.routes.url_helpers.requests_path
      }
    }
    send_to_owners(title, options)
    Notification.create(text: title + ", " + describe_request(request))
  end

  def self.notify_admins_cancelled_request(request)
    title = "Cancelled request by #{request.customer.nickname}"
    options = {
      body: describe_request(request),
      icon: 'calendar_glyphicon.png',
      requireInteraction: true,
      data: {
        url: Rails.application.routes.url_helpers.requests_path
      }
    }
    send_to_owners(title, options)
    Notification.create(text: title + ", " + describe_request(request))
  end

  def self.notify_admins_cancelled_booking(booking)
    title = "Cancelled booking by #{booking.customer.nickname}"
    body = 'Collection was on ' + booking.collection_trip_item.trip.date.to_fs
    if booking.delivery_trip_item.present?
      body += ', delivery was on ' + booking.delivery_trip_item.trip.date.to_fs
    end
    options = {
      body: body,
      icon: 'calendar_glyphicon.png',
      requireInteraction: true,
      data: {
        url: Rails.application.routes.url_helpers.bookings_path
      }
    }
    send_to_owners(title, options)
    Notification.create(text: title + ", " + body)
  end

  def self.notify_admins_new_registration(reg)
    title = "New registration: #{reg.address}; #{reg.post_code}"
    options = {
      icon: 'user_glyphicon.png',
      requireInteraction: true,
      data: {
        url: Rails.application.routes.url_helpers.customer_registrations_path
      }
    }
    send_to_owners(title, options)
    Notification.create(text: title)
  end

  def self.notify_admin_text_failed(customer, text)
    title = "The system failed to send the text to customer #{customer.nickname}"
    body = "The text was: '#{text}'"
    options = {
      body: body,
      icon: 'user_glyphicon.png',
      requireInteraction: true,
    }
    send_to_owners(title, options)
    Notification.create(text: title + ' | ' + body)
  end

  def self.notify_customer_driver_nearby(customer_id)
    title = 'Your ironing is almost here'
    options = {
      body: 'The driver is approximately 5 minutes away from your address',
      icon: 'iron-web.png',
      requireInteraction: true,
      data: {
        url: '/'
      }
    }
    PushNotificationAddress.where(customer_id: customer_id).each do |addr|
      addr.send_msg(title, options)
    end
  end

  def self.notify_customer_new_booking(booking)
    title = 'New booking created'
    options = {
      body: "Our driver will collect your clothes on #{booking.collection_trip_item.trip_date}",
      icon: 'iron-web.png',
      requireInteraction: true,
      data: {
        url: '/'
      }
    }
    PushNotificationAddress.where(customer_id: booking.customer_id).each do |addr|
      addr.send_msg(title, options)
    end
  end

  def self.notify_customer_new_delivery(booking)
    title = 'Delivery booked'
    options = {
      body: "Our driver will return your clothes on #{booking.delivery_trip_item.trip_date}",
      icon: 'iron-web.png',
      requireInteraction: true,
      data: {
        url: '/'
      }
    }
    PushNotificationAddress.where(customer_id: booking.customer_id).each do |addr|
      addr.send_msg(title, options)
    end
  end

  def self.notify_customer_scheduled_visit(customer_id, visit_time, visit_type)
    title = "Your #{visit_type} is today"
    options = {
      body: "Expect us today at approximately #{visit_time.to_fs(:time)}. Check out the website for a live estimate.",
      icon: 'iron-web.png',
      requireInteraction: true,
      data: {
        url: '/'
      }
    }
    PushNotificationAddress.where(customer_id: customer_id).each do |addr|
      addr.send_msg(title, options)
    end
  end

  private

  def self.send_to_owners(title, options)
    owner_ids = Staff.where(permissions: 1).pluck(:id)
    PushNotificationAddress.where(staff_id: owner_ids).each do |addr|
      addr.send_msg(title, options)
    end
  end

  def self.describe_request(request)
    description = "Dates: #{request.dates.join(' | ')}"
    description += ", delivery on: #{request.delivery_dates.join(' | ')}" if request.delivery_dates.present?
    description += ", Requirements: #{request.additional_requirements}" if request.additional_requirements.present?
    description
  end
end
