class IronerAccount < ActiveRecord::Base
  belongs_to :staff
  has_many :ironing_tasks

  validates :pay_rate, presence: true

  before_save { self.active = true if active.nil? }

  scope :all_active, -> { where active: true }

  def full_name
    name = staff.full_name

    if active
      name
    elsif old_wage?
      name + ' (old wage)'
    else # staff is inactive or no longer an ironer
      name + ' (inactive)'
    end
  end

  def minutes_at(date, tasks = ironing_tasks.includes(:ironing))
    tasks.select { |t| t.date_ironed == date }.map(&:minutes_ironed).sum
  end

  def update_wage(new_wage)
    if pay_rate == new_wage
      false
    else
      update(active: false)
      staff.ironer_accounts.create(active: true, pay_rate: new_wage)
    end
  end

  def old_wage?
    !active && staff.ironer_account.present?
  end
end
