class CustomerRegistration < ActiveRecord::Base
  belongs_to :customer, optional: true
  enum payment_scheme: [:flat_rate, :itemized]

  def make_customer
    c = Customer.new(
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone_mobile: phone_mobile,
      phone_home: phone_home,
      payment_scheme: payment_scheme)
    c.addresses.build(
      address_name: address,
      post_code: post_code)
    c
  end

  def reject!(contact_customer, rejection_reason)
    failed_to_contact = false
    if contact_customer
      begin
        send_text(rejection_reason)
      rescue Exception
        failed_to_contact = true
      end
    end
    self.destroy!
    failed_to_contact
  end

  def complete_registration(customer, auto_book)
    text = "Your registration has been completed. "
    if auto_book
      text += "We will visit you to collect your items on #{collection_dates.first} and return them on #{delivery_dates.first}. "
    end
    text += "To keep track of your booking, you may log in to your account here: #{ENV['CUSTOMER_APP_URL']}"
    recipient = customer.get_phone_number || phone
    update(customer: customer)
    begin
      send_text(text, recipient: recipient)
    rescue Exception
      PushNotificationAddress.notify_admin_text_failed(customer, text)
    end
  end

  def can_automatically_book?
    collection_bookable = collection_dates.present? &&
      BookingAvailability.available_on?(collection_dates.first) == 'available'
    delivery_bookable = delivery_dates.present? &&
      BookingAvailability.available_on?(delivery_dates.first) == 'available'
    collection_bookable && delivery_bookable
  end

  def create_request
    if collection_dates.present?
      Request.create(dates: collection_dates, delivery_dates: delivery_dates,
                     address: customer.addresses.first, additional_requirements: request_requirements)
    else
      nil
    end
  end

  private

  def send_text(message, recipient: phone)
    if recipient.starts_with?('0')
      recipient = '+44' + recipient.delete_prefix('0')
    end
    client = Twilio::REST::Client.new(ENV['TWILLIO_ACCOUNT_SID'], ENV['TWILLIO_AUTH_TOKEN'])
    client.messages.create(
      from: ENV['SENDER_PHONE_NUMBER'],
      to: recipient,
      body: message)
  end
end
