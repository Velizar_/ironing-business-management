# Behind the statistics pages
module Statistics
  def self.growth_over_time(klass, step = 1, start_date = nil)
    data = klass.pluck(:created_at).map(&:to_date).sort
    data = data.drop_while { |d| d < start_date } if start_date
    range = (start_date || data.first)..(Time.zone.today + step)
    range.step(step).map do |date|
      period_data = data.take_while { |d| d <= date }
      data = data.drop(period_data.size)
      [date, period_data.size]
    end.to_h
  end

  def self.count_from_growth(growth)
    running_total = 0
    growth.to_a.sort_by(&:first).map do |date, amount|
      running_total += amount
      [date, running_total.round(2)]
    end.to_h
  end

  def self.count_over_time(klass, step = 1, start_date = nil)
    count_from_growth(growth_over_time(klass, step, start_date))
  end
end
