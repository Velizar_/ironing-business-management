# There exists up to one record per date
# If `available` is true, requests are automatically accepted on that date
# If it is false, no requests can be made
# If there is no record for a given date, that means we don't know yet, so
#   requests are accepted, but not automatically confirmed
class BookingAvailability < ApplicationRecord
  def self.available_on?(date)
    avl = where(date: date)
    trips = Trip.at_date_all(date)
    available?(date, avl, trips)
  end

  # date - check the availability on the date, must be today or future
  # availabilities - must include the record for date, if it exists
  # trips - must include the trip for date, if it exists
  # availabilities and trips are provided for performance reasons
  def self.available?(date, availabilities, trips)
    avl = availabilities.find { |a| a.date == date }
    if avl.nil?
      'unconfirmed'
    elsif avl.available
      trips_at_date = trips.select { |t| t.date == date }.sort_by(&:created_at)
      trip = trips_at_date.first || Trip.create(date: date)
      trip.accepts_bookings? ? 'available' : 'unconfirmed'
    else
      'unavailable'
    end
  end
end
