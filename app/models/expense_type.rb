# A "cause" of expenses, e.g. fuel for the car
class ExpenseType < ActiveRecord::Base
  has_many :expenses

  validates :name, uniqueness: true

  def deletable?
    expenses.empty?
  end
end
