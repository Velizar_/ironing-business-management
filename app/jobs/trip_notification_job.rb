class TripNotificationJob < ActiveJob::Base
  queue_as do
    trip = self.arguments.first
    trip.id.to_s
  end

  def perform(trip)
    addresses = trip.trip_items.addresses
    visit_times, _, _ = trip.trip_items.visit_times!(trip.start_time)
    for i in 1...trip.trip_items.size
      visit_type = trip.trip_items[i].collection_or_delivery
      customer_id = addresses[i].customer_id
      visit_time = visit_times[i - 1]
      PushNotificationAddress.notify_customer_scheduled_visit(customer_id, visit_time, visit_type)
    end
  end

  # would be much simpler with .reschedule_job, but that method does not work
  def self.create_or_update(trip)
    already_exists = false
    GoodJob::Job.where(queue_name: trip.id).each do |job|
      if job.scheduled_at == trip.sched_deadline_datetime
        already_exists = true
      elsif !job.finished?
        job.discard_job('Replace with updated job')
      end
    end
    unless already_exists or trip.sched_deadline_passed
      TripNotificationJob.set(wait_until: trip.sched_deadline_datetime).perform_later(trip)
    end
  end
end
