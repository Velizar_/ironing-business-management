// This file holds all global functions

// Key: controller and method, for example "customers index"
// Value: function to be executed on that method
let onPageReady = {};

function onReady(actionName, f) {
    onPageReady[actionName] = f;
}


$(document).on("turbolinks:load", () => {
    const initializer = onPageReady[$("body").attr("class")];
    if (initializer !== undefined) // some methods lack an initializer
        initializer();
});

// "03:40" to 220
function timeFormatToMinutesInt(timeFormat) {
    const hours = +timeFormat.split(':')[0];
    const minutes = +timeFormat.split(':')[1];
    return hours * 60 + minutes;
}

// "03:40" to 3.66666..
function timeFormatToHoursFloat(timeFormat) {
    return timeFormatToMinutesInt(timeFormat) / 60.0;
}

// For keeping track of hour:minute and displaying it
// Based on Date using composition
// Mutable
class HourMinute {
    // No argument - 00:00
    // String argument - treated as hh:mm
    // Number argument - treated as number of seconds after 00:00
    constructor(time = undefined) {
        this.date = new Date(); // private variable! do not access
        this.date.setHours(0, 0, 0, 0); // 00:00
        switch (typeof time) {
            case 'undefined':
                break;
            case 'string':
                this.addHoursMinutesStr(time);
                break;
            case 'number':
                this.addSeconds(time);
                break;
            default:
                throw "HourMinute: Invalid constructor argument";
        }
    }

    addHoursMinutesStr(hoursMinutes) {
        const [hour, min] = hoursMinutes.split(':');
        this.date.setMinutes(this.date.getMinutes() + (+min));
        this.date.setHours(this.date.getHours() + (+hour));
        return this;
    }

    addSeconds(seconds) {
        this.date.setSeconds(this.date.getSeconds() + (+seconds));
        return this;
    }

    // hh:mm
    display() {
        const pad = (i) => (i < 10 ? '0' : '') + i;
        return pad(this.date.getHours()) + ":" + pad(this.date.getMinutes());
    }
}

function visitConstraintTimepicker(el) {
    el.timepicker({
        timeFormat: 'H:i',
        scrollDefault: '18:30',
        step: 15
    });

    el.on('timeFormatError', () => {
        alert('Invalid time format. Resetting to blank.');
        $(el).val("");
    });
}
