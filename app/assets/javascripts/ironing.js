onReady("ironing new", () => onIroningForm());
onReady("ironing edit", () => onIroningForm());

function makeTimepicker(e) {
    e.timepicker({
        timeFormat: 'H:i',
        step: 5
    });
    e.on('timeFormatError', () => {
        alert('Invalid time format. Resetting to blank.');
        e.val('');
    });
}

function onIroningForm() {
    let i = 0;
    while (true) {
        let field = $(`#ironing_ironing_tasks_attributes_${i}_minutes_ironed`);
        if (field.length === 0)
            break;
        makeTimepicker(field);
        i += 1;
    }
    makeTimepicker($("#ironing_per_hour_field"));

    // is hidden on New but not on Edit
    if ($("#ironing_date_ironed").attr('type') !== 'hidden')
        $("#ironing_date_ironed").datepicker({
            dateFormat: "yy-mm-dd"
        });

    $("#ironing_per_hour_field").on('changeTime', () => {
        $(".ironing_per_hour_float").val(
            timeFormatToHoursFloat($("#ironing_per_hour_field").val()));
    });

    $(".service-select").select2({ width: '100%' });

    $("#extra-ironing-tasks").on('cocoon:after-insert', (e, insertedItem) =>
        makeTimepicker($(".ironing_task_time_ironed:last"))
    );

    $("#extra-booking-services").on('cocoon:after-insert', (e, insertedItem) =>
        $(".service-select:last").select2()
    );
}
