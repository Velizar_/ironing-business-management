onReady("customer_registrations index", () => initializeRejectModal());

function initializeRejectModal() {
    $('#do-contact-customer').on('click', function() {
        $('#rejection-reason').prop('disabled', false)
    })
    $('#do-not-contact-customer').on('click', function() {
        $('#rejection-reason').prop('disabled', true)
    })
}

function openRejectModal(reg) {
    $('#rejectRegistrationModal').modal('show')

    $('#modalLabel').text(`Reject registration for ${reg.first_name} ${reg.last_name} at ${reg.address}, ${reg.post_code}`)
    $('#reject-registration').attr('action', `/customer_registrations/${reg.id}`)
    $('.email-or-text-label').text(reg.phone_mobile ? 'SMS' : 'email')
    $('#do-contact-customer').prop('checked', true)
    $('#rejection-reason').prop('disabled', false)
}

function submitRejectForm() {
    $('#reject-registration').submit()
}
