
//= require jquery3
//= require jquery_ujs
//= require turbolinks

//= require bootstrap-sprockets
//= require jquery-ui
//= require jquery.ui.touch-punch
//= require jquery.timepicker.js

//= require jquery.validate
//= require jquery.validate.additional-methods
//= require cocoon

//= require jquery-readyselector
//= require select2
//= require tablesaw
//= require tablesaw-init

// from app/assets/javascripts/
//= require functions
//= require_tree .
