onReady("trips update", () => onTripsPage());
onReady("trips index", () => onTripsPage());

function initializeVehicleMap(id) {
    window.directionsDisplay = new google.maps.DirectionsRenderer();
    const [lat, lng] = getBaseGeocode(id).split(',')
    const mapOptions = {
        zoom: 12,
        center: {
            lat: +lat,
            lng: +lng
        }
    }
    const map = new google.maps.Map(document.getElementById(id + "-map-canvas"), mapOptions);

    const path = $.makeArray($(`.driver-location-${id}`)).map(x => ({
        lat: $(x).data('lat'),
        lng: $(x).data('lng'),
        createdAt: $(x).data('created')
    }));

    const addresses = $.makeArray($(`.address-${id}`)).map(x => ({
        lat: $(x).data('lat'),
        lng: $(x).data('lng'),
        sequence: $(x).data('sequence'),
        nickname: $(x).data('nickname')
    }));

    displayPath(path, map);
    displayAddresses(addresses, map);
}

function onTripsPage() {
    $("#datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: dateText =>
            window.location.href = '/trips?date=' + dateText
    });

    const addressWaitTime = 300;
    const dist = {};

    const vehicleCount = +$("#vehicle-count").data('value');

    for (let i = 0; i < vehicleCount; i++) {
        // setup timepickers
        const startTimeEl = $(`#${i}_trip_start_time`)
        // schedDeadlineEl might be missing, but then the code still works
        const schedDeadlineEl = $(`#${i}_trip_sched_deadline`)
        startTimeEl.timepicker({
            timeFormat: 'H:i',
            step: 5,
            minTime: schedDeadlineEl.val()
        })
        startTimeEl.on('timeFormatError', () => {
            const defaultStartTime = $("#default-start-time").data('value')
            alert('Invalid time format. Resetting to ' + defaultStartTime)
            $(startTimeEl).val(defaultStartTime)
        })
        schedDeadlineEl.timepicker({
            timeFormat: 'H:i',
            step: 5,
            maxTime: startTimeEl.val()
        })
        schedDeadlineEl.on('timeFormatError', () => {
            const defaultSchedDeadline = $("#default-sched-deadline").data('value')
            alert('Invalid time format. Resetting to ' + defaultSchedDeadline)
            $(schedDeadlineEl).val(defaultSchedDeadline)
        })
        startTimeEl.on('changeTime', () => {
            schedDeadlineEl.timepicker('option', 'maxTime', startTimeEl.val())
        })
        schedDeadlineEl.on('changeTime', () => {
            startTimeEl.timepicker('option', 'minTime', schedDeadlineEl.val())
        })

        const addressDistances = $(`.address-distance-${i}`);
        const addressDistancesExist = addressDistances.length;
        // setup sortables
        addressDistances.each((i, v) => {
            const from = $(v).data('from');
            const to = $(v).data('to');
            if (dist[from] === undefined)
                dist[from] = {};
            dist[from][to] = +$(v).data('dist');
        });

        const baseID = $(`#${i}-base-id`).data('value');
        const listEl = $(`#${i}-trip-sortable`);
        listEl.sortable();
        listEl.on("sortupdate", (el, _) => {
            const id = +el.target.id.split('-')[0];

            const time = new HourMinute($(`#${id}_trip_start_time`).val());
            let previousTripID = baseID;
            $(listEl).children("li").each((idx, v) => {
                $(v).children(".sequence-field").val(idx + 1);
                $(v).children(".sortable-number").html(idx + 1);

                if (addressDistancesExist) {
                    const tripID = $(v).children(".trip-item-address-id").html();
                    if (previousTripID !== baseID)
                        time.addSeconds(addressWaitTime);
                    time.addSeconds(dist[previousTripID][tripID]);
                    $(v).find(".trip-item-display > .visit-at").html(time.display());
                    previousTripID = tripID;
                } else {
                    $(v).find(".trip-item-display > .visit-at").html("[Unknown time]");
                }
            });
            time.addSeconds(addressWaitTime)
            time.addSeconds(dist[previousTripID][baseID])
            $(`#${i}-return-time`).html(time.display())
            updateBingMapsURL(id);
        });
        listEl.trigger('sortupdate');
        initializeVehicleMap(i);
    }
}

function updateBingMapsURL(id) {
    const urlPrefix = "https://www.bing.com/mapspreview?rtp=pos.";
    const geocodes = getGeocodes(id);
    const names = getNicknames(id);
    const mapTokens = geocodes.map((geocode, i) => (geocode.replace(',', '_') + "_" + names[i]));
    const bingMapsURL = urlPrefix + mapTokens.join("~pos.");
    $(`#bing-maps-url-${id}`).attr("href", bingMapsURL);
}

function getGeocodes(id) {
    const geocodes = $.makeArray($(`#${id}-trip-sortable .trip-item-display .geocode`)
                                ).map(x => $(x).data('value'));
    return [getBaseGeocode(id)].concat(geocodes);
}

function getNicknames(id) {
    const nicknames = $.makeArray($(`#${id}-trip-sortable .trip-item-display .nickname`)
                                 ).map(x => $(x).data('value'));
    return ["Home"].concat(nicknames);
}

function getBaseGeocode(id) {
    return $(`#${id}-base-geocode`).data('value');
}

function displayPath(path, map) {
    let prevCoordinate = null;
    let i = 1;
    for (let node of path) {
        const myLatlng = new google.maps.LatLng(node.lat, node.lng);
        const icon_url = '/mm_20_red.png';
        const displayStr = `<div style="min-width:100px">#${i} at ${node.createdAt}</div>`;
        const infoWindow = new google.maps.InfoWindow({
            content: displayStr
        });

        const marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: icon_url,
            html: displayStr
        });

        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.setContent(marker.html);
            infoWindow.open(map, marker);
        });

        if (prevCoordinate !== null) {
            const lineCoordinates = [prevCoordinate, myLatlng];
            const lineSymbol = {
                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                scale: 2
            };

            new google.maps.Polyline({
                path: lineCoordinates,
                icons: [
                    {
                        icon: lineSymbol,
                        offset: '100%'
                    }
                ],
                strokeWeight: 2,
                map: map
            });
        }

        prevCoordinate = myLatlng;
        i += 1;
    }
}

function displayAddresses(addresses, map) {
    for (let node of addresses) {
        const myLatlng = new google.maps.LatLng(node.lat, node.lng);
        const iconUrl = node.nickname === 'System' ? '/blue_MarkerA.png' : '/marker_green.png';
        const sequence = node.sequence;
        const displayStr = `<div style="min-width:115px">#${sequence} ${node.nickname}</div>`;
        const infoWindow = new google.maps.InfoWindow({
            content: displayStr
        });

        const marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: iconUrl,
            html: displayStr
        });

        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.setContent(marker.html);
            infoWindow.open(map, marker);
        });
    }
}

function openNotificationModal(idx) {
    $(`#notificationModal-${idx}`).modal('show')
}

function submitNotificationModal(id) {
    const ids = $.makeArray($(`.notify-for-${id}:checked`)).map(x => x.value)
    fetch(`trips/notify_customers`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        body: JSON.stringify({ trip_item_ids: ids })
    })
    $('.modal').modal('hide')
}

function deselectAll(id) {
    $(`.notify-for-${id}`).each((_i, el) => $(el).prop('checked', ''))
}
