const customerData = {}
const selectStr = "#booking_collection_trip_item_attributes_address_id";

onReady("bookings new", () => {
    for (let el of $.makeArray($(".customer-data"))) {
        customerData[$(el).data('customer-id')] = [$(el).data('address-ids'), +$(el).data('discount')]
    }

    // Set no selections by default
    $("#booking_customer_id, " + selectStr).val(-1);

    // Disable editing until a customer is selected
    $(selectStr + ", #booking_discount").prop('disabled', true);

    // JS plugins
    $("#booking_customer_id").select2({ width: '100%' });
    setSelectedBookingDates($("#requestedDates"), $("#booking_collection_trip_item_attributes_trip_date"));
    const visitElStr = "#booking_collection_trip_item_attributes_visit_constraints_attributes_0_visit_";
    visitConstraintTimepicker($(visitElStr + "after"));
    visitConstraintTimepicker($(visitElStr + "before"));

    // select if parameter was passed
    if (window.customerID)
        $("#booking_customer_id").val(window.customerID).trigger('change');
});

onReady("bookings edit", () => {
    setSelectedBookingDates($("#requestedDates"), $("#booking_collection_trip_item_attributes_trip_date"));
    setSelectedBookingDates($("#requestedDeliveryDates"), $("#booking_delivery_trip_item_attributes_trip_date"));

    const visitElStr = (a, b) =>
        `#booking_${a}_trip_item_attributes_visit_constraints_attributes_0_visit_${b}`;

    visitConstraintTimepicker($(visitElStr('collection', 'after')));
    visitConstraintTimepicker($(visitElStr('collection', 'before')));
    visitConstraintTimepicker($(visitElStr('delivery', 'after')));
    visitConstraintTimepicker($(visitElStr('delivery', 'before')));
});

onReady("bookings delivery", () => {
    setSelectedBookingDates($("#requestedDeliveryDates"), $("#trip_item_trip_date"));

    const visitElStr = "#trip_item_visit_constraints_attributes_0_visit_";
    visitConstraintTimepicker($(visitElStr + "after"));
    visitConstraintTimepicker($(visitElStr + "before"));
});

onReady("bookings past", () => {
    $("#from_date").datepicker({
        dateFormat: "yy-mm-dd"
    });
    $("#to_date").datepicker({
        dateFormat: "yy-mm-dd"
    });
});

// Enable editing and display the customer's information
function customerSelected(customerId) {
    const addressIndexes = customerData[customerId][0];

    // Show customer's addresses, hide the rest
    for (let option of $(selectStr + " option")) {
        if (addressIndexes.includes(+option.value))
            option.style.display = 'initial';
        else
            option.style.display = 'none';
    }

    // Enable editing
    $(selectStr + ", #booking_discount").prop('disabled', false);

    // Set values
    $(selectStr).val(addressIndexes[0]);
    $("#booking_discount").val(customerData[customerId][1]);
}

function setSelectedBookingDates(requestedDatesEl, datepicker) {
    let beforeShowDay = undefined;
    const req = $("#requestedDates");
    if (requestedDatesEl.length !== 0) {
        const requestedDates = requestedDatesEl.data("value");
        beforeShowDay = date => {
            const string = jQuery.datepicker.formatDate('yy-mm-dd', date);
            const isSelected = requestedDates.indexOf(string) !== -1;
            return [true, isSelected ? 'green' : 'yellow']
        }
    }
    datepicker.datepicker({
        dateFormat: "yy-mm-dd",
        beforeShowDay
    });
}
