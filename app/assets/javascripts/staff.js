onReady("staff ironer_weekly_wage", () => {
    $("#go-to-week").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: dateText => {
            window.location.href =
                `/staff/${window.ironer_account_id}/ironer_weekly_wage?date=${dateText}`;
        }
    });
});
