onReady("expenses new", () => onExpensesForm());
onReady("expenses edit", () => onExpensesForm());
onReady("expenses new_periodic", () => onExpensesForm());
onReady("expenses edit_periodic", () => onExpensesForm());

function onExpensesForm() {
    $("#expense_from").datepicker({
        dateFormat: "yy-mm-dd"
    });

    if ($("#expense_to").length !== 0) {
        $("#expense_to").datepicker({
            dateFormat: "yy-mm-dd"
        });

        jQuery.validator.addMethod("notEqual", (value, element, param) => {
            return element.value === "" || value !== $(param).val();
        }, "From and To dates cannot be equal in a periodic expense");

        $("form:first").validate({
            rules: {
                'expense[to]': {
                    notEqual: "#expense_from"
                }
            }
        });
    }
}
