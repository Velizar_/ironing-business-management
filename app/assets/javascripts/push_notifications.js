const promptForPushNotifications = () => {
    if (navigator.serviceWorker) {
        navigator.serviceWorker.register('/service_worker.js').then(async () => {
            const registration = await navigator.serviceWorker.ready
            const subscription = await registration.pushManager.getSubscription()
            if (subscription !== null)
                return
            const permission = await Notification.requestPermission()
            if (permission === 'denied')
                return
            const newSubscription = await registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: window.VAPID_PUBLIC_KEY
            })
            const subObj = JSON.parse(JSON.stringify(newSubscription))
            await fetch('/push_notification_address', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                body: JSON.stringify({
                    endpoint: subObj.endpoint,
                    auth_key: subObj.keys.auth,
                    p256dh_key: subObj.keys.p256dh
                })
            })
        }, err => {
            console.log('Register failed:')
            console.log(err)
        })
    }
}

onReady("requests index", promptForPushNotifications)
onReady("notifications index", promptForPushNotifications)
