onReady("customers new", () => onCustomerForm());
onReady("customers edit", () => onCustomerForm());

// In case create/update failed and the form was re-rendered
onReady("customers create", () => onCustomerForm());
onReady("customers update", () => onCustomerForm());

onReady("customers index", () => onCustomerIndex());
onReady("customers inactive", () => onCustomerIndex());

onReady("addresses new", () => onAddressForm());
onReady("addresses edit", () => onAddressForm());
onReady("addresses edit_active", () => onAddressForm());

onReady("customer_debts increase", () => {
    $("#customer").select2({ width: '100%' });
});

const mapOptions = () => {
    const [lat, lng] = $('#base-geocode').data('value').split(',')
    return {
        zoom: 12,
        center: {
            lat: +lat,
            lng: +lng
        }
    }
}

function phoneIsValidUk(phone, isLandline) {
    let remaining = phone
    if (phone.startsWith('+44'))
        remaining = remaining.slice(3)
    else if (phone.startsWith('0'))
        remaining = remaining.slice(1)
    else
        return false
    if (isLandline && !['1', '2', '3', '5'].includes(remaining[0]))
        return false
    if (!isLandline && remaining[0] !== '7')
        return false
    if (!/^\d+$/.test(remaining))
        return false
    return remaining.length === 10 || (isLandline && remaining.length === 9)
}

function onCustomerForm() {
    const phoneHomeEl = $('#customer_phone_home')
    const phoneHomeELFormGroup = $('div.customer_phone_home')
    const phoneMobileEl = $('#customer_phone_mobile')
    const phoneMobileELFormGroup = $('div.customer_phone_mobile')
    phoneHomeEl.on('input', () => {
        const text = phoneHomeEl.val()
        if (phoneIsValidUk(text, true)) {
            phoneHomeELFormGroup.removeClass('has-error')
        } else {
            phoneHomeELFormGroup.addClass('has-error')
        }
    })
    phoneMobileEl.on('input', () => {
        const text = phoneMobileEl.val()
        if (phoneIsValidUk(text, false)) {
            phoneMobileELFormGroup.removeClass('has-error')
        } else {
            phoneMobileELFormGroup.addClass('has-error')
        }
    })

    $.validator.addMethod('isGeocode', (value, _el) => {
        if (typeof value !== 'string')
            return false
        const parts = value.split(',')
        if (parts.length !== 2)
            return false
        for (let part of parts)
            if (isNaN(part) || isNaN(parseFloat(part)))
                return false
        return true
    }, 'Invalid format, should look like 51.41569,0.02394')
    $.validator.addClassRules('geocode-input', {
        isGeocode: true
    })
    const validateArgs = {
        errorPlacement(error, element) {
            if (element.prop('name') === 'customer[payment_scheme]')
                error.insertAfter(element.parent().parent().parent());
            else
                error.insertAfter(element);
        },
        rules: {},
        messages: {
            "customer[nickname]": {
                remote: 'Nickname already in use.'
            }
        },
        invalidHandler() {
            const prefix = window.prefix.slice(1);
            const postfix = (window.includeID) ? '_geocode' : 'geocode';
            for (let geocodeEl of $(`[id^=${prefix}][id$=${postfix}]`)) {
                if (geocodeEl.value === '') {
                    const idNum = +(geocodeEl.id.slice(prefix.length, postfix.length));
                    if (getPostCode(idNum) !== '') {
                        displayByPostcode(idNum, () => {
                            mapMarkerToGeocode(idNum);
                            // Try to submit again
                            $( "button[type='submit']" ).click();
                        })
                    }
                }
            }
        }
    };

    if ($("#new_customer").length !== 0) {
        validateArgs['rules']['customer[nickname]'] = {
            remote: '/customers/check_unique'
        };
    }

    $("form.customer_form").validate(validateArgs);

    window.prefix = "#customer_addresses_attributes_";
    window.includeID = true;
    initializeAddressMap();
}

function onAddressForm() {
    $("form#new_address").validate({
        invalidHandler() {
            if ($('#address_geocode').val() === '') {
                const postCode = $('#address_post_code').val()
                if (postCode !== '') {
                    geocodeAndDisplayOnMap(postCode, () => {
                        mapMarkerToGeocode('')
                        // Try to submit again
                        $( "button[type='submit']" ).click();
                    })
                }
            }
        }
    })

    window.prefix = "#address_";
    window.includeID = false;
    initializeAddressMap();
}

function initializeAllCustomersMap() {
    window.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions());

    const addresses = $.makeArray($(".addresses-data")).map(x => ({
        geocode: $(x).data('geocode'),
        nickname: $(x).data('nickname')
    }));

    displayCustomerLocations(addresses, window.map);
}

function initializeAddressMap() {
    window.map = new google.maps.Map(document.getElementById('address-map'), mapOptions());
    window.lastMarker = null;
}

function onCustomerIndex() {
    initializeAllCustomersMap();
}

function displayCustomerLocations(addresses, map) {
    for (let node of addresses) {
        const [lat, lng] = node.geocode.split(',');
        const myLatlng = new google.maps.LatLng(lat, lng);

        if (myLatlng !== null) {
            const displayStr = `<div style=\"min-width:90px\">${node.nickname}</div>`;
            const infoWindow = new google.maps.InfoWindow({ content: displayStr });
            const marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                html: displayStr
            });

            google.maps.event.addListener(marker, 'click', () => {
                infoWindow.setContent(marker.html);
                infoWindow.open(map, marker);
            });
        }
    }
}

// returns the prefixes for the HTML ids of all textfields for the address with that id
function getFullPrefix (id) {
    return window.includeID ? window.prefix + id + '_' : window.prefix;
}

function mapMarkerToGeocode(id) {
    if (window.lastMarker === null) {
        alert('Please place a marker first.');
    } else {
        const pos = window.lastMarker.getPosition();
        const latLng = pos.lat() + "," + pos.lng();
        const geocodeEl = getFullPrefix(id) + 'geocode';
        $(geocodeEl).val(latLng);
    }
}

const getPostCode    = (id) => $(getFullPrefix(id) + 'post_code').val();
const getAddressName = (id) => $(getFullPrefix(id) + 'address_name').val();
const getGeocode     = (id) => $(getFullPrefix(id) + 'geocode').val();

function displayByPostcode(id, cbOnSuccess = () => {}) {
    geocodeAndDisplayOnMap(getPostCode(id), cbOnSuccess);
}

function displayByFullAddress(id) {
    const fullAddress = getAddressName(id) + ', ' + getPostCode(id);
    geocodeAndDisplayOnMap(fullAddress);
}

function displayByGeocode(id) {
    const [lat, lng] = getGeocode(id).split(',');
    const myLatlng = new google.maps.LatLng(lat, lng);
    displayLatLng(myLatlng);
}

// display address on map as marker
/* Reason for using Google Maps:
 * - It will be free for our usage levels
 * - MapQuest stopped accepting our key, and it looks like our account disappeared with
 *   no explanation; they appeared to be dying anyway.
 * - Radar seemed bad at geocoding, because it did not place sufficient emphasis on post codes:
 *   for example when I wanted to find Abingdon Road in OX1 4PL, then it would find
 *   the Abingdon Road in London, even with the post code specified.
 */
function geocodeAndDisplayOnMap(address, cbOnSuccess = () => {}) {
    new google.maps.Geocoder().geocode({ 'address': address }, (results, status) => {
        if (status !== google.maps.GeocoderStatus.OK) {
            if (status === 'ZERO_RESULTS') {
                alert('Error: Postcode or address not recognized')
            } else {
                alert("Map operation was unsuccessful for the following reason: " + status + ". "
                    + "Please double-check the post code and address.")
            }
        } else {
            displayLatLng(results[0].geometry.location)
            cbOnSuccess()
        }
    })
}

function displayLatLng(myLatlng) {
    window.map.setCenter(myLatlng);
    const newMarker = new google.maps.Marker({
        position: myLatlng,
        map: window.map,
        draggable: true
    });

    if (window.lastMarker !== null)
        window.lastMarker.setMap(null);
    window.lastMarker = newMarker;
}
