class StaffController < ApplicationController
  before_action :set_staff, only: [:show, :edit, :update]

  # GET /staff
  def index
    @staff = Staff.all.order(id: :desc)

    # either active, or inactive but not a previous wage
    # would show up if the account or its staff was marked as inactive
    @ironers = IronerAccount.all.reject(&:old_wage?).sort_by { |s| -s.id }
    @drivers = DriverAccount.all.reject(&:old_wage?).sort_by { |s| -s.id }
  end

  # GET /staff/1
  def show
  end

  # GET /staff/new
  def new
    @staff = Staff.new
    @password_placeholder = ''
  end

  # GET /staff/1/edit
  def edit
    @password_placeholder = 'Leave empty to keep'
  end

  # POST /staff
  def create
    @staff = Staff.new(staff_params)

    if @staff.save
      redirect_to @staff, notice: 'Staff was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /staff/1
  def update
    if @staff.update(staff_params)
      redirect_to @staff, notice: 'Staff was successfully updated.'
    else
      render :edit
    end
  end

  # GET staff/appoint/ironer
  def appoint_index
    staff = Staff.where active: true
    if params[:role] == 'ironer'
      @all_staff = staff.select { |s| s.ironer_account.nil? }
      @staff = IronerAccount.new
    elsif params[:role] == 'driver'
      @all_staff = staff.select { |s| s.driver_account.nil? }
      @staff = DriverAccount.new
    else
      return # error
    end
    render 'appoint'
  end

  # POST staff/appoint
  def appoint
    if params[:ironer_account]
      @acc = IronerAccount.new(
        staff_profile_accounts(:ironer_account)
      )
    elsif params[:driver_account]
      @acc = DriverAccount.new(
        staff_profile_accounts(:driver_account)
      )
    else
      return # error
    end

    return unless @acc.save
    redirect_to staff_index_path, notice: 'Staff was successfully appointed.'
  end

  # GET staff/edit_wage/1/ironer
  def edit_wage
    @acc = account
  end

  # GET staff/update_wage/1/ironer
  def update_wage
    acc = account
    new_wage = (
      params[:driver_account] ||
      params[:ironer_account]
    )[:pay_rate].to_f

    if acc.update_wage(new_wage)
      redirect_to staff_index_path, notice: 'Wage successfully updated'
    else
      redirect_to staff_index_path, alert:  'Wage not changed'
    end
  end

  def ironer_weekly_wage
    @date = Date.parse params[:date]
    @ironer_account = IronerAccount.find(params[:ironer_id])
    other_accounts = @ironer_account.staff.ironer_accounts.sort_by(&:created_at)
    idx = other_accounts.index { |acc| acc.id == @ironer_account.id }
    @prev_ironer_account = other_accounts[idx - 1] if idx > 0
    @next_ironer_account = other_accounts[idx + 1] if idx < (other_accounts.length - 1)

    @minutes = (0...7).map { |i| @ironer_account.minutes_at(@date + i) }
    @wages = @minutes.map { |min| (min / 60.0) * @ironer_account.pay_rate }
    # for best floating point precision
    @weekly_wage = (@minutes.sum / 60.0) * @ironer_account.pay_rate
  end

  # PATCH /staff/toggle_active/1/ironer
  def toggle_active
    acc = account

    if !acc.staff.active && !acc.active
      redirect_to staff_index_path,
                  alert: 'Error: please set staff account to active first'
    else
      acc.update(active: !acc.active)
      redirect_to staff_index_path,
                  notice: 'Activeness was successfully updated.'
    end
  end

  private

  def account
    case params[:account_type]
    when 'Ironer'
      IronerAccount.find(params[:id])
    when 'Driver'
      DriverAccount.find(params[:id])
    else
      nil
    end
  end

  def set_staff
    @staff = Staff.find(params[:id])
  end

  def staff_params
    params.require(:staff).permit(:username, :password, :password_confirmation,
                                  :active, :email, :phone_number,
                                  :first_name, :last_name)
  end

  def staff_profile_accounts(profile_type)
    params.require(profile_type).permit(:staff_id, :pay_rate)
  end
end
