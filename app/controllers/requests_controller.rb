class RequestsController < ApplicationController
  # GET /requests
  def index
    if params[:show_past] == 'Yes'
      page = (params[:page]&.to_i || 1)
      @requests = Request.where(active: false).page(page).order(id: :desc)
    else
      @requests = @active_requests.order(id: :desc)
    end
  end

  # PUT /requests/toggle_active/1
  def toggle_active
    request = Request.find(params[:id])
    request.update(active: !request.active)
    redirect_to requests_path,
                notice: 'Request was successfully updated.'
  end

  # GET /requests/add_booking_availability
  def add_booking_availability
    @trip_item_counts = []
    @request_counts = []
    start_date = params[:date]&.to_date || Date.today
    end_date = start_date + 13
    @booking_avl = BookingAvailability.where(date: start_date..end_date).to_a
    trip_ids = Trip.where(date: start_date..end_date).pluck(:id)
    trip_items = TripItem.where(trip_id: trip_ids).where.not(address_id: Address.for_base.pluck(:id))
    requests = Request.where(active: true)
    (start_date..end_date).each do |date|
      @trip_item_counts << trip_items.select { |ti| ti.trip.date == date }.size
      @request_counts << requests.select { |req| req.dates.include? date }.size
      if @booking_avl.none? { |b| b.date == date }
        @booking_avl << BookingAvailability.new(date: date)
      end
    end
    @booking_avl = @booking_avl.sort_by(&:date)
  end

  # PUT /requests/save_booking_availability
  def save_booking_availability
    availabilities = params[:booking_availabilities][:booking_availability].values
    availabilities.each do |param|
      booking_avl = BookingAvailability.find_or_initialize_by(date: param['date'])
      if param['available'] == ''
        booking_avl&.destroy
      else
        booking_avl.available = param['available']
        booking_avl.save
      end
    end
    min_date = availabilities.map { |a| a[:date] }.min
    redirect_to add_availability_path(date: min_date), notice: 'Days successfully saved'
  end
end
