class CustomerRegistrationsController < ApplicationController
  # GET /customer_registrations
  def index
    if params[:show_past] == 'Yes'
      page = (params[:page]&.to_i || 1)
      @customer_registrations = CustomerRegistration.where.not(customer_id: nil).page(page).order(id: :desc)
    else
      @customer_registrations = CustomerRegistration.where(customer_id: nil)
    end
  end

  def destroy
    registration = CustomerRegistration.find(params[:id])
    if registration.reject!(params[:contact_customer] == 'yes', params[:rejection_reason])
      redirect_to customer_registrations_path,
                  alert: 'Registration was successfully deleted, but the system failed to contact the customer. ' +
                    "Please contact them manually - phone: #{registration.phone_mobile}, email: #{registration.email}"
    else
      redirect_to customer_registrations_path,
                  notice: 'Registration was successfully deleted.'
    end
  end
end
