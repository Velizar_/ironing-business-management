class ExpenseTypesController < ApplicationController
  before_action :set_expense_type, only: [:edit, :update, :destroy]

  # GET /expense_types
  def index
    @expense_types = ExpenseType.includes(:expenses).all.order(id: :desc)
  end

  # GET /expense_types/new
  def new
    @expense_type = ExpenseType.new
  end

  # GET /expense_types/1/edit
  def edit
  end

  # POST /expense_types
  def create
    @expense_type = ExpenseType.new(expense_type_params)

    if @expense_type.save
      redirect_to expense_types_path,
                  notice: 'Expense type was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /expense_types/1
  def update
    if @expense_type.update(expense_type_params)
      redirect_to expense_types_path,
                  notice: 'Expense type was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /expense_types/1
  def destroy
    return unless @expense_type.deletable?
    @expense_type.destroy
    redirect_to expense_types_url,
                notice: 'Expense type was successfully deleted.'
  end

  private

  def set_expense_type
    @expense_type = ExpenseType.find(params[:id])
  end

  def expense_type_params
    params.require(:expense_type).permit(:name)
  end
end
