class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy, :create_code]

  # GET /customers
  def index
    all_customers = Customer.search(params[:search])
                            .non_system
                            .all_active
    customers_list_pages(all_customers)
    @base_address = Address.main_for_base
  end

  # GET /customers/inactive
  def inactive
    all_customers = Customer.search(params[:search])
                            .non_system
                            .where(active: false)
    customers_list_pages(all_customers)
    render action: 'index'
  end

  def update_customers_per_page
    new_value = params[:customers_per_page]
    current_staff.update(customers_per_page: params[:customers_per_page])
    redirect_to customers_url,
                notice: "Now displaying #{new_value} customers per page"
  end

  # GET /customers/1
  def show
  end

  # GET /customers/new
  def new
    if params[:registration_id].present?
      @registration = CustomerRegistration.find(params[:registration_id])
      @customer = @registration.make_customer
    else
      @customer = Customer.new
      @address = @customer.addresses.build
    end
    @base_address = Address.main_for_base
  end

  # GET /customers/1/edit
  def edit
    @base_address = Address.main_for_base
  end

  # POST /customers
  def create
    @customer = Customer.new(customer_params)
    reg_id = params[:customer][:registration_id]
    if reg_id.present?
      reg = CustomerRegistration.find(reg_id)
      @customer.encrypted_password = reg.encrypted_password
    end
    if @customer.addresses.present? && @customer.save
      if reg_id.present?
        reg.complete_registration(@customer, params[:auto_book])
        if params[:auto_book] == 'yes'
          Booking.create_from_registration(reg)
          redirect_to @customer, notice: 'Customer was successfully created with a booking.'
        else
          req = reg.create_request
          if req.nil?
            redirect_to @customer, notice: 'Customer was successfully created without a booking.'
          else
            redirect_to new_booking_path(request_id: req.id),
                        notice: 'Customer was successfully created, please finish booking.'
          end
        end
      else
        redirect_to @customer, notice: 'Customer was successfully created.'
      end
    else
      c = @customer
      params[:registration_id] = params[:customer][:registration_id]
      new
      @customer = c
      render action: 'new'
    end
  end

  # PATCH/PUT /customers/1
  def update
    if @customer.nickname == 'System'
      redirect_to customers_url, alert: 'Customer not editable'
    elsif @customer.update(customer_params)
      redirect_to @customer, notice: 'Customer was successfully updated.'
    else
      edit
      render action: 'edit'
    end
  end

  # GET /customers/check_unique.json?customer[nickname]=myName
  def check_unique
    respond_to do |format|
      format.json do
        render plain: (
          !Customer.exists_by_nickname(params[:customer][:nickname])
        ).to_s
      end
    end
  end

  def create_code
    url = @customer.create_code
    render html: "<h1><a href='#{url}'>#{url}</a></h1>".html_safe, layout: true
  end

  # DELETE /customer/1
  def destroy
    return unless @customer.deletable?
    @customer.destroy!
    redirect_to customers_url, notice: "Customer #{@customer.nickname} was successfully deleted."
  end

  private

  def set_customer
    @customer = Customer.find(params[:id])
  end

  def customer_params
    params.require(:customer).permit(
      :nickname, :payment_scheme, :discount,
      :email, :active, :first_name, :last_name,
      :phone_home, :phone_mobile, :notes,
      addresses_attributes:
        [:id, :address_name, :post_code, :notes, :active, :geocode]
    )
  end

  # for pages which render index
  def customers_list_pages(customers_query)
    page = (params[:page]&.to_i || 1)
    all_customers = customers_query.order(:nickname).includes(:addresses)
    @addresses = all_customers.flat_map(&:addresses).select(&:active)
    Customer.per_page = current_staff.customers_per_page
    @customers = all_customers.page(page)
  end
end
