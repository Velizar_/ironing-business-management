class BookingsController < ApplicationController
  before_action :set_booking, only: [:delivery, :new_delivery,
                                     :edit, :update, :destroy]

  # GET /bookings
  def index
    @bookings = all_undelivered_bookings.order(id: :desc)
  end

  # GET /bookings/past
  def past
    page = (params[:page]&.to_i || 1)
    from = (params[:from_date]&.to_date || '2000-01-01'.to_date)
    to   = (params[:to_date]&.to_date || Time.zone.today + 40.years)
    @bookings = Booking.all_past(from..to)
                       .page(page)
                       .includes(:customer, :ironing,
                                 delivery_trip_item:   :trip,
                                 collection_trip_item: :trip)
    render action: 'index'
  end

  # GET /bookings/new
  def new
    trip_item = TripItem.new(visit_constraints: [VisitConstraint.new])
    @booking = Booking.new(collection_trip_item: trip_item)
    @customers = Customer.non_system.all_active.includes(:addresses)
    @addresses = @customers.map(&:addresses).flatten.select(&:active)

    @request = Request.find_by(id: params[:request_id]) # optional param
    if @request
      @selected = @request.address.customer_id
    elsif params[:customer]
      @selected = Customer.find_by(nickname: params[:customer])&.id
    end
  end

  # GET /bookings/1/edit
  def edit
    @addresses = @booking.customer.addresses.all_active
    ensure_constraint_fields @booking.collection_trip_item
    ensure_constraint_fields @booking.delivery_trip_item
    @request = @booking.request
  end

  # POST /bookings
  def create
    @booking = Booking.new(booking_with_collection_trip_items_params)
    if @booking.save
      if params[:request_id]
        Request.find_by(id: params[:request_id])&.update(booking_id: @booking.id, active: false)
      end
      redirect_to bookings_path, notice: 'Booking successfully created'
    else
      # The user loses their input in the unlikely event that this happens
      redirect_to new_booking_path(@booking),
                  alert: 'Failed to create booking, reason: ' + @booking.errors.full_messages.to_s
    end
  end

  # PUT/PATCH /bookings/1
  def update
    if @booking.update booking_trip_items_params
      redirect_to bookings_path, notice: 'Booking was successfully updated.'
    else
      # The user loses their input in the unlikely event that this happens
      redirect_to edit_booking_path(@booking),
                  alert: 'Failed to update booking, reason: ' + @booking.errors.full_messages.to_s
    end
  end

  # GET /bookings/delivery/1
  def delivery
    @delivery_trip_item = TripItem.new(visit_constraints: [VisitConstraint.new])
    @addresses = @booking.customer.addresses.all_active
    @request = @booking.request
  end

  # POST /bookings/new_delivery/1
  def new_delivery
    if @booking.delivery_trip_item.present?
      redirect_to bookings_path,
                  alert: 'Error: delivery item already booked ' \
                       + "for #{@booking.customer.nickname}"
    elsif @booking.update(delivery_trip_item_attributes: trip_item_params)
      PushNotificationAddress.notify_customer_new_delivery(@booking)
      redirect_to bookings_path,
                  notice: "Booking for #{@booking.customer.nickname} " \
                        + 'successfully created'
    else
      @delivery_trip_item = TripItem.new
      render action: 'delivery'
    end
  end

  # DELETE /bookings/1
  def destroy
    return if @booking.ironed?
    @booking&.request&.update(booking_id: nil)
    @booking.destroy!
    redirect_to bookings_url, notice: 'Booking was successfully deleted.'
  end

  private

  def set_booking
    @booking = Booking.find(params[:id])
  end

  def booking_with_collection_trip_items_params
    params.require(:booking).permit(
      :customer_id,
      :discount,
      collection_trip_item_attributes:
        [:address_id, :trip_date, :notes,
         visit_constraints_attributes: [:visit_after, :visit_before]]
    )
  end

  def booking_trip_items_params
    params.require(:booking).permit(
      :discount,
      collection_trip_item_attributes: [
        :id, :address_id, :trip_date, :notes,
        visit_constraints_attributes:
          [:id, :visit_after, :visit_before]
      ],
      delivery_trip_item_attributes: [
        :id, :address_id, :trip_date, :notes,
        visit_constraints_attributes:
          [:id, :visit_after, :visit_before]
      ]
    )
  end

  def trip_item_params
    params.require(:trip_item).permit(
      :address_id, :trip_date, :notes,
      visit_constraints_attributes:
        [:visit_after, :visit_before]
    )
  end

  def all_undelivered_bookings
    ti = TripItem.joins(:trip).where(trips: { completed: false })
    Booking.where(delivery_trip_item: ti + [nil])
           .includes(:customer, :ironing,
                     delivery_trip_item:   :trip,
                     collection_trip_item: :trip)
  end

  # Builds a blank constraint if none exists
  # This is to make fields show up in the Edit form
  # Otherwise it will be impossible to add constraints to
  #   an originally unconstrained trip item
  def ensure_constraint_fields(trip_item)
    trip_item.visit_constraints.build if trip_item&.visit_constraints&.empty?
  end
end
