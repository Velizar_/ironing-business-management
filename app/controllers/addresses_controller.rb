class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :edit, :update]

  # GET /addresses
  def index
    @addresses = Address.for_customers
  end

  # GET /addresses/1
  def show
  end

  # GET /customers/1/add_address
  def new
    @address = Address.new
    @customer = Customer.find(params[:id])
    @base_address = Address.main_for_base
  end

  # GET /customers/1/edit_active_address
  def edit_active
    @address = Address.new
    @customer = Customer.find(params[:id])
    @base_address = Address.main_for_base
  end

  # GET /customers/1/update_active_address
  def update_active
    @address = Address.new(address_params)
    if @address.save
      @address.customer.addresses.select { |addr| addr.id != @address.id }.each do |addr|
        addr.update(active: false)
      end
      redirect_to @address, notice: 'Active address was successfully replaced.'
    else
      @customer = @address.customer
      render action: 'new'
    end
  end

  # GET /addresses/1/edit
  def edit
    @customer = Address.find(params[:id]).customer
    @base_address = Address.main_for_base
  end

  # POST /addresses
  def create
    @address = Address.new(address_params)
    if @address.save
      redirect_to @address, notice: 'Address was successfully created.'
    else
      @customer = @address.customer
      @base_address = Address.main_for_base
      render action: 'new'
    end
  end

  # PATCH/PUT /addresses/1
  def update
    if @address.update(address_params)
      redirect_to @address, notice: 'Address was successfully updated.'
    else
      render action: 'edit'
    end
  end

  private

  def set_address
    @address = Address.find(params[:id])
  end

  def address_params
    params.require(:address).permit(:customer_id,
                                    :address_name,
                                    :post_code,
                                    :notes,
                                    :active,
                                    :geocode)
  end
end
