class NotificationsController < ApplicationController
  # GET /notifications
  def index
    @unseen_notifications = Notification.where(seen: false).order(id: :desc).load
    page = (params[:page]&.to_i || 1)
    @seen_notifications = Notification.where(seen: true).page(page).order(id: :desc).load

    Notification.where(seen: false).in_batches.update_all(seen: true)
  end
end
