class TripsController < ApplicationController
  # GET /trips[?date=2014-11-23]
  def index
    date = params[:date]&.to_date || DateTime.current.to_date
    any_gathered, @gathering_error, @trips, @trip_drivers, @trip_address_distances, @visit_times, @notifiable = TripService.index(date)
    display_gathered(any_gathered)
  end

  # PATCH/PUT /trips/1/update_active_base
  def update_active_base
    trip = Trip.find(params[:id])
    trip.trip_items.first.update!(address: Address.main_for_base)
    redirect_to trips_path(date: trip.date),
                notice: 'Trip active base was successfully updated.'
  end

  # PATCH/PUT /trips?date=2014-11-23
  def update
    trip = Trip.includes(trip_items: [:visit_constraints, :address])
               .find(params[:trip][:id])
    trip.update!(trip_params)
    if params[:optimize_exactly] or params[:optimize_approximately]
      begin
        trip.optimize!(params[:optimize_exactly].present?)
        redirect_to trips_path(date: trip.date),
                    notice: 'Route was successfully optimized.'
      rescue OptimizationError
        redirect_to trips_path(date: trip.date),
                    alert: "Error: cannot optimize - some 'before' times are too early and therefore impossible to fulfill."
      end
    elsif params[:reverse_order]
      trip.trip_items.reload
      trip.reverse_visit_order
      redirect_to trips_path(date: trip.date),
                  notice: 'Trip was successfully reversed.'
    else
      redirect_to trips_path(date: trip.date),
                  notice: 'Trip was successfully updated.'
    end
  end

  # GET /trips/printable/1
  def printable
    @trip = Trip.find(params[:id])
    @trip.fix_sequences

    @trip_items = @trip.trip_items
                       .offset(1)
                       .includes(address: :customer)
                       .map(&:decorate)
    @bookings = @trip.bookings(true)
    @visit_times, gathered, @gathering_error = @trip.trip_items.visit_times!(@trip.start_time)
    display_gathered(gathered.present?)
  end

  # GET /trips/status/1
  def status
    printable
  end

  # POST /trips/create/2016-07-18
  def create
    Trip.create(date: params[:date], completed: false)
    redirect_to trips_path(date: params[:date]),
                notice: 'Vehicle successfully added'
  end

  # PATCH /trips/move_item/:trip_item_id/:dest_trip_id
  def move_item
    trip = Trip.find params[:trip_id]
    TripItem.find(params[:id]).update(trip: trip, sequence: 100)
    redirect_to trips_path(date: trip.date), notice: 'Item successfully moved'
  end

  # Implements removing a vehicle with no trip items
  # DELETE /trips/1
  def destroy
    trip = Trip.find(params[:id])
    if trip.deletable? && Trip.at_date_all(trip.date).size > 1
      trip.destroy!
      redirect_to trips_path(date: trip.date),
                  notice: 'Vehicle was successfully deleted.'
    else
      redirect_to trips_path(date: trip.date),
                  alert: 'Route not deletable'
    end
  end

  # DELETE /trips/wipe_distances/1
  def wipe_distances
    trip = Trip.find(params[:id])
    addr = trip.trip_items.addresses
    AddressDistance.where(from_address: addr, to_address: addr).delete_all
    redirect_to trips_path(date: trip.date)
  end

  # POST /trips/notify_customers
  def notify_customers
    trip_item_ids = params[:trip_item_ids].map(&:to_i)
    unless trip_item_ids.empty?
      trip = TripItem.find(trip_item_ids.first).trip
      trip.notify(trip_item_ids.to_set)
    end
  end

  private

  def trip_params
    params.require(:trip).permit(
      :start_time, :sched_deadline, :completed, :driver_account_id,
      trip_items_attributes: [:id, :sequence]
    )
  end
end
