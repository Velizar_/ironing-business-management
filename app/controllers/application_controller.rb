class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception, unless: :is_devise_customers_endpoint?
  before_action :assign_permissions
  # default action, skipped sometimes
  before_action :authenticate_owner!
  before_action :retrieve_active_requests
  before_action :count_unread_notifications
  before_action :check_pending_registrations
  respond_to :html, :json

  def display_gathered(any_gathered)
    flash.now[:notice] = 'Missing distances gathered.' if any_gathered
  end

  def assign_permissions
    @ironer_mode = current_staff&.only_ironer? || false
    @is_owner    = current_staff&.owner? || false
  end

  def authenticate_owner!
    return unless authenticate_staff!
    render html: '', layout: true unless current_staff.owner?
  end

  # Sometimes used in place of authenticate_owner!
  def authenticate_ironer!
    return unless authenticate_staff!
    render html: '', layout: true unless @ironer_mode || current_staff.owner?
  end

  def retrieve_active_requests
    @active_requests = Request.where(active: true)
    @booking_avl_insufficient = BookingAvailability.where(date: Date.today..(Date.today + 6)).count < 7
  end

  def count_unread_notifications
    @unread_notifications_count = Notification.where(seen: false).count
  end

  def check_pending_registrations
    @pending_registrations_count = CustomerRegistration.where(customer_id: nil).count
  end

  def is_devise_customers_endpoint?
    request.path == customer_session_path + '.json' ||
      request.path == destroy_customer_session_path + '.json'
  end
end
