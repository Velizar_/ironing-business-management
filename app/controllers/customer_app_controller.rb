include ApplicationHelper

class CustomerAppController < ApplicationController
  skip_before_action :authenticate_owner!
  before_action      :authenticate_customer!, except: [
    :request_code, :check_code, :complete_account,
    :booking_availability, :register]
  skip_forgery_protection

  # POST /customer_app/request_code
  def request_code
    if params[:phone].blank?
      render plain: 'Error', layout: false
      return
    end
    customer = Customer.find_by_phone_number(params[:phone])
    if customer.nil?
      render plain: 'Customer not found', layout: false
      return
    end

    if params[:phone].starts_with?('0')
      params[:phone] = '+44' + params[:phone].delete_prefix('0')
    end
    res = customer.create_and_send_code(params[:phone])
    if res == true
      render plain: 'Success', layout: false
    else
      head :too_many_requests, 'Retry-After': res
    end
  end

  # GET /customer_app/check_code
  def check_code
    customer = Customer.find_by_phone_number(params[:phone])
    if params[:phone].present? && customer&.confirmation_code == params[:code]
      render plain: 'Success', layout: false
    else
      render plain: 'Failure', layout: false
    end
  end

  # POST /customer_app/complete_account
  def complete_account
    if params[:password].blank? or params[:phone].blank? or params[:code].blank?
      render plain: 'Error', layout: false
      return
    end
    customer = Customer.find_by_phone_number(params[:phone])
    if customer&.confirmation_code == params[:code]
      customer.update(password: params[:password], confirmation_code: nil)
      render plain: 'Success', layout: false
    else
      render plain: 'Error', layout: false
    end
  end

  # POST /customer_app/request
  def create_request
    request = Request.new(new_request_params)
    if request.can_automatically_book?
      request.save_with_booking(automatically_created: true)
      PushNotificationAddress.notify_admins_new_booking(request)
      render plain: 'Success', layout: false
    elsif request.save
      PushNotificationAddress.notify_admins_new_request(request)
      render plain: 'Success', layout: false
    else
      render plain: 'Error', layout: false
    end
  end

  # DELETE /customer_app/request/1
  def delete_request
    request = Request.find_by_id(params[:id])
    if request.customer == current_customer && request.booking.nil?
      PushNotificationAddress.notify_admins_cancelled_request(request)
      request.destroy!
      render plain: 'Success', layout: false
    else
      render plain: 'Error', layout: false
    end
  end

  # DELETE /customer_app/booking/1
  def delete_booking
    booking = Booking.find_by_id(params[:id])
    if booking&.customer == current_customer &&
      !booking.collection_trip_item.trip.sched_deadline_passed
    then
      PushNotificationAddress.notify_admins_cancelled_booking(booking)
      booking.request&.destroy
      booking.destroy!
      render plain: 'Success', layout: false
    else
      render plain: 'Error', layout: false
    end
  end

  # GET /customer_app/booking_status.json
  def booking_status
    @has_notification_address = current_customer.push_notification_addresses.present?
    @has_one_active_address = current_customer.addresses.where(active: true).count <= 1
    @requests = Request.where(address: current_customer.addresses, active: true).sort_by(&:created_at)
    @bookings = Booking.where(customer: current_customer).includes(
      ironing: { booking_services: :service },
      collection_trip_item: :trip,
      delivery_trip_item: :trip
    ).reject(&:delivered?).sort_by {|b| b.collection_trip_item.trip_date }
  end

  # GET /customer_app/booking_availability.json
  def booking_availability
    @available = []
    @unavailable = []
    @unconfirmed = []
    date_interval = Date.today..(Date.today + 44)
    booking_avl = BookingAvailability.where(date: date_interval)
    trips = Trip.where(date: date_interval).includes(:trip_items)
    (date_interval).map do |date|
      case BookingAvailability.available?(date, booking_avl, trips)
      in 'available'
        @available << date
      in 'unavailable'
        @unavailable << date
      else
        @unconfirmed << date
      end
    end
    @not_confirmed_as_available = @unconfirmed + @unavailable
    @current_driver_location = DriverLocation.last
  end

  def register_push_notification
    notification_params = params.permit(:endpoint, :auth_key, :p256dh_key)
    addr = PushNotificationAddress.new(notification_params)
    addr.customer_id = current_customer&.id
    if addr.save
      render plain: 'Success', layout: false
    else
      render plain: 'Failed to save', layout: false
    end
  end

  def register
    reg = CustomerRegistration.create(registration_params)
    PushNotificationAddress.notify_admins_new_registration(reg)
    render plain: 'Success', layout: false
  end

  private

  def new_request_params
    address_id = current_customer.addresses.all_active.first&.id
    collection_dates = params[:collection_dates].map { |d| Date.strptime(d, "%m/%d/%Y") }
    delivery_dates = params[:delivery_dates].map { |d| Date.strptime(d, "%m/%d/%Y") }
    { address_id: address_id, dates: collection_dates, delivery_dates: delivery_dates }.merge(params.permit(:additional_requirements))
  end

  def registration_params
    par = params.require(:customer_registration).permit(
      :first_name, :last_name, :phone_mobile, :phone_home, :email, :encrypted_password, :payment_scheme,
      :address, :post_code, :request_requirements, collection_dates: [], delivery_dates: [])
    par['collection_dates'] = par['collection_dates'].map { |d| Date.strptime(d, "%m/%d/%Y") }
    par['delivery_dates'] = par['delivery_dates'].map { |d| Date.strptime(d, "%m/%d/%Y") }
    par
  end
end
