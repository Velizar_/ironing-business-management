class PushNotificationAddressController < ApplicationController
  def create
    addr = PushNotificationAddress.new(sanitized_params)
    addr.staff_id = current_staff&.id
    render plain: addr.save.to_s, layout: false
  end

  private

  def sanitized_params
    params.permit(:endpoint, :auth_key, :p256dh_key)
  end
end
