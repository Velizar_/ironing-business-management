class DriverApiController < ApplicationController
  skip_before_action :authenticate_owner!
  skip_before_action :verify_authenticity_token
  before_action :authenticate_driver_non_browser!

  # POST /driver_api/receive lat=[] long=[]
  def receive
    DriverLocation.received(driver_location_params)
    render plain: 'ACK', layout: false
  end

  # GET /driver_api/get_daily_trip.json?username=driver&password=123456
  def get_daily_trip
    @trip = Trip.today_for_driver(@driver)
    if @trip.nil?
      render plain: 'No trip assigned', layout: false
    else
      trip_items = @trip.trip_items.includes(address: :customer)
      visit_times, = trip_items.visit_times!(@trip.start_time)
      trip_items = trip_items.offset(1)
      bookings = @trip.bookings(true)
      @trip_data = trip_items.zip(bookings, visit_times)
    end
  end

  # POST /driver_api/upload_trip_status.json
  def upload_trip_status
    trip_status_params[:tripItems].each do |trip_item|
      TripItem.find_by(id: trip_item[:id])&.update(trip_item)
    end
  end

  # treats the visitor as a json user who should be a driver
  def authenticate_driver_non_browser!
    staff = Staff.find_by(username: params[:username])
    if staff&.valid_password?(params[:password]) && staff&.driver?
      @driver = staff.driver_account
    else
      render plain: 'Invalid username or password', layout: false
    end
  end

  private

  def driver_location_params
    params.permit(:trip_id, :trip_item_id, :lat, :long)
  end

  def trip_status_params
    params.permit(tripItems: [:id, :state, :paid])
  end
end
