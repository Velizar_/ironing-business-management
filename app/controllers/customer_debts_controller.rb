class CustomerDebtsController < ApplicationController
  def index
    @customers = Customer.all_active
                         .where.not(money_owed: 0)
                         .order(money_owed: :desc)
  end

  # GET /customers/customer_debts/increase
  def increase
    @customers = Customer.all_active.non_system
  end

  # POST /customers/customer_debts/increase_debt
  def increase_debt
    customer = Customer.find(params[:customer])
    amount = BigDecimal.new(params[:amount])
    customer.update!(money_owed: customer.money_owed + amount)
    redirect_to customer_debts_index_path,
                notice: "Debt for #{customer.nickname} " \
                      + "successfully increased by £#{amount}"
  end

  def decrease
    @customer = Customer.find(params[:id])
  end

  def decrease_debt
    customer = Customer.find(params[:id])
    amount = BigDecimal.new(params[:amount])
    customer.update!(money_owed: customer.money_owed - amount)
    redirect_to customer_debts_index_path,
                notice: "Debt for #{customer.nickname} " \
                      + "successfully decreased by £#{amount}"
  end
end
