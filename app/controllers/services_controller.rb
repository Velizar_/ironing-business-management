class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update,
                                     :edit_price, :update_price, :destroy]

  # GET /services
  def index
    @services = Service.includes(:category, :booking_services)
                       .non_system
                       .all_active
                       .order(id: :desc)
    @editable_system_services = [
      Service.ironing_per_hour, Service.missed_driver_fine,
      Service.washing_per_laundry, Service.minimum_charge
    ]
  end

  # GET /services/1
  def show
  end

  # GET /services/new
  def new
    @service = Service.new
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services
  def create
    @service = Service.new(new_service_params)
    if @service.system?
      render plain: 'Error: cannot create a system service.'
    elsif @service.save
      redirect_to @service, notice: 'Service was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /services/1
  def update
    return if edit_service_params['category_id'].to_i == Category.system.id

    if @service.system?
      redirect_to @service, alert: 'Error: cannot edit.'
    elsif @service.update(edit_service_params)
      redirect_to @service, notice: 'Service was successfully updated.'
    else
      render :edit
    end
  end

  # GET /services/1/edit_price
  def edit_price
    return if @service.active
    redirect_to services_url, alert: 'Error: cannot edit an inactive price.'
  end

  # POST /services/1/update_price
  def update_price
    old_name = @service.name
    if !@service.active
      redirect_to services_url,
                  alert: 'Error: cannot edit an inactive price.'
    elsif @service == Service.extra_charge
      redirect_to services_url,
                  alert: 'Error: cannot edit this service.'
    elsif @service.update_price(edit_price_param)
      redirect_to services_url,
                  notice: "#{old_name} price was successfully changed."
    else
      render :edit_price
    end
  end

  # DELETE /services/1
  def destroy
    return unless @service.deletable?
    @service.destroy
    redirect_to services_url, notice: 'Service was successfully deleted.'
  end

  private

  def set_service
    @service = Service.find(params[:id])
  end

  def edit_service_params
    params.require(:service).permit(:name, :category_id)
  end

  def new_service_params
    params.require(:service).permit(:name, :price, :category_id, :active)
  end

  def edit_price_param
    params.require(:service).permit(:price)[:price]
  end
end
