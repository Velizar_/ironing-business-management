class ExpensesController < ApplicationController
  before_action :set_expense, only: [:edit, :edit_periodic, :update, :destroy]

  # GET /expenses
  def index
    all_expenses = Expense.includes(:expense_type).all.order(id: :desc)
    @one_time, @periodic = all_expenses.partition(&:one_time?)
  end

  # GET /expenses/new
  def new
    @expense = Expense.new
  end

  # GET /expenses/new_periodic
  def new_periodic
    new
  end

  # GET /expenses/1/edit
  def edit
  end

  # GET /expenses/1/edit_periodic
  def edit_periodic
  end

  # POST /expenses
  def create
    @expense = Expense.new(expense_params)

    if @expense.save
      redirect_to expenses_path,
                  notice: 'Expense was successfully created.'
    elsif @expense.one_time?
      render :new
    else
      render :new_periodic
    end
  end

  # PATCH/PUT /expenses/1
  def update
    if @expense.update(expense_params)
      redirect_to expenses_path,
                  notice: 'Expense was successfully updated.'
    elsif @expense.one_time?
      render :edit
    else
      render :edit_periodic
    end
  end

  # DELETE /expenses/1
  def destroy
    @expense.destroy!
    redirect_to expenses_url,
                notice: 'Expense was successfully deleted.'
  end

  private

  def set_expense
    @expense = Expense.find(params[:id])
  end

  # Used for both one-time and periodic
  def expense_params
    params[:expense][:to] = params[:expense][:from] if params[:one_time]
    params.require(:expense).permit(:cost, :expense_type_id,
                                    :from, :to,
                                    :period, :period_type)
  end
end
