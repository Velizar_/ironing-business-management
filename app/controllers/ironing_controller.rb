class IroningController < ApplicationController
  before_action :set_ironing, only: [:show, :receipt, :new, :edit]

  OWNER_ONLY_PAGES = [:receipt, :show, :past].freeze
  skip_before_action :authenticate_owner!,  except: OWNER_ONLY_PAGES
  before_action      :authenticate_ironer!, except: OWNER_ONLY_PAGES

  # GET /ironing
  def index
    @ironed, @ready_for_ironing = Ironing.pending(
      ironing_tasks: { ironer_account: :staff },
      booking: :customer
    )
    return if @ironer_mode
    @needing_extra_input, @ironed = Ironing.ironed_to_administrative(@ironed)
  end

  # GET /ironing/past
  def past
    page = (params[:page]&.to_i || 1)
    from = (params[:from_date]&.to_date || '2000-01-01'.to_date)
    to   = (params[:to_date]&.to_date   || Time.zone.today + 40.years)
    @ironed = Ironing.all_past(from..to).page(page)
                     .includes(booking:
                                 { customer: [],
                                   delivery_trip_item: :trip },
                               ironing_tasks:
                                 { ironer_account: :staff })
    render action: 'index'
  end

  # GET /ironing/new/:state_2_id (collected)
  # transition to state 3 (ironed)
  def new
    @ironer_accounts = IronerAccount.all_active.includes(:staff)
    @ironing_tasks = @ironing.ironing_tasks.build

    svc = if @ironing.booking.flat_rate?
            Service.ironing_per_hour
          else
            nil
          end
    @ironing_items = @ironing.booking_services.build(service: svc)
    @minimum_charge_checked = true
    @ironing = @ironing.decorate
  end

  # POST /ironing
  # reached from new and edit, technically works as update
  def create
    @ironing = Ironing.find(params[:ironing][:id])
    if @ironing.save_form(ironing_params, params[:minimum_charge], @ironer_mode)
      redirect_to ironing_index_path, notice: 'Ironing was successfully saved.'
    else
      @ironing = @ironing.decorate
      render action: 'new', id: params[:id]
    end
  end

  # GET /ironing/:state_2_or_3_id
  def show
    @ironing_items = @ironing.booking_services
                             .where.not(service: Service.all_system)
  end

  # GET /ironing/receipt/:state_3_id
  def receipt
  end

  # GET /ironing/:state_3_id/edit
  # posts to /ironing/create
  def edit
    @ironing_tasks = @ironing.ironing_tasks.includes(:ironer_account)
    inactive_used_accounts = @ironing_tasks.map(&:ironer_account)
                                           .uniq
                                           .reject(&:active)
    @ironer_accounts = IronerAccount.all_active + inactive_used_accounts

    if @ironing.booking.flat_rate?
      @ironing.booking_services.where(
        service: Service.ironing_per_hour
      ).first_or_initialize
    end
    @ironing_items = @ironing.booking_services

    if @ironer_mode
      @ironing_items = @ironing_items.where(
        service: Service.all_active.non_system
      )
    end
    @minimum_charge_checked = !@ironing.minimum_charge_skipped?
    @ironing = @ironing.decorate
  end

  def ironing_params
    par = params.require(:ironing).permit(
      :bag_count, :has_hangers, :date_ironed, :minimum_charge,
      booking_services_attributes:
        [:id, :service_id, :count, :discount, :_destroy],
      ironing_tasks_attributes:
        [:id, :ironer_account_id, :minutes_ironed, :_destroy]
    )
    par[:bag_count] ||= 0
    # convert input in time string format to minutes int format
    # example: "02:40" -> 160
    par[:ironing_tasks_attributes]&.each do |key, values|
      time = Time.zone.parse(values[:minutes_ironed])
      par[:ironing_tasks_attributes][key][:minutes_ironed] =
        time.hour * 60 + time.min
    end
    par
  end

  private

  def set_ironing
    @ironing = Ironing.find(params[:id])
  end
end
