class StatisticsController < ApplicationController
  def index
    @filters = ['', 'Ironing', 'Driver', 'Ironers'] + ExpenseType.pluck(:name)
  end

  def general
    step = (params[:period].to_i || 7)
    start_date = params[:start_date]&.to_date

    @booking_growth  = Statistics.growth_over_time(Booking, step, start_date)
    @customer_growth = Statistics.growth_over_time(Customer, step, start_date)

    @booking_count  = Statistics.count_over_time(Booking, step, start_date)
    @customer_count = Statistics.count_over_time(Customer, step, start_date)
  end

  def customers
    @customers = Customer.all.includes(
      bookings: { ironing: { booking_services: :service } }
    ).sort_by { |c| -c.bookings.size }
  end

  def finance
    @period = (params[:period].to_i || 7)
    start_date = params[:start_date]&.to_date

    profits = Expense.profits_in_period(start_date)
    if params[:filter].present?
      profits = profits.select { |name, _| name == params[:filter] }
    end

    sorted_dates = profits.values.flatten.map(&:date).sort
    start_date = sorted_dates.first + @period - 1
    end_date = sorted_dates.last + @period
    @date_period = (start_date..end_date).step(@period)

    # { name => [date, amount] }
    # the difference between two dates is step
    # if amount is between two dates, it is included in the _later_
    profits_periods_hash = profits.keys.map do |name|
      profits_array = profits[name].sort_by(&:date)
      [name, @date_period.map do |date|
        relevant = profits_array.take_while { |p| p.date <= date }
        profits_array = profits_array.drop(relevant.size)
        [date, relevant.sum(&:amount).round(2)]
      end.to_h]
    end.to_h

    # date => amount
    @expenses_growth = profits_periods_hash.values.reduce do |h1, h2|
      h1.merge(h2) { |_, a1, a2| (a1 + a2).round(2) }
    end

    # date => accumulated_amount_until_date
    @expenses_sum = Statistics.count_from_growth(@expenses_growth)

    # { date => { name => amount_in_date } }
    @date_breakdown = @date_period.map do |date|
      [date, profits_periods_hash.map do |name, date_values|
        [name, date_values[date]]
      end.to_h]
    end.to_h

    # { name => total_amount }
    @total_breakdown = @date_breakdown.values.reduce do |h1, h2|
      h1.merge(h2) { |_, a1, a2| (a1 + a2).round(2) }
    end
  end
end
