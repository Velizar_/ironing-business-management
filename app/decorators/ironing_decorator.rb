class IroningDecorator < Draper::Decorator
  delegate_all

  def categories(ironer_mode)
    if ironer_mode
      return Category.non_system.map do |c|
        [c.name, c.services.all_active]
      end
    end

    cat = if booking.itemized?
            Category.all.to_a
          else
            [Category.system]
          end

    # [ [[category1.name],
    #    [all services of category1]],
    #    [same for all other categories] ]

    # not inputted via this page
    noinput_service_ids = [Service.ironing_per_hour,
                           Service.minimum_charge].map(&:id)

    inactive_used_services_ids =
      booking_services.includes(:service).map(&:service)
                      .select { |s| !s.active }
                      .map(&:id)

    # inactive and not in booking_services
    unusable_service_ids = (Service.where(active: false).pluck(:id) -
                            inactive_used_services_ids)

    to_exclude = noinput_service_ids + unusable_service_ids
    cat.map do |c|
      [c.name, c.services.where.not(id: to_exclude)]
    end
  end
end
