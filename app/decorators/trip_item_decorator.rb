class TripItemDecorator < Draper::Decorator
  delegate_all

  def bag_hangers_str(booking)
    ir = booking.ironing
    hangers = ir.has_hangers?
    bags = (ir.bag_count != 0)
    bags_string = ir.bag_count.to_s + ' ' + 'bag'.pluralize(ir.bag_count)
    if collection_or_delivery(booking) == 'collection'
      ''
    elsif !ir.ironed?
      '(not ironed yet)'
    elsif !hangers && !bags
      ''
    elsif hangers && bags
      "(#{bags_string} + hangers)"
    elsif hangers && !bags
      '(only hangers)'
    elsif !hangers && bags
      "(#{bags_string})"
    end
  end

  def map_marker_display_data
    lat, lng = address.geocode.split(',')
    [lat, lng, sequence, address.customer.nickname]
  end
end
