namespace :daily do
  task all: [:update_trips, :clean_up_jwt]

  desc 'Mark older trips as completed'
  task update_trips: :environment do
    Trip.where(date: ...DateTime.current.yesterday, completed: false).update_all(completed: true)
  end

  desc 'Delete expired JWT tokens'
  task clean_up_jwt: :environment do
    JwtDenylist.where(exp: ..DateTime.now).destroy_all
  end
end
