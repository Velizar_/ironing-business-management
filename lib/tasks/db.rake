namespace :db do
  # For use both in development, testing, and demoing.
  desc 'Add mock data to DB including admin, all passwords are 123456'
  task populate: :environment do
    # The following data is generated:
    # an owner with username: admin and password: 123456
    # four staff members - three ironers and one driver
    # customers, each with 1 or more addresses
    # their address distances, calculated by geocode difference
    #   some distances will be missing
    # one category: 'Ironing'
    # multiple services
    # multiple trips (one per day), one is for today and some for tomorrow
    # each trip has its DriverLocation
    # multiple bookings and their trip items
    # each ironed booking has booking_services

    # 150 randomly generated addresses
    #   from http://www.doogal.co.uk/RandomAddresses.php
    # format: [geocode, address_name, post_code]
    ADDRESS_DATA = [
      ['51.416776,-0.062616', '61 Anerley Park', 'SE20'], ['51.470123,0.049313', '15 Howerd Way', 'SE18 4PX'], ['51.443695,-0.016999', '51A Sangley Road', 'SE6 2DT'], ['51.492762,-0.080152', ' ', 'SE1P 5PT'], ['51.49905,-0.062209', '2 Marigold Street', 'SE16 4PF'], ['51.485372,-0.105656', '6 Harmsworth Street', 'SE17 3TJ'], ['51.473673,0.072185', '25 Bushmoor Crescent', 'SE18 3EG'], ['51.488074,0.108003', '80 Basildon Road', 'SE2 0EP'], ['51.501133,-0.072942', '83 Saint Saviours', 'SE1 2YP'], ['51.390448,-0.088103', '74 Selhurst Road', 'SE25 5QD'], ['51.493607,-0.07189', '55 Southwark Park Road', 'SE16 3TZ'], ['51.480272,-0.071635', '36 Colegrove Road', 'SE15 6ND'], ['51.45523,-0.070456', '133A Hindmans Road', 'SE22 9NH'], ['51.489363,0.018305', '1 Horn Lane', 'SE10 9HR'], ['51.407082,-0.080263', '29C Auckland Road', 'SE19 2DR'], ['51.485415,-0.095036', '2B Fielding Street', 'SE17 3HD'], ['51.399682,-0.092199', '3 Oban Road', 'SE25 6QQ'], ['51.503964,-0.07473', '38 Shad Thames', 'SE1 2YD'], ['51.49929,-0.084488', '195 Long Lane', 'SE1 4PD'], ['51.473776,-0.096544', ' ', 'SE5 5FY'], ['51.447621,0.016812', '51 Pitfold Road', 'SE12 9JB'], ['51.436289,-0.063226', '20A Kirkdale', 'SE26 4NE'], ['51.503331,-0.10777', ' Cons Street', 'SE1'], ['51.506273,0.122796', '22 Bledlow Close', 'SE28 8HF'], ['51.49655,-0.090698', '67 Deverell Street', 'SE1 4EX'], ['51.499751,0.085855', '8 Narrow Boat Close', 'SE28 0HZ'], ['51.492838,0.0713', '62 Beresford Street', 'SE18 6GB'], ['51.427493,-0.047444', '26B Mayow Road', 'SE26 4JA'], ['51.41909,-0.083154', '4-6 Westow Street', 'SE19 3AH'], ['51.403415,-0.065047', '25B Wheathill Road', 'SE20 7XQ'], ['51.464288,0.048982', '20 Arbroath Road', 'SE9 6RN'], ['51.484543,-0.084189', '19 Albany Road', 'SE5 0AW'], ['51.477311,-0.099334', '7 Councillor Street', 'SE5 0LY'], ['51.46153,-0.031781', '120-130 Breakspears Road', 'SE4'], ['51.453885,0.055306', '47 Archery Road', 'SE9 1HF'], ['51.50527,-0.109454', '11B Theed Street', 'SE1 8ST'], ['51.487304,-0.107251', '120 Kennington Park Road', 'SE11 4DJ'], ['51.485437,0.017095', '28B Combedale Road', 'SE10 0LG'], ['51.470374,-0.104662', '40 Lilford Road', 'SE5 9HX'], ['51.407033,-0.081113', '15 Sunset Gardens', 'SE25 4AX'], ['51.45988,0.002926', '88 Boone Street', 'SE13 5SA'], ['51.479208,-0.096244', ' Bowyer Street', 'SE5'], ['51.499876,0.083789', '38 Princess Alice Way', 'SE28 0HQ'], ['51.480234,-0.080058', '39 Lynbrook Grove', 'SE15 6HZ'], ['51.416889,-0.080285', '11 Belvedere Road', 'SE19'], ['51.490055,-0.092457', '104 Brandon Street', 'SE17 1AL'], ['51.485929,0.079882', '58 Waverley Road', 'SE18'], ['51.502021,-0.08214', '47 Bermondsey Street', 'SE1 3XT'], ['51.495827,-0.065636', "87 Saint James's Road", 'SE16 4QS'], ['51.487196,0.052578', '23 Pellipar Road', 'SE18 5EG'], ['51.472821,-0.088117', '5 Camberwell Grove', 'SE5 8JA'], ['51.481895,0.126041', ' Hurst Lane', 'SE2'], ['51.492762,-0.080152', ' ', 'SE1P 5PT'], ['51.478125,0.106651', '3 Littledale', 'SE2 0NU'], ['51.39179,-0.069591', '21B Howard Road', 'SE25 5BU'], ['51.497579,-0.072256', '79 Enid Street', 'SE16 3RA'], ['51.426469,-0.033534', '3 Thomas Dean Road', 'SE26 5BY'], ['51.501957,-0.094736', '8 Disney Street', 'SE1 1JF'], ['51.425931,-0.058414', '5 Hall Drive', 'SE26 6XL'], ['51.462308,-0.009737', '93 Lewisham High Street', 'SE13 6BB'], ['51.486699,-0.065254', '7 Fern Walk', 'SE16 3JD'], ['51.472349,-0.056349', '9 Evan Cook Close', 'SE15 2HL'], ['51.500574,-0.091019', '249 Empire Square West', 'SE1 4NL'], ['51.492762,-0.080152', ' ', 'SE1P 5PT'], ['51.43623,0.077748', '78 Dulverton Road', 'SE9 3RL'], ['51.444245,0.026567', '172 Alnwick Road', 'SE12 9BT'], ['51.489149,-0.098453', '111-123 Crampton Street', 'SE17 3AA'], ['51.456842,0.006856', '20 Lampmead Road', 'SE12 8QL'], ['51.484346,0.08565', '19 Leghorn Road', 'SE18 1SZ'], ['51.38933,-0.089998', '2 Dagnall Park', 'SE25'], ['51.506463,-0.095952', '105 Sumner Street', 'SE1 9HZ'], ['51.474574,-0.029435', '15A Florence Road', 'SE14 6TW'], ['51.488652,0.005872', '43A Azof Street', 'SE10 0EG'], ['51.49665,-0.077893', ' Fendall Street', 'SE1'], ['51.451997,0.026286', '22 Scotsdale Road', 'SE12 8BP'], ['51.477135,-0.017574', '110-114 Norman Road', 'SE10 9QJ'], ['51.494001,-0.121003', ' Albert Embankment', 'SE1'], ['51.473776,-0.096544', ' ', 'SE5 5FY'], ['51.475162,-0.0336', '9 Parkfield Road', 'SE14 6QB'], ['51.484341,0.105066', '13A Bastion Road', 'SE2 0RD'], ['51.492762,-0.080152', ' ', 'SE1P 5PT'], ['51.489711,-0.092639', '1 Townley Street', 'SE17 1DZ'], ['51.484834,-0.055783', '3 Gerards Close', 'SE16 3DF'], ['51.461224,-0.098729', ' Poplar Walk', 'SE24 0BS'], ['51.482736,0.083114', '51 Waverley Crescent', 'SE18 7QU'], ['51.49385,-0.105547', '147B Brook Drive', 'SE11 4TQ'], ['51.447415,-0.012328', '137B Laleham Road', 'SE6 2JD'], ['51.462317,-0.014488', '15 Bankside Avenue', 'SE13 7BD'], ['51.427919,0.022862', '89 Wydeville Manor Road', 'SE12 0EP'], ['51.414497,-0.060365', '14 Upchurch Close', 'SE20 8QY'], ['51.433207,-0.104618', '14A Bloom Grove', 'SE27 0HZ'], ['51.482634,0.019593', '116B Westcombe Hill', 'SE3 7DT'], ['51.43548,-0.025507', '20 Grangemill Way', 'SE6 3JU'], ['51.479661,-0.012593', '33 Randall Place', 'SE10 9LA'], ['51.488917,-0.04732', '21 Regeneration Road', 'SE16 2NY'], ['51.432895,0.070938', '844 Sidcup Road', 'SE9 3PN'], ['51.427452,-0.10021', '2 Elder Road', 'SE27 9NQ'], ['51.48785,0.073216', '152 Crescent Road', 'SE18 7AA'], ['51.484305,-0.07091', '43-45 Ossory Road', 'SE1 5AN'], ['51.436922,-0.089184', '119A Park Hall Road', 'SE21 8ES'], ['51.448841,-0.011589', '133 Farley Road', 'SE6 2AF'], ['51.427876,-0.051527', '11A Queensthorpe Road', 'SE26 4PJ'], ['51.461283,0.053877', '10 Brome Road', 'SE9 1LD'], ['51.44405,-0.022281', '11 Canadian Avenue', 'SE6 3AT'], ['51.444114,-0.011234', '67 Sportsbank Street', 'SE6 2EY'], ['51.481253,0.125416', ' Woolwich Road', 'SE2'], ['51.404631,-0.058744', '9 Felmingham Road', 'SE20 7YD'], ['51.432228,-0.057808', ' Shrublands Close', 'SE26'], ['51.437919,0.022339', '197 Burnt Ash Hill', 'SE12 0QB'], ['51.486198,0.022559', '4 Hardman Road', 'SE7 7QX'], ['51.504842,-0.09079', '22 Southwark Street', 'SE1 1TU'], ['51.477189,-0.039', '45 Southerngate Way', 'SE14 6DN'], ['51.472272,-0.069449', '48 Peckham High Street', 'SE15 5ER'], ['51.492026,-0.098389', '100 Walworth Road', 'SE17 1RW'], ['51.480338,0.074349', '16A Vernham Road', 'SE18 3EZ'], ['51.488294,-0.079178', '37 Kinglake Street', 'SE17 2RR'], ['51.3979,-0.088957', '122 Clifton Road', 'SE25 6QA'], ['51.491973,-0.092452', '63 Rodney Road', 'SE17'], ['51.42297,0.047269', '287 Beaconsfield Road', 'SE9 4EF'], ['51.436439,-0.098183', '29 Idmiston Road', 'SE27 9HQ'], ['51.452859,0.049642', '21A Lassa Road', 'SE9 6PU'], ['51.497659,-0.04908', '21 Surrey Quays Road', 'SE16'], ['51.479386,-0.09668', ' Bower Street', 'SE5 0UU'], ['51.445657,-0.063621', '107B Wood Vale', 'SE23 3DT'], ['51.487261,0.096183', '48A Ceres Road', 'SE18 1HP'], ['51.479723,0.062281', '6 Kempt Street', 'SE18 4ET'], ['51.500495,0.093966', '64 New Acres Road', 'SE28 0LA'], ['51.427156,-0.087535', '226C Gipsy Road', 'SE27 9RB'], ['51.493349,-0.075031', '105 Alscot Road', 'SE1'], ['51.471537,-0.070888', '9 Highshore Road', 'SE15 5AA'], ['51.495612,0.105887', '107 Sewell Road', 'SE2 9DJ'], ['51.478816,0.096949', '125 Gilbourne Road', 'SE18 2NX'], ['51.399228,-0.060939', '225 Harrington Road', 'SE25 4NF'], ['51.412026,-0.059942', '5 Hartfield Grove', 'SE20 8LD'], ['51.487975,-0.085747', ' Thurlow Street', 'SE17 2UZ'], ['51.482199,0.125658', ' Hurst Lane', 'SE2 0QD'], ['51.502721,-0.082586', '33 Bermondsey Street', 'SE1 2EG'], ['51.475503,-0.085019', ' Saint Giles Road', 'SE5 7RN'], ['51.465656,-0.058505', '33-35 Nunhead Green', 'SE15 3QG'], ['51.458588,0.049353', '51 Kidbrooke Lane', 'SE9'], ['51.428631,-0.063489', '59 Longton Grove', 'SE26 6QQ'], ['51.430202,-0.080777', '31 Seeley Drive', 'SE21 8QR'], ['51.492762,-0.080152', ' ', 'SE1P 5PT'], ['51.47811,-0.082467', '80 Coleman Road', 'SE5'], ['51.460162,-0.091866', '15 Sunray Avenue', 'SE24'], ['51.453423,-0.038551', '406A Brockley Road', 'SE4 2DH'], ['51.477385,0.070946', '19B Dallin Road', 'SE18 3NY'], ['51.469217,0.027219', '23 Chiswell Square', 'SE3 0JG'], ['51.461548,-0.036144', '79 Foxberry Road', 'SE4 2SS'], ['51.492762,-0.080152', ' ', 'SE1P 5PT']
    ].shuffle

    # note: currently, random ranges are located within
    #   individual methods rather than as CAPS_CONSTANTS

    generate_staff number_of_ironers: 3
    generate_customers_and_addresses address_data: ADDRESS_DATA, number_of_customers: 40,
                                     itemized_probability: 0.5, extra_address_probability: 0.1
    generate_categories_and_services number_of_services: 8
    generate_bookings_trips_and_trip_items days_since_first_booking: 5,
                                           average_new_bookings_per_day: 10,
                                           trip_start: Trip.default_start_time,
                                           trip_end: Trip.default_start_time + 6.hours,
                                           trip_item_constraint_probability: 0.07
    generate_address_distances probability_of_missing_distance: 0.2
    insert_trips_missing_distances_and_driver
    Trip.all.each do |trip|
      trip.fix_sequences
      trip.reload.optimize!(true)
    end
    generate_ironing_data_for_collected laundry_probability: 0.1
    generate_driver_locations seconds_between_geocode_signal: 80
    generate_expenses number_of_expense_types: 4, start_date: Time.zone.today - 15
  end

  # generate owner, ironers, and driver
  def generate_staff(number_of_ironers:)
    Staff.create(username: 'admin',
                 first_name: 'Admin',
                 password: '123456',
                 password_confirmation: '123456',
                 permissions: 1,
                 active: true)
    (1..number_of_ironers).each do |i|
      ironer = Staff.create(username: "Ironer #{i}",
                            first_name: "Ironer #{i}",
                            password: '123456',
                            password_confirmation: '123456',
                            active: true)
      ironer.ironer_accounts.create(active: true, pay_rate: 8 + rand(-2..2))
    end
    driver = Staff.create(username: 'Driver',
                          first_name: 'Driver',
                          password: '123456',
                          password_confirmation: '123456',
                          active: true)
    driver.driver_accounts.create(active: true, pay_rate: 35)
  end

  def generate_customers_and_addresses(address_data:,
                                       number_of_customers:,
                                       itemized_probability:,
                                       extra_address_probability:)
    current_address = 0
    (1..number_of_customers).each do |i|
      scheme = rand < itemized_probability ? 'itemized' : 'flat_rate'
      # used for Statistics: New customers over time
      created = random_date(2.months.ago, Time.current)
      c = Customer.create(nickname: "Customer #{i}",
                          payment_scheme: scheme,
                          active: true,
                          created_at: created)
      attributes = address_data[current_address]
      current_address += 1
      c.addresses.create(geocode: attributes[0],
                         address_name: attributes[1],
                         post_code: attributes[2],
                         active: true)
      while rand < extra_address_probability
        attributes = address_data[current_address]
        current_address += 1
        c.addresses.create(geocode: attributes[0],
                           address_name: attributes[1],
                           post_code: attributes[2],
                           active: true)
      end
    end
  end

  def generate_categories_and_services(number_of_services:)
    c = Category.create(name: 'Ironing')
    (1..number_of_services).each do |i|
      Service.create(name: "Service #{i}", price: (10 + rand(-5..10)),
                     category: c, active: true)
    end
  end

  def random_visit_after(from, to)
    # don't place constraints close to the edges, especially not to the end
    from += 20.minutes
    to -= 4.hours
    from + rand(to - from)
  end

  def random_visit_before(from, to)
    # don't place constraints close to the edges
    from += 1.hour
    to -= 20.minutes
    from + rand(to - from)
  end

  def add_random_visit_constraint(trip_item:, trip_start:, trip_end:,
                                  trip_item_constraint_probability:)
    after  = if rand < trip_item_constraint_probability
               random_visit_after(trip_start, trip_end)
             else
               nil
             end
    before = if rand < trip_item_constraint_probability
               random_visit_before((after || trip_start), trip_end)
             else
               nil
             end
    return if after.nil? && before.nil?
    VisitConstraint.create(visit_after: after,
                           visit_before: before,
                           trip_item: trip_item)
  end

  def generate_bookings_trips_and_trip_items(days_since_first_booking:,
                                             average_new_bookings_per_day:,
                                             trip_start:, trip_end:,
                                             trip_item_constraint_probability:)
    # cursor date - each iteration it's increased by one
    date = Time.zone.today - days_since_first_booking
    days_since_first_booking.times do
      # like Customer.in_booking, but at `date_at` instead of today
      in_booking_at_date = lambda do |customer, date_at|
        # true if each of its bookings is delivered
        delivered_each = customer.bookings.map do |b|
          # in booking if the delivery date is in the future
          if b.delivery_trip_item.nil?
            true
          else
            b.delivery_trip_item.trip.date >= date_at
          end
        end
        ([false] + delivered_each).reduce { |bool1, bool2| bool1 || bool2 }
      end

      (average_new_bookings_per_day + rand(-3..3)).times do
        customer = nil
        loop do
          customer = Customer.non_system.sample
          break unless in_booking_at_date.call(customer, date)
        end

        booking = customer.bookings.create(
          # for Statistics
          created_at: random_date(date - 3, date),
          collection_trip_item_attributes: {
            trip_date: date,
            address: customer.addresses.sample
          }
        )
        add_random_visit_constraint(
          trip_item: booking.collection_trip_item,
          trip_start: trip_start,
          trip_end: trip_end,
          trip_item_constraint_probability: trip_item_constraint_probability
        )

        # book delivery for all collected trip_items
        next if date >= Time.zone.today
        booking.update(delivery_trip_item_attributes:
                         { trip_date: date + rand(1..2),
                           address: customer.addresses.sample })
        add_random_visit_constraint(
          trip_item: booking.delivery_trip_item,
          trip_start: trip_start,
          trip_end: trip_end,
          trip_item_constraint_probability: trip_item_constraint_probability
        )
      end

      date += 1
    end
  end

  # returns in estimated distance_seconds
  def dist_euclidean_by_geocode(gc1, gc2)
    eucl = Math.sqrt(gc1.zip(gc2).map { |x_i, y_i| (x_i - y_i)**2 }.sum)
    # 13752 is the average, with min of ~6k and max of ~80k
    eucl * 13_752
  end

  # Starts with the Euclidean distance between the two geocodes
  # Then scales by a random number
  # The result is an asymmetric matrix
  def dist_randomized(addr1, addr2)
    (gc1, gc2) = [addr1, addr2].map { |a| a.geocode.split(',').map(&:to_f) }
    dist_euclidean_by_geocode(gc1, gc2) * (rand(7..15) / 10.0)
  end

  def generate_address_distances(probability_of_missing_distance:)
    addresses = Address.all
    sql = 'INSERT INTO address_distances ' \
        + '(from_address_id, to_address_id, distance_seconds) VALUES '
    # slow solution without raw SQL
    #    address_distances = []
    addresses.each do |addr1|
      addresses.each do |addr2|
        # distance to itself is always 0 and never missing
        if addr1.id == addr2.id
          sql += "(#{addr1.id}, #{addr2.id}, 0), "
#          address_distances << {
#            from_address_id: addr1.id,
#            to_address_id: addr2.id,
#            distance_seconds: 0
#          }
        elsif rand > probability_of_missing_distance
          sql += "(#{addr1.id}, #{addr2.id}, " \
               + "#{dist_randomized(addr1, addr2)}), "
#          address_distances << {
#            from_address_id: addr1.id,
#            to_address_id: addr2.id,
#            distance_seconds: dist_euclidean(addr1, addr2)
#          }
        end
      end
    end
#    AddressDistance.create address_distances

    sql = sql[0...-2] # remove trailing comma
    ActiveRecord::Base.connection.execute(sql)
  end

  # to ensure that optimize will work immediately
  # note that most missing distances between addresses will remain missing
  # because they won't be on the same trip
  def insert_trips_missing_distances_and_driver
    driver = DriverAccount.first
    Trip.all.each do |trip|
      # fill in missing address distances
      missing = AddressDistance.missing_records(trip.trip_items.addresses)
      address_distances = []
      missing.each do |src, dests|
        dests.each do |dest|
          address_distances << {
            from_address_id: src,
            to_address_id: dest,
            distance_seconds:
              dist_randomized(Address.find(src), Address.find(dest))
          }
        end
      end
      AddressDistance.create(address_distances)
      trip.update(driver_account: driver)
    end
  end

  def generate_ironing_data_for_collected(laundry_probability:)
    Booking.select(&:collected?).each do |booking|
      ironing = booking.ironing
      # add ironer_account, time_ironed, bag_count, has_hangers, date_ironed
      ironer = IronerAccount.all.sample
      minutes_ironed = rand(30..240)
      bag = rand(0..3)
      hangers = [true, false].sample
      date_ironed = booking.collection_trip_item.trip.date + 1
      ironing.ironing_tasks.create(ironer_account: ironer,
                                   minutes_ironed: minutes_ironed)
      ironing.update(bag_count: bag,
                     has_hangers: hangers,
                     date_ironed: date_ironed)

      # create booking_services
      if booking.customer.flat_rate?
        hours_ironed = rand(30..240) / 60.0 # 0.5-4 hours
        ironing.booking_services.create(service: Service.ironing_per_hour,
                                        count: hours_ironed)
      else
        Service.non_system.sample(rand(1..6)).each do |service|
          count = rand(1..8)
          ironing.booking_services.create(service: service,
                                          count: count)
        end
      end
      if rand < laundry_probability
        ironing.booking_services.create(service: Service.washing_per_laundry,
                                        count: rand(1..6))
      end
      ironing.apply_minimum_charge
    end
  end

  # simulate driving in a straight line between geocodes
  def generate_driver_locations(seconds_between_geocode_signal:)
    sql = 'INSERT INTO driver_locations ' \
        + '(trip_id, trip_item_id, lat, long, created_at) VALUES '
    # slow solution without raw SQL
    # driver_locations = []
    Trip.where(completed: true).each do |trip|
      base_geocode = [Address.main_for_base.geocode]
      geocodes = (
        trip.trip_items.map { |ti| ti.address.geocode } + base_geocode
      ).map { |gc| gc.split(',').map(&:to_f) }
      # the base geocode
      current_gc = geocodes.last
      # a cursor variable which increases in a loop
      current_time = trip.start_time
      geocodes.each do |gc|
        dist_seconds = dist_euclidean_by_geocode(current_gc, gc)
        number_of_steps = (dist_seconds / seconds_between_geocode_signal).ceil
        step_size = current_gc.zip(gc).map do |from, to|
          (to - from) / number_of_steps
        end

        number_of_steps.times do
          current_gc = current_gc.zip(step_size).map(&:sum) # increment
          current_time += seconds_between_geocode_signal
          sql += "(#{trip.id}, 0, '#{current_gc[0]}', "
          sql += "'#{current_gc[1]}', '#{current_time}'), "
#          driver_locations << {
#            trip_id: trip.id,
#            trip_item_id: 0,
#            lat: current_gc[0],
#            long: current_gc[1],
#            created_at: current_time
#          }
        end
        current_time += TripItem.seconds_between_trip_items
      end
    end
#    DriverLocation.create driver_locations

    sql = sql[0...-2] # remove trailing comma
    ActiveRecord::Base.connection.execute(sql)
  end

  # generates ExpenseTypes and several Expenses: periodic and one-offs
  def generate_expenses(number_of_expense_types:, start_date:)
    (1..number_of_expense_types).each do |i|
      expense_type = ExpenseType.create(name: "Expense #{i}")
      number_of_expenses = rand(4)
      (1..number_of_expenses).each do |_|
        cost = (rand * 100).round
        first_paid = random_date(start_date, Time.zone.today)
        if rand > 0.5 # one-time expense
          Expense.create(expense_type: expense_type,
                         cost: cost,
                         from: first_paid,
                         to: first_paid)
        else # periodic expense
          last_paid = random_date(first_paid, Time.zone.today)
          Expense.create(expense_type: expense_type,
                         cost: cost,
                         from: first_paid,
                         to: last_paid,
                         period: 1,
                         period_type: 0)
        end
      end
    end
  end

  ### not used currently
  # center of London
#  GEOCODE_CENTER = [51.510040503715, -0.13176789350587]
  # distance to boundaries of London
#  GEOCODE_VARIANCE = [0.09, 0.19]
#  def random_geocode(center = GEOCODE_CENTER, variance = GEOCODE_VARIANCE)
#    center.zip(variance)                            # [[c0, v0], [c1, v1]]
#          .map{ |c, v| c + (v * (rand - 0.5) * 2) } # [lng, lat]
#          .join(',')                                # lng,lat
#  end

  # Uniform random distribution: each date in interval is equally likely
  def random_date(from, to)
    from + rand(to - from + 1)
  end
end
