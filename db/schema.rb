# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_23_105942) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "address_distances", force: :cascade do |t|
    t.integer "from_address_id"
    t.integer "to_address_id"
    t.float "distance_seconds"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "meters"
    t.index ["from_address_id", "to_address_id"], name: "index_address_distances_on_from_address_id_and_to_address_id", unique: true
    t.index ["from_address_id"], name: "index_address_distances_on_from_address_id"
    t.index ["to_address_id"], name: "index_address_distances_on_to_address_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.integer "customer_id"
    t.string "address_name"
    t.string "post_code"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "notes"
    t.boolean "active"
    t.string "geocode"
  end

  create_table "booking_availabilities", force: :cascade do |t|
    t.date "date", null: false
    t.boolean "available", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date"], name: "index_booking_availabilities_on_date"
  end

  create_table "booking_services", force: :cascade do |t|
    t.float "count"
    t.integer "ironing_id"
    t.integer "service_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.float "discount"
    t.index ["ironing_id"], name: "index_booking_services_on_ironing_id"
    t.index ["service_id"], name: "index_booking_services_on_service_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "collection_trip_item_id"
    t.integer "delivery_trip_item_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.float "discount"
    t.index ["customer_id"], name: "index_bookings_on_customer_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "customer_registrations", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "first_name"
    t.string "last_name"
    t.string "phone_mobile"
    t.string "email"
    t.string "encrypted_password"
    t.integer "payment_scheme"
    t.string "address"
    t.string "post_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "collection_dates", array: true
    t.date "delivery_dates", array: true
    t.string "request_requirements"
    t.string "phone_home"
    t.index ["customer_id"], name: "index_customer_registrations_on_customer_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "nickname"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "phone_home"
    t.string "phone_mobile"
    t.string "notes"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "payment_scheme"
    t.float "discount"
    t.boolean "active"
    t.integer "bookings_count", default: 0
    t.decimal "money_owed", default: "0.0"
    t.string "confirmation_code"
    t.datetime "confirmation_code_generated_at", precision: nil
    t.string "encrypted_password"
    t.datetime "first_login", precision: nil
    t.index ["email"], name: "index_customers_on_email"
    t.index ["nickname"], name: "index_customers_on_nickname"
    t.index ["phone_home"], name: "index_customers_on_phone_home"
    t.index ["phone_mobile"], name: "index_customers_on_phone_mobile"
  end

  create_table "driver_accounts", force: :cascade do |t|
    t.float "pay_rate"
    t.boolean "active"
    t.integer "staff_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["staff_id"], name: "index_driver_accounts_on_staff_id"
  end

  create_table "driver_locations", force: :cascade do |t|
    t.integer "trip_id"
    t.integer "trip_item_id"
    t.string "lat"
    t.string "long"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.float "distance_seconds"
    t.index ["distance_seconds"], name: "index_driver_locations_on_distance_seconds"
    t.index ["trip_id"], name: "index_driver_locations_on_trip_id"
    t.index ["trip_item_id"], name: "index_driver_locations_on_trip_item_id"
  end

  create_table "expense_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "expenses", force: :cascade do |t|
    t.float "cost"
    t.integer "expense_type_id"
    t.integer "period"
    t.date "from"
    t.date "to"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "period_type"
    t.index ["expense_type_id"], name: "index_expenses_on_expense_type_id"
  end

  create_table "good_job_batches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.jsonb "serialized_properties"
    t.text "on_finish"
    t.text "on_success"
    t.text "on_discard"
    t.text "callback_queue_name"
    t.integer "callback_priority"
    t.datetime "enqueued_at"
    t.datetime "discarded_at"
    t.datetime "finished_at"
  end

  create_table "good_job_processes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "state"
  end

  create_table "good_job_settings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "key"
    t.jsonb "value"
    t.index ["key"], name: "index_good_job_settings_on_key", unique: true
  end

  create_table "good_jobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "queue_name"
    t.integer "priority"
    t.jsonb "serialized_params"
    t.datetime "scheduled_at"
    t.datetime "performed_at"
    t.datetime "finished_at"
    t.text "error"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "active_job_id"
    t.text "concurrency_key"
    t.text "cron_key"
    t.uuid "retried_good_job_id"
    t.datetime "cron_at"
    t.uuid "batch_id"
    t.uuid "batch_callback_id"
    t.index ["active_job_id", "created_at"], name: "index_good_jobs_on_active_job_id_and_created_at"
    t.index ["active_job_id"], name: "index_good_jobs_on_active_job_id"
    t.index ["batch_callback_id"], name: "index_good_jobs_on_batch_callback_id", where: "(batch_callback_id IS NOT NULL)"
    t.index ["batch_id"], name: "index_good_jobs_on_batch_id", where: "(batch_id IS NOT NULL)"
    t.index ["concurrency_key"], name: "index_good_jobs_on_concurrency_key_when_unfinished", where: "(finished_at IS NULL)"
    t.index ["cron_key", "created_at"], name: "index_good_jobs_on_cron_key_and_created_at"
    t.index ["cron_key", "cron_at"], name: "index_good_jobs_on_cron_key_and_cron_at", unique: true
    t.index ["finished_at"], name: "index_good_jobs_jobs_on_finished_at", where: "((retried_good_job_id IS NULL) AND (finished_at IS NOT NULL))"
    t.index ["priority", "created_at"], name: "index_good_jobs_jobs_on_priority_created_at_when_unfinished", order: { priority: "DESC NULLS LAST" }, where: "(finished_at IS NULL)"
    t.index ["queue_name", "scheduled_at"], name: "index_good_jobs_on_queue_name_and_scheduled_at", where: "(finished_at IS NULL)"
    t.index ["scheduled_at"], name: "index_good_jobs_on_scheduled_at", where: "(finished_at IS NULL)"
  end

  create_table "ironer_accounts", force: :cascade do |t|
    t.float "pay_rate"
    t.boolean "active"
    t.integer "staff_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["staff_id"], name: "index_ironer_accounts_on_staff_id"
  end

  create_table "ironing_tasks", force: :cascade do |t|
    t.integer "ironer_account_id"
    t.integer "ironing_id"
    t.integer "minutes_ironed"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["ironer_account_id"], name: "index_ironing_tasks_on_ironer_account_id"
    t.index ["ironing_id"], name: "index_ironing_tasks_on_ironing_id"
  end

  create_table "ironings", force: :cascade do |t|
    t.integer "bag_count"
    t.boolean "has_hangers"
    t.integer "booking_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.date "date_ironed"
    t.index ["booking_id"], name: "index_ironings_on_booking_id"
  end

  create_table "jwt_denylist", force: :cascade do |t|
    t.string "jti", null: false
    t.datetime "exp", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["jti"], name: "index_jwt_denylist_on_jti"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "text", null: false
    t.boolean "seen", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["seen"], name: "index_notifications_on_seen"
  end

  create_table "push_notification_addresses", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "staff_id"
    t.string "endpoint"
    t.string "auth_key"
    t.string "p256dh_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_push_notification_addresses_on_customer_id"
    t.index ["staff_id"], name: "index_push_notification_addresses_on_staff_id"
  end

  create_table "requests", force: :cascade do |t|
    t.bigint "address_id"
    t.bigint "booking_id"
    t.boolean "active", default: true
    t.date "dates", null: false, array: true
    t.string "additional_requirements"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "delivery_dates", array: true
    t.index ["active"], name: "index_requests_on_active"
    t.index ["address_id"], name: "index_requests_on_address_id"
    t.index ["booking_id"], name: "index_requests_on_booking_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.boolean "active"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "category_id"
  end

  create_table "staff", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "username"
    t.boolean "active"
    t.string "first_name"
    t.string "last_name"
    t.string "phone_number"
    t.integer "permissions"
    t.integer "customers_per_page", default: 20
    t.index ["permissions"], name: "index_staff_on_permissions"
  end

  create_table "trip_items", force: :cascade do |t|
    t.integer "sequence"
    t.integer "address_id"
    t.integer "trip_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "notes"
    t.integer "state"
    t.integer "paid"
    t.index ["address_id"], name: "index_trip_items_on_address_id"
    t.index ["sequence"], name: "index_trip_items_on_sequence"
    t.index ["trip_id"], name: "index_trip_items_on_trip_id"
  end

  create_table "trips", force: :cascade do |t|
    t.date "date"
    t.time "start_time"
    t.boolean "completed"
    t.integer "driver_account_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.time "sched_deadline"
    t.index ["completed"], name: "index_trips_on_completed"
    t.index ["date"], name: "index_trips_on_date"
    t.index ["driver_account_id"], name: "index_trips_on_driver_account_id"
  end

  create_table "visit_constraints", force: :cascade do |t|
    t.time "visit_before"
    t.time "visit_after"
    t.integer "trip_item_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["trip_item_id"], name: "index_visit_constraints_on_trip_item_id"
  end

  add_foreign_key "customer_registrations", "customers"
  add_foreign_key "expenses", "expense_types"
  add_foreign_key "ironing_tasks", "ironer_accounts"
  add_foreign_key "ironing_tasks", "ironings"
  add_foreign_key "push_notification_addresses", "customers"
  add_foreign_key "push_notification_addresses", "staff"
  add_foreign_key "requests", "addresses"
  add_foreign_key "requests", "bookings"
end
