# Minimal required values for the app to function

# The values below are going to be different for different ironing companies
base_post_code = 'BR1 3LU'
base_geocode = '51.4158128,0.0240241'
ironing_per_hour_price = 13.0
missed_driver_fine_price = 10.0
laundry_price = 6.0
minimum_charge = 15.0

sys = Customer.create nickname: 'System',
                      payment_scheme: 'itemized',
                      active: true

if Address.for_base.count.zero?
  Address.create customer: sys,
                 post_code: base_post_code,
                 geocode: base_geocode,
                 active: true
end

system = Category.create name: 'System'

services = [
  { name: 'Ironing per hour',    price: ironing_per_hour_price },
  { name: 'Missed driver fine',  price: missed_driver_fine_price },
  { name: 'Washing per laundry', price: laundry_price },
  { name: 'Minimum charge',      price: minimum_charge },
  { name: 'Extra charge',        price: 1.0 }
]
Service.create(services) do |s|
  s.category = system
  s.active = true
end

if ENV.has_key?('ADMIN_PASSWORD')
  Staff.create(username: 'admin',
               first_name: 'Admin',
               password: ENV['ADMIN_PASSWORD'],
               password_confirmation: ENV['ADMIN_PASSWORD'],
               permissions: 1,
               active: true)
end
