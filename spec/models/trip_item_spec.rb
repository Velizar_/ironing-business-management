require 'spec_helper'

describe TripItem, type: :model do
  describe 'accurate_visit_time!' do
    it 'works' do
      Rails.application.load_seed # for System address
      time_now = Time.zone.now
      trip = Trip.create(date: Date.today,
                         start_time: time_now - 10.minutes,
                         sched_deadline: time_now - 20.minutes)
      (1..4).map { |_| FactoryBot.create(:trip_item, trip: trip) }
      trip.fix_sequences
      trip_items = trip.trip_items
      addr = trip_items.map(&:address)
      (0..4).to_a.product((0..4).to_a).map do |src_idx, dst_idx|
        if dst_idx == src_idx + 1
          dist = dst_idx * 60
        else
          dist = 0
        end
        AddressDistance.create(from_address: addr[src_idx],
                               to_address: addr[dst_idx],
                               distance_seconds: dist)
      end
      (1..11).each do |i|
        DriverLocation.create(trip: trip,
                              trip_item: trip_items[i / 5 + 1],
                              created_at: time_now - 10.minutes + (i * 30).seconds,
                              distance_seconds: 60)
      end
      # ETA: 2 minutes to trip_items[3]
      DriverLocation.create(trip: trip,
                            trip_item: trip_items[3],
                            created_at: time_now - 1.minutes,
                            distance_seconds: 180)
      allow(Time).to receive(:now).and_return time_now
      current_item_time = trip_items[3].accurate_visit_time!
      next_item_time = trip_items[4].accurate_visit_time!
      previous_item_time = trip_items[2].accurate_visit_time!
      expect(current_item_time[0]).to be_within(1.second).of time_now + 2.minutes
      expect(next_item_time[0]).to be_within(1.second).of time_now + 11.minutes
      expect(previous_item_time[0]).to be_within(1.second).of trip_items[2].visit_time!
      expect(previous_item_time[1]).to eq("Completed (scheduled at #{trip_items[2].visit_time!.to_fs(:time)})")
    end
  end
end
