require 'spec_helper'

describe 'Trip pages' do
  before do
    Rails.application.load_seed # for System address
    log_in
  end

  let!(:trip) do
    FactoryBot.create(:trip, date: (Time.zone.today + 3))
  end

  let!(:driver_account) do
    staff = FactoryBot.create(:staff, first_name: 'John', last_name: 'Doe')
    FactoryBot.create(:driver_account, staff: staff)
  end

  let!(:inactive_driver_account) do
    staff = FactoryBot.create(:staff, first_name: 'Jane', last_name: 'Smith')
    FactoryBot.create(:driver_account, staff: staff, active: false)
  end

  def visit_trip(trip)
    gen_address_distances(trip)
    visit trips_path(date: trip.date.to_s)
  end

  feature 'Index' do
    it 'Renders trip items' do
      customer = FactoryBot.create(:addressless_customer, nickname: 'John')
      address = FactoryBot.create(:address, customer: customer, post_code: 'QW1 3ER')
      FactoryBot.create(:trip_item_with_booking, trip: trip, address: address, notes: 'lorem ipsum', sequence: 10)
      (1..4).each { |_| FactoryBot.create(:trip_item_with_booking, trip: trip) }
      visit_trip(trip)

      nickname = all('.trip-item-display .customer-link')[4].text
      booking_type = all('.trip-item-display .collection-or-delivery')[4].text
      post_code = all('.trip-item-display .post-code')[4].text
      trip_item_text = all('.trip-item-display')[4].text

      assert_equal 'John', nickname
      assert_equal 'collection', booking_type
      assert_equal 'QW1 3ER', post_code
      assert_match /lorem ipsum/, trip_item_text
    end

    it 'Gathers missing distances' do
      trip_item = FactoryBot.create(:trip_item_with_booking, trip: trip)
      addresses = [trip_item.address, Address.main_for_base]
      stub_replace(MapsWrapper, :distance_matrix) do |origins, destinations|
        origins.map { |_o| destinations.map { |d| [d.hash.abs, d.hash.abs.to_s[0..5].to_i] } }
      end

      assert_equal 0, AddressDistance.count
      visit trips_path(date: trip.date.to_s)
      assert_equal 4, AddressDistance.count
      relevant_distances = AddressDistance.where(from_address_id: addresses.map(&:id),
                                                 to_address_id: addresses.map(&:id))
      assert_equal 4, relevant_distances.count
    end

    it 'Supports multiple vehicles' do
      trip_2 = FactoryBot.create(:trip, date: (Time.zone.today + 3), start_time: '19:05:00')
      gen_address_distances(trip)
      gen_address_distances(trip_2)

      visit trips_path(date: trip.date.to_s)
      assert_equal '18:07', find('#0_trip_start_time').value
      assert_equal '19:05', find('#1_trip_start_time').value
    end

    it 'Does not list inactive drivers' do
      visit_trip(trip)
      displayed_accounts = all("#0_trip_driver_account_id option").map(&:text)
      assert_equal ['', 'John Doe'], displayed_accounts
    end

    it 'Lists inactive drivers only when looking at trips they are used in' do
      FactoryBot.create(:trip, date: (Time.zone.today + 3), driver_account: inactive_driver_account)
      visit_trip(trip)
      displayed_accounts_trip_1 = all("#0_trip_driver_account_id option").map(&:text)
      displayed_accounts_trip_2 = all("#1_trip_driver_account_id option").map(&:text)
      assert_equal ['', 'John Doe'],                          displayed_accounts_trip_1
      assert_equal ['', 'John Doe', 'Jane Smith (inactive)'], displayed_accounts_trip_2
    end

    it 'Updates the trip on form submission' do
      trip_items = (1..2).map { |i| FactoryBot.create(:trip_item_with_booking, trip: trip, sequence: i) }
      assert_equal [1, 2], trip_items.map(&:sequence)

      visit_trip(trip)
      find('#0_trip_trip_items_attributes_0_sequence', visible: false).set(2)
      find('#0_trip_trip_items_attributes_1_sequence', visible: false).set(1)
      click_button 'Update Trip'
      assert_equal [2, 1], trip_items.map { |ti| ti.reload.sequence}
    end

    it 'Renders address and customer info for JS code to use' do
      customer = FactoryBot.create(:addressless_customer, nickname: 'Bob')
      address = FactoryBot.create(:address, customer: customer, geocode: '52.345,-1.234')
      FactoryBot.create(:trip_item_with_booking, trip: trip, address: address)

      visit_trip(trip)
      assert_equal '52.345,-1.234', find('.geocode')['data-value']
      assert_equal 'Bob',           find('.nickname')['data-value']
    end

    it 'Renders driver locations for JS code to use' do
      DriverLocation.create(trip: trip, trip_item_id: 0, lat: '51.234', long: '0.012', created_at: '2019-12-28 12:34:04')
      DriverLocation.create(trip: trip, trip_item_id: 0, lat: '52.345', long: '-4.321', created_at: '2019-12-28 21:40:43')

      visit_trip(trip)
      assert_equal '51.234',   all('.driver-location-0')[0]['data-lat']
      assert_equal '0.012',    all('.driver-location-0')[0]['data-lng']
      assert_equal '12:34:04', all('.driver-location-0')[0]['data-created']

      assert_equal '52.345',   all('.driver-location-0')[1]['data-lat']
      assert_equal '-4.321',   all('.driver-location-0')[1]['data-lng']
      assert_equal '21:40:43', all('.driver-location-0')[1]['data-created']
    end

    it 'Renders distance matrix for JS code to use' do
      base_address = Address.main_for_base
      address = FactoryBot.create(:trip_item_with_booking, trip: trip).address

      AddressDistance.create(from_address: address, to_address: base_address, distance_seconds: 1)
      AddressDistance.create(from_address: base_address, to_address: address, distance_seconds: 2)
      AddressDistance.create(from_address: address, to_address: address, distance_seconds: 0)
      AddressDistance.create(from_address: base_address, to_address: base_address, distance_seconds: 0)

      visit trips_path(date: trip.date.to_s)
      assert_equal base_address.id, all('.address-distance-0')[0]['data-from'].to_i
      assert_equal base_address.id, all('.address-distance-0')[0]['data-to'].to_i
      assert_equal 0,               all('.address-distance-0')[0]['data-dist'].to_i

      assert_equal base_address.id, all('.address-distance-0')[1]['data-from'].to_i
      assert_equal address.id,      all('.address-distance-0')[1]['data-to'].to_i
      assert_equal 2,               all('.address-distance-0')[1]['data-dist'].to_i

      assert_equal address.id,      all('.address-distance-0')[2]['data-from'].to_i
      assert_equal base_address.id, all('.address-distance-0')[2]['data-to'].to_i
      assert_equal 1,               all('.address-distance-0')[2]['data-dist'].to_i

      assert_equal address.id,      all('.address-distance-0')[3]['data-from'].to_i
      assert_equal address.id,      all('.address-distance-0')[3]['data-to'].to_i
      assert_equal 0,               all('.address-distance-0')[3]['data-dist'].to_i
    end

    it 'Optimizes' do
      trip_item_2 = FactoryBot.create(:trip_item_with_booking, trip: trip, sequence: 1)
      trip_item_3 = FactoryBot.create(:trip_item_with_booking, trip: trip, sequence: 2)
      # distance of 10 between every pair of addresses
      gen_address_distances(trip)
      AddressDistance.find_by(from_address: trip_item_3.address,
                              to_address: trip_item_2.address
      ).update(distance_seconds: 1)

      visit trips_path(date: trip.date.to_s)
      find('#optimize-exactly').click
      assert_equal 1, trip_item_3.reload.sequence
      assert_equal 2, trip_item_2.reload.sequence
    end

    it 'Renders most of the page even if gathering distances fails' do
      address = FactoryBot.create(:address, post_code: '123 456')
      trip_item = FactoryBot.create(:trip_item_with_booking, trip: trip, address: address)
      AddressDistance.create(from_address: trip_item.address,
                             to_address: Address.main_for_base,
                             distance_seconds: 132)
      stub_replace(MapsWrapper, :distance_matrix) do |_, _|
        raise DistanceGatheringError.new('Ipsum gathering error')
      end

      visit trips_path(date: trip.date.to_s)
      assert_match /Ipsum gathering error/, find('.error-text').text
      assert_equal '123 456', find('.trip-item-display .post-code').text
      assert_empty all('.address-distance-0')
    end

    it 'Uses the correct base address even if a new one has been added' do
      initial_base = Address.main_for_base
      customer_address = FactoryBot.create(:trip_item_with_booking, trip: trip).address
      new_base = FactoryBot.build(:address, address_name: 'Replacement base address', customer: nil)
      initial_base.customer.addresses << new_base
      initial_base.update(active: false)

      AddressDistance.create(from_address: initial_base, to_address: customer_address, distance_seconds: 2)
      AddressDistance.create(from_address: new_base, to_address: customer_address, distance_seconds: 34)
      stub_replace(MapsWrapper, :distance_matrix) do |origins, destinations|
        origins.map { |_o| destinations.map { |d| [d.hash.abs, d.hash.abs.to_s[0..5].to_i] } }
      end

      visit trips_path(date: trip.date.to_s)
      assert_equal "Origin: #{initial_base.display}", find('.inactive_base_address_info').text

      assert_equal initial_base.id,     all('.address-distance-0')[1]['data-from'].to_i
      assert_equal customer_address.id, all('.address-distance-0')[1]['data-to'].to_i
      assert_equal 2,                   all('.address-distance-0')[1]['data-dist'].to_i
      assert_equal 4,                   all('.address-distance-0').count
    end
  end

  feature 'Printable' do
    it 'Should load with all kinds of data' do
      trip_items = (1..7).map do |_|
        FactoryBot.create(:trip_item, trip: trip)
      end

      bookings = []
      # Note: changes the trip_item addresses
      trip_items[0..3].each do |ti|
        bookings << FactoryBot.create(:booked_booking,
                                       collection_trip_item: ti,
                                       delivery_trip_item: nil)
      end
      trip_items[4..6].each do |ti|
        bookings << FactoryBot.create(:ironed_booking,
                                       delivery_trip_item: ti)
      end
      trip_items[1].update notes: 'dolor'
      gen_address_distances(trip)

      svc  = FactoryBot.create(:service, price: 1.0)
      svc2 = FactoryBot.create(:service, price: 2.0)
      Service.ironing_per_hour.update price: 5.0
      Service.extra_charge.update     price: 1.0
      Service.minimum_charge.update   price: 10.0

      bookings[4].customer.update payment_scheme: :flat_rate
      bookings[5].customer.update payment_scheme: :itemized

      i = bookings[4].ironing
      i.booking_services.create(count: 2.0, service: Service.ironing_per_hour)
      i.booking_services.create(count: 3.1, service: Service.extra_charge)
      i.update bag_count: 2
      i.update has_hangers: true
      expect(i.total_cost).to be_within(0.001).of(13.1)
      i1 = i

      i = bookings[5].ironing
      i.booking_services.create(count: 3,   service: svc)
      i.booking_services.create(count: 1,   service: svc2)
      i.booking_services.create(count: 0.5, service: Service.minimum_charge)
      i.update bag_count: 0
      i.update has_hangers: true
      expect(i.total_cost).to be_within(0.001).of(10.0)
      i2 = i

      i = bookings[6].ironing
      i.update bag_count: 1
      i.update has_hangers: false

      visit printable_trip_path(trip)
      # verify the number of trip items
      expect(page).to     have_selector('p', text: '7. ')
      expect(page).not_to have_selector('p', text: '8. ')

      expect(page).to     have_selector('p', text: 'dolor')

      expect(page).to have_selector('p', text: "£#{i1.total_cost}")
      expect(page).to have_selector('p', text: "£#{i2.total_cost}")
    end

    it 'Should cope with missing distances' do
      FactoryBot.create(:trip_item_with_booking, trip: trip, notes: 'sit amet')
      stub_replace(MapsWrapper, :distance_matrix) do |_, _|
        raise DistanceGatheringError.new('Dolor error gathering')
      end
      visit printable_trip_path(trip)

      assert_match /Dolor error gathering/, find('.error-text').text
      expect(page).to have_selector('p', text: 'sit amet')
    end
  end
end
