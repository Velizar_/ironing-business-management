require 'spec_helper'

describe 'Multi-page features', type: :feature do
  before do
    # for system services and category
    Rails.application.load_seed

    log_in
  end

  let(:ironing) { FactoryBot.create(:blank_completed_ironing) }

  scenario 'Ironing service price change' do
    # Create an ironing with my service
    svc_price = 1.0
    svc_new_price = 1.5
    svc_name = 'My service'
    service = FactoryBot.create(:service, price: svc_price, name: svc_name)
    svc_count = 1
    ironing.booking.customer.update payment_scheme: :itemized
    ironing.booking_services.create(service: service, count: svc_count)

    # Change service price
    service.update_price(svc_new_price)
    ironing.reload
    new_service = Service.find_by(name: svc_name)

    # Assert that DB is working correctly
    expect(ironing.total_cost).to be_within(0.0001).of(svc_price * svc_count)
    expect(ironing.booking_services.count).to eq(1)
    s = ironing.booking_services.first.service
    expect(s.id).to       eq(service.id)
    expect(s.price).to    eq(svc_price)
    expect(s.name).not_to eq(svc_name)

    selectable_system_services = [
      Service.missed_driver_fine,
      Service.washing_per_laundry,
      Service.extra_charge
    ]

    # Assert that new ironing only displays the new service
    new_ironing = FactoryBot.create(:blank_ironing)
    new_ironing.booking.customer.update payment_scheme: :itemized
    category_name = service.category.name

    visit new_ironing_path(new_ironing)
    select = page.find(
      :css,
      'select#ironing_booking_services_attributes_0_service_id'
    )
    assert_group_select_elements(select,
                                 'System' => selectable_system_services,
                                 category_name => [new_service])

    # Assert that page is displaying the old service too
    service.reload
    new_service.reload
    ironing.booking_services.reload
    ironing.reload
    visit edit_ironing_path(ironing)
    select = page.find(
      :css,
      'select#ironing_booking_services_attributes_0_service_id'
    )
    assert_group_select_elements(select,
                                 'System' => selectable_system_services,
                                 category_name => [service, new_service])
  end

  scenario 'Ironing per hour price change' do
    # Create an ironing with my service
    service = Service.ironing_per_hour
    svc_price = service.price
    svc_new_price = svc_price + 1
    svc_name = service.name
    ironing.booking.customer.update(payment_scheme: :flat_rate)
    ironing.booking_services.create(service: service, count: 0.5)

    # Change service price
    service.update_price(svc_new_price)
    ironing.reload
    new_service = Service.find_by(name: svc_name)

    # Assert that DB is working correctly
    expect(ironing.total_cost).to be_within(0.0001).of(svc_price * 0.5)
    expect(ironing.booking_services.count).to eq(1)
    s = ironing.booking_services.first.service
    expect(s.id).to       eq(service.id)
    expect(s.price).to    eq(svc_price)
    expect(s.name).not_to eq(svc_name)

    # New ironings are created with the new service
    new_ironing = FactoryBot.create(:blank_ironing)
    new_ironing.booking.customer.update payment_scheme: :flat_rate

    visit new_ironing_path(new_ironing)
    ironer_staff = IronerAccount.first.staff
    ironer_name = ironer_staff.first_name + ' ' + ironer_staff.last_name
    select ironer_name,                    from: 'Ironer account'
    fill_in 'Time ironed (hours:minutes)', with: '00:30'
    fill_in 'Bag count',                   with: '0'
    uncheck 'Apply minimum charge'
    # equivalent to fill in Ironing per hour with 02:00
    find(
      :css,
      '#ironing_booking_services_attributes_0_count',
      visible: false
    ).set(2)
    click_button 'Submit ironing'
    ironing.reload

    expect(new_ironing.booking_services.first.service).to eq(new_service)

    # Assert that the page is (properly) displaying the old service
    service.reload
    new_service.reload
    ironing.booking_services.reload
    ironing.reload
    visit edit_ironing_path(ironing)
    selected = page.find('#ironing_booking_services_attributes_0_service_id')
                   .find('option[selected]').text
    expect(selected).to start_with('Ironing per hour (expired at: ')
    expect(page.find('#ironing_booking_services_attributes_0_count').value).to eq('0.5')
  end
end
