require 'spec_helper'

describe 'Ironing pages', type: :feature do
  before do
    Rails.application.load_seed # for system services and category
    log_in
  end

  let(:customers) do
    (0..5).map do |idx|
      FactoryBot.create(:customer, nickname: "Customer #{idx}", active: true)
    end
  end

  # waiting ironing input
  let(:pending_ironing) do
    customers[1..3].map do |c|
      FactoryBot.create(:collected_booking, customer: c).ironing
    end
  end

  # can be edited
  let(:done_ironing) do
    customers[4..5].map do |c|
      # blank_completed_ironing, no booking_services
      FactoryBot.create(:ironed_booking, customer: c).ironing
    end
  end

  feature 'Index' do
    # should not be shown
    let!(:irrelevant_ironing) do
      [FactoryBot.create(:booked_booking,    customer: customers[0]).ironing,
       FactoryBot.create(:delivered_booking, customer: customers[1]).ironing,
       FactoryBot.create(:delivered_booking, customer: customers[2]).ironing]
    end

    # should be shown with editables
    let!(:need_extra_info_ironing) do
      i = FactoryBot.create(:ironed_booking, customer: customers[0]).ironing
      i.booking_services.create(service: Service.ironing_per_hour, count: 0)
      i
    end

    before do
      # let is lazy
      pending_ironing
      done_ironing
    end

    it 'should display correct ironing links' do
      visit ironing_index_path

      irrelevant_ironing.each do |i|
        expect(page).not_to have_link('Add information',
                                      href: new_ironing_path(i))
        expect(page).not_to have_link('Edit',
                                      href: edit_ironing_path(i))
      end

      pending_ironing.each do |i|
        expect(page).to     have_link('Add information',
                                      href: new_ironing_path(i))
        expect(page).not_to have_link('Edit',
                                      href: edit_ironing_path(i))
      end

      done_ironing.each do |i|
        expect(page).not_to have_link('Add information',
                                      href: new_ironing_path(i))
        expect(page).to     have_link('Edit',
                                      href: edit_ironing_path(i))
      end

      expect(page).to have_link('Add extra information', href:
        edit_ironing_path(need_extra_info_ironing))

      expect(page).to have_link('Add information',       count: 3)
      expect(page).to have_link('Add extra information', count: 1)
      expect(page).to have_link('Edit',                  count: 2)
    end
  end

  feature 'Create and edit' do
    let!(:ironing_services_1) do
      c1 = Category.create name: 'Cat 1'

      [FactoryBot.create(:service, name: 'Svc 1', category: c1),
       FactoryBot.create(:service, name: 'Svc 2', category: c1),
       FactoryBot.create(:service, name: 'Svc 3', category: c1)]
    end

    let!(:ironing_services_2) do
      c2 = Category.create name: 'Cat 2'

      [FactoryBot.create(:service, name: 'Svc 4', category: c2)]
    end

    let(:selectable_system_services) do
      [Service.missed_driver_fine,
       Service.washing_per_laundry,
       Service.extra_charge]
    end

    let(:ironer_account_names) do
      ['Steve Austin', 'Chuck Norris', 'Iron Man']
    end

    before do
      ironer_account_names.each do |name|
        first, last = name.split(' ')
        staff = FactoryBot.create(:staff, first_name: first, last_name: last)
        FactoryBot.create(:ironer_account, staff: staff, active: true)
      end
    end

    feature 'Create' do
      let(:ironing) { pending_ironing[0] }

      it 'displays all ironer accounts' do
        visit new_ironing_path(ironing)
        expect(page).to have_select(
          'ironing_ironing_tasks_attributes_0_ironer_account_id',
          options: [''] + ironer_account_names
        )
      end

      # Note: With flat rate is tested on edit
      it 'displays all the correct services with itemized scheme' do
        ironing.booking.customer.update payment_scheme: :itemized
        visit new_ironing_path(ironing)

        select = page.find(
          :css,
          'select#ironing_booking_services_attributes_0_service_id'
        )
        assert_group_select_elements(
          select,
          'System' => selectable_system_services,
          'Cat 1'  => ironing_services_1,
          'Cat 2'  => ironing_services_2
        )
      end

      it 'correctly creates with itemized scheme' do
        svc = Service.find_by(name: 'Svc 1')
        svc_count = 2.5
        svc_total_cost = Service.find_by(name: svc.name).price * svc_count
        min_charge_price = Service.minimum_charge.price
        # if this fails, change the data so that it passes
        expect(svc_total_cost).to be <= min_charge_price # (discount = 0.0)

        ironing.booking.customer.update payment_scheme: :itemized
        visit new_ironing_path(ironing)

        select ironer_account_names[0],        from: 'Ironer account'
        fill_in 'Time ironed (hours:minutes)', with: '01:31'
        fill_in 'Bag count',                   with: '1'
        check   'Has hangers'
        # leave Apply minimum charge checked
        select svc.name,                       from: 'Service'
        fill_in 'Count',                       with: svc_count
        # leave Discount (%) to 0
        click_button 'Submit ironing'
        ironing.reload

        tasks = ironing.ironing_tasks
        ironer_staff = tasks[0].ironer_account.staff
        ironer_name = "#{ironer_staff.first_name} #{ironer_staff.last_name}"
        bs = ironing.booking_services
        min_charge_bs = bs.find_by(service: Service.minimum_charge)
        svc_bs        = bs.find_by(service: svc)

        expect(ironer_name).to              eq(ironer_account_names[0])

        expect(tasks.size).to               eq(1)
        expect(tasks[0].minutes_ironed).to  eq(91)
        expect(ironing.bag_count).to        eq(1)
        expect(ironing.has_hangers).to      eq(true)
        expect(ironing.date_ironed).to      eq(Time.zone.today)

        expect(bs.count).to                 eq(2)
        expect(svc_bs.discount).to          eq(0)
        expect(svc_bs.count).to             eq(svc_count)
        min_charge_applied = min_charge_bs.total_cost
        min_charge_expected = Service.minimum_charge.price - svc_total_cost
        expect(min_charge_applied).to(
          be_within(0.001).of(min_charge_expected)
        )
      end

      it 'correctly creates with flat rate scheme' do
        # amount charged to customer, not paid to ironer
        # it's equal to 1 hour 45 minutes
        ironing_svc_hours = 1.75

        ironing.booking.customer.update payment_scheme: :flat_rate
        visit new_ironing_path(ironing)

        select ironer_account_names[1],        from: 'Ironer account'
        fill_in 'Time ironed (hours:minutes)', with: '02:00'
        fill_in 'Bag count',                   with: '2'
        # leave Has hangers unchecked
        uncheck 'Apply minimum charge'
        # equivalent to fill in Ironing per hour with 01:45
        find(
          :css,
          '#ironing_booking_services_attributes_0_count',
          visible: false
        ).set(ironing_svc_hours)
        fill_in 'Discount (%)', with: '50'
        click_button 'Submit ironing'
        ironing.reload

        tasks = ironing.ironing_tasks
        ironer_staff = tasks[0].ironer_account.staff
        ironer_name = "#{ironer_staff.first_name} #{ironer_staff.last_name}"
        bs = ironing.booking_services
        svc_bs = bs.first

        expect(ironer_name).to              eq(ironer_account_names[1])

        expect(tasks.size).to               eq(1)
        expect(tasks[0].minutes_ironed).to  eq(120)
        expect(ironing.bag_count).to        eq(2)
        expect(ironing.has_hangers).to      eq(false)
        expect(ironing.date_ironed).to      eq(Time.zone.today)

        expect(bs.count).to         eq(1)
        expect(svc_bs.discount).to  be_within(0.001).of(50.0)
        expect(svc_bs.count).to     be_within(0.001).of(ironing_svc_hours)
      end
    end

    feature 'Edit' do
      let(:ironing) { done_ironing[0] }

      it 'correctly edits and displays correct values afterwards' do
        Service.ironing_per_hour.update price: 0.05
        date = Time.zone.today - 1
        # amount charged to customer, not paid to ironer
        # it's equal to 1 hour 45 minutes
        ironing_svc_hours = 1.75

        ironing.booking.customer.update payment_scheme: :flat_rate
        visit edit_ironing_path(ironing)

        select ironer_account_names[1],        from: 'Ironer account'
        fill_in 'Time ironed (hours:minutes)', with: '02:00'
        fill_in 'Bag count',                   with: '2'
        uncheck 'Has hangers'
        fill_in 'Date ironed', with: date.to_s
        uncheck 'Apply minimum charge'
        find(
          :css,
          '#ironing_booking_services_attributes_0_count',
          visible: false
        ).set(ironing_svc_hours)
        fill_in 'Discount (%)', with: '50'
        click_button 'Submit ironing'
        ironing.reload

        ### Assert values in DB ###
        tasks = ironing.ironing_tasks
        ironer_staff = tasks[0].ironer_account.staff
        ironer_name = "#{ironer_staff.first_name} #{ironer_staff.last_name}"
        bs = ironing.booking_services
        svc_bs = bs.first

        expect(ironer_name).to              eq(ironer_account_names[1])

        expect(tasks.size).to               eq(1)
        expect(tasks[0].minutes_ironed).to  eq(120)
        expect(ironing.bag_count).to        eq(2)
        expect(ironing.has_hangers).to      eq(false)
        expect(ironing.date_ironed).to      eq(date)

        expect(bs.count).to         eq(1)
        expect(svc_bs.discount).to  be_within(0.001).of(50.0)
        expect(svc_bs.count).to     be_within(0.001).of(ironing_svc_hours)

        ### Assert values on edit page ###
        visit edit_ironing_path(ironing)

        expect(page).to have_select('Ironer account',
                                    selected: ironer_account_names[1])
        expect(page).to have_field('Time ironed (hours:minutes)',
                                   with: '02:00')
        expect(page).to have_field('Bag count', with: '2')
        expect(page).to have_unchecked_field('Apply minimum charge')
        expect(page).to have_field('Date ironed', with: date.to_s)
        expect(page).to have_unchecked_field 'Has hangers'
        expect(page).to have_css "#ironing_per_hour_field[value='01:45']"
        expect(page).to have_field 'Discount (%)', with: '50.0'
      end

      it 'For flat_rate only allows selectable_system_services' do
        ironing.booking.customer.update payment_scheme: :flat_rate
        ironing.booking_services.create(service: Service.extra_charge, count: 1)
        visit edit_ironing_path(ironing)

        select = page.find(
          :css,
          'select#ironing_booking_services_attributes_0_service_id'
        )
        assert_group_select_elements(select,
                                     'System' => selectable_system_services)
      end

      it 'Correctly displays multiple ironing tasks' do
        ironing.ironing_tasks[0].update(minutes_ironed: 256) # "04:16"
        FactoryBot.create(:ironing_task,
                           ironing: ironing,
                           minutes_ironed: 195) # "03:15"
        visit edit_ironing_path(ironing)

        expect(page).to have_field 'Time ironed (hours:minutes)',
                                   with: '04:16'
        expect(page).to have_field 'Time ironed (hours:minutes)',
                                   with: '03:15'
      end

      feature 'With inactive ironers' do
        # include factory accounts
        let(:ironer_names) do
          ironer_account_names + ['fname lname', 'fname lname']
        end

        let(:active_accounts) { ironer_names.drop(1) }
        let!(:inactive_account) do
          inactive_name = ironer_names[0]
          first, last = inactive_name.split(' ')
          ironer = Staff.find_by(
            first_name: first,
            last_name: last
          ).ironer_account
          ironer.update active: false
          ironer
        end

        it 'Does not display inactive ironer accounts' do
          visit edit_ironing_path(ironing)
          expect(page).to have_select(
            'ironing_ironing_tasks_attributes_0_ironer_account_id',
            options: [''] + active_accounts
          )
        end

        it 'Displays ironer account as inactive if it is the value' do
          ironing.ironing_tasks[0].update(ironer_account: inactive_account)
          visit edit_ironing_path(ironing)
          expected_options = [''] +
                             active_accounts +
                             [inactive_account.staff.full_name + ' (inactive)']
          expect(page).to have_select(
            'ironing_ironing_tasks_attributes_0_ironer_account_id',
            options: expected_options
          )
        end
      end
    end

    feature 'Receipt' do
      it 'Displays total cost' do
        ironing = done_ironing[0]
        ironing.booking_services.create(service: Service.extra_charge, count: 1)
        visit ironing_receipt_path(ironing)
        expect(page).to have_selector('p', text: ironing.total_cost)
      end
    end
  end
end
