require 'spec_helper'

describe 'Service pages', type: :feature do
  before do
    # for system category and services
    Rails.application.load_seed

    log_in
  end

  let!(:category) { Category.create name: 'category' }

  let(:service) do
    s = Service.create name: 'service',
                       category: category,
                       price: 1,
                       active: true
    FactoryBot.create(:booking_service, service: s)
    s
  end

  let(:service_2) do
    Service.create name: 'service_2',
                   category: category,
                   price: 2,
                   active: true
  end

  let(:system_services) { Service.all_system }

  def visit_index
    # let is lazy
    service
    service_2

    visit services_path
  end

  feature 'Index' do
    before { visit_index }

    it 'should list regular services in table' do
      [service, service_2].each do |s|
        expect(page).to have_selector('.non-system-services td', text: s.name)
      end
    end

    it 'should not list system services in table' do
      expect(system_services.size).to eq(5) # unrelated spec sanity check
      system_services.each do |s|
        expect(page).not_to(
          have_selector('.non-system-services td', text: s.name)
        )
      end
    end

    it 'should list system services as links' do
      system_services.select { |s| s.name != 'Extra charge' }.each do |s|
        expect(page).to(
          have_selector('.system-services td', text: s.name)
        )
      end
    end

    it 'should list not regular services or extra charge as links' do
      [service, service_2, Service.extra_charge].each do |s|
        expect(page).not_to(
          have_selector('.system-services td', text: s.name)
        )
      end
    end
  end

  feature 'New' do
    before { visit new_service_path }

    it 'should load and show the correct select' do
      expect(page).to(
        have_select('service_category_id', options: [category.name])
      )
    end

    scenario 'Create' do
      # setup
      name = 'New service'
      price = 2.2

      # do
      fill_in 'Name',       with: name
      fill_in 'Price(£)',   with: price
      select category.name, from: 'Category'
      click_button 'Create Service'

      # assert
      Service.find_by! name: name, price: price, category: category
    end
  end

  feature 'Edit' do
    before { visit edit_service_path(service) }

    it 'should load and show the correct select' do
      expect(page).to have_select('', options: [category.name])
    end

    scenario 'Edit normally' do
      # setup
      name = 'Edited name'

      # do
      fill_in 'Name', with: name
      click_button 'Update Service'
      service.reload

      # assert
      expect(service.name).to eq(name)
    end
  end

  feature 'Delete' do
    before { visit_index }

    it 'should list as deletable some non-system services' do
      expect(page).to have_link('Delete', href: service_path(service_2))
    end

    it 'should not list as deletable other non-system services' do
      expect(page).not_to have_link('Delete', href: service_path(service))
    end

    it 'should not list as deletable system services' do
      system_services.each do |s|
        expect(page).not_to have_link('Delete', href: service_path(s))
      end
    end

    scenario 'Deleting' do
      expect do
        click_link 'Delete', href: service_path(service_2)
      end.to change(Service, :count).by(-1)
    end
  end

  feature 'Edit price' do
    it 'lists the previous price' do
      visit edit_price_path(service_2)
      expect(page).to have_selector('p', text: '£' + service_2.price.to_s)
    end

    def edit_price_test(svc)
      # setup
      old_name = svc.name
      old_price = svc.price
      new_price = old_price + 1

      # do
      visit edit_price_path(svc)
      fill_in 'service_price', with: new_price
      click_button 'Update Service'

      # load
      svc.reload
      new_service = Service.find_by(name: old_name)

      # assert
      expect(new_service.name).to eq(old_name)
      expect(new_service.price).to eq(new_price)
      expect(new_service.active).to eq(true)
      # the original service is actually archived
      # it is renamed with a timestamp appended on its name
      expect(svc.name).not_to eq(old_name)
      expect(svc.price).to eq(old_price)
      expect(svc.active).to eq(false)
    end

    def cannot_edit_price_test(svc)
      old_price = svc.price
      new_price = old_price + 1

      expect do
        suppress(Exception) do
          visit edit_price_path(svc)
          fill_in 'service_price', with: new_price
          click_button 'Update Service'
        end
      end.not_to change(Service, :count)
      svc.reload
      expect(svc.price).to eq(old_price)
    end

    scenario 'normal edit price' do
      edit_price_test(service)
    end

    scenario 'system service edit price' do
      edit_price_test(Service.ironing_per_hour)
    end

    scenario 'extra charge edit price should not work' do
      cannot_edit_price_test(Service.extra_charge)
    end

    scenario 'inactive service edit price should not work' do
      svc = FactoryBot.create(:service, active: false)
      cannot_edit_price_test(svc)
    end
  end

  feature 'Show' do
    it 'should load and list price' do
      visit service_path(service_2)
      expect(page).to have_selector('p', text: '£' + service_2.price.to_s)
    end
  end
end
