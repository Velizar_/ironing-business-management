require 'spec_helper'

describe 'Addresses', type: :feature do
  before do
    log_in
    FactoryBot.create(:customer, nickname: 'System')
  end

  let(:post_code) { 'postcode1' }
  let(:geocode)   { '42.12,52.75' }
  let(:notes)     { 'Pink house' }

  let(:address) do
    FactoryBot.create(:address,
                       post_code: post_code,
                       geocode: geocode,
                       notes: notes)
  end

  feature 'index' do
    it 'loads and contains a postcode from the DB' do
      address # let is lazy
      visit addresses_path
      expect(page).to have_selector('td', text: 'postcode1')
    end
  end

  feature 'creating a new address' do
    before do
      customer = FactoryBot.create(:customer, nickname: 'Customer1')
      visit add_address_path(customer.id)
    end

    feature 'with valid parameters' do
      before do
        fill_in 'Address name',    with: 'Address 1'
        fill_in 'Post code',       with: 'Post code 1'
        fill_in 'Address geocode', with: '1.2,3.2'
      end

      scenario 'addresses increase by 1' do
        expect do
          click_button 'Create Address'
        end.to change(Address, :count).by(1)
      end

      scenario 'customers should not change' do
        expect do
          click_button 'Create Address'
        end.not_to change(Customer, :count)
      end

      scenario 'with correct attributes' do
        click_button 'Create Address'
        address = Address.find_by!(address_name: 'Address 1',
                                   post_code: 'Post code 1')
        expect(address.geocode).to eq('1.2,3.2')
        expect(address.customer.nickname).to eq('Customer1')
      end
    end

    feature 'with invalid parameters' do
      before do
        fill_in 'Address name',    with: 'Address 2'
        fill_in 'Post code',       with: '' # shouldn't be empty
        fill_in 'Address notes',   with: 'Notes 2'
        fill_in 'Address geocode', with: '1.2,3.2'
      end

      scenario 'addresses should not change' do
        expect { click_button 'Create Address' }.not_to change(Address, :count)
      end

      scenario 'customers should not change' do
        expect { click_button 'Create Address' }.not_to change(Customer, :count)
      end
    end
  end

  feature 'editing an address' do
    before { visit edit_address_path(address.id) }

    scenario 'view edit page' do
      expect(page).to have_field('Post code',       with: post_code)
      expect(page).to have_field('Address notes',   with: notes)
      expect(page).to have_field('Address geocode', with: geocode)
    end

    scenario 'change two fields' do
      # prepare data
      new_name = 'Address 007'
      new_post_code = 'PC0 7CP'

      # update address
      fill_in 'Address name',    with: new_name
      fill_in 'Post code',       with: new_post_code
      click_button 'Update Address'
      address.reload

      # assert
      expect(address.address_name).to eq(new_name)
      expect(address.post_code).to eq(new_post_code)
      expect(address.geocode).to eq(geocode) # old value - unchanged
    end
  end

  feature 'showing an address' do
    scenario 'displays correct information' do
      visit address_path(address.id)
      expect(page).to have_selector('p', text: post_code)
      expect(page).to have_selector('p', text: notes)
    end
  end
end
