require 'spec_helper'

describe 'Booking pages', type: :feature do
  before do
    FactoryBot.create(:customer, nickname: 'System')
    log_in
  end

  let(:customer_1) do
    FactoryBot.create :addressless_customer,
                       nickname: 'Customer 1',
                       discount: 0.0,
                       active: true
  end

  let(:customer_2) do
    FactoryBot.create :addressless_customer,
                       nickname: 'Customer 2',
                       discount: 0.0,
                       active: true
  end

  let(:inactive_customer) do
    FactoryBot.create :customer,
                       nickname: 'Customer inactive',
                       discount: 0.0,
                       active: false
  end

  let(:address_1) do
    FactoryBot.create(:address,
                       address_name: 'Address 1',
                       post_code: 'Post code 1',
                       active: true,
                       customer: customer_1)
  end

  let(:address_2) do
    FactoryBot.create(:address,
                       address_name: 'Address 2',
                       post_code: 'Post code 2',
                       active: true,
                       customer: customer_2)
  end

  let(:address_3) do
    FactoryBot.create(:address,
                       address_name: 'Address 3',
                       post_code: 'Post code 3',
                       active: true,
                       customer: customer_2)
  end

  let(:inactive_address) do
    FactoryBot.create(:address,
                       address_name: 'Inactive Address',
                       post_code: 'Inactive Post code',
                       active: false,
                       customer: customer_2)
  end

  feature 'Index' do
    let!(:booked)    { FactoryBot.create(:booked_booking) }
    let!(:collected) { FactoryBot.create(:collected_booking) }
    let!(:ironed)    { FactoryBot.create(:ironed_booking) }
    let!(:delivered) { FactoryBot.create(:delivered_booking) }
    before { visit bookings_path }

    it 'Lists all correct items' do
      [booked, collected, ironed].each do |booking|
        expect(page).to   have_link('Edit', href: edit_booking_path(booking))
      end

      expect(page).not_to have_link('Edit', href: edit_booking_path(delivered))
    end

    it 'Includes the correct delete links only' do
      [booked, collected].each do |deletable_booking|
        expect(page).to have_link(
          'Delete',
          href: booking_path(deletable_booking)
        )
      end

      [ironed, delivered].each do |non_deletable_booking|
        expect(page).not_to have_link(
          'Delete',
          href: booking_path(non_deletable_booking)
        )
      end
    end

    scenario 'Delete' do
      # sanity check
      expect(Booking.count).to  eq(4)
      expect(Trip.count).to     eq(5)
      expect(TripItem.count).to eq(13) # 2 * bookings + trips

      click_link 'Delete', href: booking_path(booked)

      expect(Booking.count).to  eq(3)
      expect(TripItem.count).to eq(11)

      click_link 'Delete', href: booking_path(collected)

      expect(Booking.count).to  eq(2)
      expect(Trip.count).to     eq(5)
      expect(TripItem.count).to eq(9)
    end
  end

  feature 'New booking' do
    before do
      # for the system customer and address
      Rails.application.load_seed

      # let is lazy
      customer_1
      customer_2
      inactive_customer
      address_1
      address_2
      address_3
      inactive_address

      visit new_booking_path
    end

    it 'Should display the correct data in the selects' do
      # Note: System and inactive_customer are missing
      customer_nicknames = [customer_1, customer_2].map(&:nickname)

      # Note: The system address and inactive_address are missing
      address_names = [address_1, address_2, address_3].map(&:display)

      expect(page).to have_select('Customer',
                                  options: [''] + customer_nicknames)
      expect(page).to have_select('Address',
                                  options: address_names)
    end

    scenario 'Create' do
      # sanity check
      expect(Booking.count).to   eq(0)
      expect(Trip.count).to      eq(0)
      expect(TripItem.count).to  eq(0)

      trip_date = Time.zone.today + 1

      select customer_1.nickname, from: 'Customer'
      select address_1.display,   from: 'Address'
      fill_in 'Notes', with: 'Say hi from me'
      fill_in 'Date',  with: trip_date.to_s
      fill_in 'Visit after', with: '18:50'
      fill_in 'Visit before', with: '22:10'
      click_button 'Create Booking'

      # Assert DB contents
      expect(Booking.count).to   eq(1)
      expect(Trip.count).to      eq(1)
      expect(TripItem.count).to  eq(2) # one for the trip base

      b = Booking.first
      ti = b.collection_trip_item
      vc = ti.visit_constraints.first
      t = Trip.first

      expect(b.customer).to eq(customer_1)
      expect(b.discount).to eq(0.0)
      expect(ti.address).to eq(address_1)
      expect(ti.notes).to   eq('Say hi from me')
      expect(t.date).to     eq(trip_date)
      expect(vc.visit_after.hour).to  eq(18)
      expect(vc.visit_after.min).to   eq(50)
      expect(vc.visit_before.hour).to eq(22)
      expect(vc.visit_before.min).to  eq(10)
    end
  end

  feature 'Book delivery' do
    before do
      # let is lazy
      address_1        # customer_1
      address_2        # customer_2
      address_3        # customer_2
      inactive_address # customer_2
    end

    let(:booking) do
      FactoryBot.create(:booking,
                         delivery_trip_item: nil,
                         customer: customer_2)
    end

    before { visit bookings_delivery_path(booking) }

    it 'Should have the correct addresses in the select' do
      # should equal customer_1.addresses.all_active
      address_names = [address_2, address_3].map(&:display)
      expect(page).to have_select('Address', options: address_names)
    end

    scenario 'Submit' do
      # sanity check
      expect(Booking.count).to   eq(1)
      expect(Trip.count).to      eq(1)
      expect(TripItem.count).to  eq(2) # collection_trip_item and base

      trip_date = booking.collection_trip_item.trip.date

      select address_2.display, from: 'Address'
      fill_in 'Notes', with: "Don't stare at their nose"
      fill_in 'Date', with: trip_date.to_s
      fill_in 'Visit after', with: '19:10'
      click_button 'Book delivery'

      expect(Booking.count).to   eq(1)
      expect(Trip.count).to      eq(1)
      expect(TripItem.count).to  eq(3)

      booking.reload
      ti = booking.delivery_trip_item
      vc = ti.visit_constraints.first

      expect(ti.address).to      eq(address_2)
      expect(ti.notes).to        eq("Don't stare at their nose")
      expect(ti.trip.date).to    eq(trip_date)
      expect(vc.visit_after.hour).to eq(19)
      expect(vc.visit_after.min).to  eq(10)
      expect(vc.visit_before).to     eq(nil)
    end
  end

  feature 'Edit' do
    before do
      # let is lazy
      address_1        # customer_1
      address_2        # customer_2
      address_3        # customer_2
      inactive_address # customer_2
    end

    feature 'Customer 1' do
      let(:trip_date) { Time.zone.today + 1 }

      let(:booking) do
        cti = FactoryBot.create(:trip_item,
                                 trip_date: trip_date,
                                 notes: 'AWARE OF DOG pleas pet dog')
        FactoryBot.create(:booking,
                           collection_trip_item: cti,
                           delivery_trip_item: nil,
                           customer: customer_1,
                           discount: 0.0)
      end

      let(:address_names) { [address_1].map(&:display) }

      before { visit edit_booking_path(booking) }

      it 'should only allow editing of collection attributes' do
        expect(page).not_to have_select(
          'booking_delivery_trip_item_attributes_address_id'
        )
        expect(page).not_to have_field(
          'booking_delivery_trip_item_attributes_trip_date'
        )
      end

      it 'should have the correct address selects' do
        expect(page).to have_select(
          'booking_collection_trip_item_attributes_address_id',
          options: address_names
        )
      end

      it 'should have its fields filled correctly' do
        expect(page).to have_select(
          'booking_collection_trip_item_attributes_address_id',
          selected: address_1.display
        )
        expect(page).to have_field(
          'booking_collection_trip_item_attributes_notes',
          with: 'AWARE OF DOG pleas pet dog'
        )
        expect(page).to have_field(
          'booking_collection_trip_item_attributes_trip_date',
          with: trip_date.to_s
        )
      end

      scenario 'Submit' do
        expect(Booking.count).to     eq(1)
        expect(Trip.count).to eq(1)
        expect(TripItem.count).to    eq(2) # one in booking + one for trip base

        fill_in 'booking_collection_trip_item_attributes_trip_date',
                with: (trip_date + 1).to_s
        fill_in 'booking_collection_trip_item_attributes_notes',
                with: 'Collect dog residue'
        click_button 'Update booking'

        booking.reload
        col_ti = booking.collection_trip_item

        expect(Booking.count).to     eq(1)
        expect(Trip.count).to eq(2)
        expect(TripItem.count).to    eq(3) # one in booking + two for trip bases

        expect(col_ti.notes).to   eq('Collect dog residue')
        expect(col_ti.address).to eq(address_1)
        expect(col_ti.trip).to    eq(Trip.at_date(trip_date + 1))
        expect(booking.delivery_trip_item).to eq(nil)
      end
    end

    feature 'Customer 2' do
      let(:col_trip_date) { Time.zone.today + 1 }
      let(:del_trip_date) { Time.zone.today + 2 }

      let(:booking) do
        cti = FactoryBot.create(:trip_item,
                                 trip_date: col_trip_date,
                                 address: address_2)
        dti = FactoryBot.create(:trip_item,
                                 trip_date: del_trip_date,
                                 address: address_3)
        FactoryBot.create(:booking,
                           collection_trip_item: cti,
                           delivery_trip_item: dti,
                           customer: customer_2,
                           discount: 0.0)
      end

      let(:address_names) { [address_2, address_3].map(&:display) }

      before { visit edit_booking_path(booking) }

      it 'should have the correct address selects' do
        expect(page).to have_select(
          'booking_collection_trip_item_attributes_address_id',
          options: address_names
        )

        expect(page).to have_select(
          'booking_delivery_trip_item_attributes_address_id',
          options: address_names
        )
      end

      it 'should have its fields filled correctly' do
        expect(page).to have_select(
          'booking_collection_trip_item_attributes_address_id',
          selected: address_2.display
        )
        expect(page).to have_field(
          'booking_collection_trip_item_attributes_trip_date',
          with: col_trip_date.to_s
        )

        expect(page).to have_select(
          'booking_delivery_trip_item_attributes_address_id',
          selected: address_2.display
        )
        expect(page).to have_field(
          'booking_delivery_trip_item_attributes_trip_date',
          with: del_trip_date.to_s
        )
      end

      scenario 'Submit' do
        expect(Booking.count).to     eq(1)
        expect(Trip.count).to eq(2)
        expect(TripItem.count).to    eq(4) # two in booking + two for trip bases

        select address_3.display, from:
          'booking_collection_trip_item_attributes_address_id'
        fill_in 'booking_collection_trip_item_attributes_trip_date',
                with: (col_trip_date + 1).to_s

        fill_in 'booking_delivery_trip_item_attributes_trip_date',
                with: (del_trip_date + 1).to_s
        click_button 'Update booking'

        booking.reload
        col_ti = booking.collection_trip_item
        del_ti = booking.delivery_trip_item

        expect(Booking.count).to     eq(1)
        expect(Trip.count).to eq(3)
        # two in booking + three for trip bases
        expect(TripItem.count).to    eq(5)

        expect(col_ti.address).to  eq(address_3)
        expect(col_ti.trip).to     eq(Trip.at_date(col_trip_date + 1))
        expect(del_ti.address).to  eq(address_2)
        expect(del_ti.trip).to     eq(Trip.at_date(del_trip_date + 1))
      end
    end
  end
end
