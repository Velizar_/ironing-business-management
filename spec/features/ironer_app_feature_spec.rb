require 'spec_helper'

describe 'When logged in as ironer' do
  before do
    staff = FactoryBot.create(:staff,
                               permissions: 0,
                               first_name: 'Ironer',
                               last_name: '')
    FactoryBot.create(:ironer_account, staff: staff)
    login_as staff
  end

  it 'Should display the proper header' do
    visit root_path

    expect(page).to     have_link('Home', href: ironing_index_path)
    expect(page).not_to have_link('Staff')
  end

  let(:customers) do
    (0..5).map do |idx|
      FactoryBot.create(:customer, nickname: "Customer #{idx}", active: true)
    end
  end

  # waiting ironing input
  let(:pending_ironing) do
    customers[1..3].map do |c|
      FactoryBot.create(:collected_booking, customer: c).ironing
    end
  end

  # can be edited
  let(:done_ironing) do
    customers[4..5].map do |c|
      # blank_completed_ironing, no booking_services
      FactoryBot.create(:ironed_booking, customer: c).ironing
    end
  end

  feature 'Index' do
    before do
      # for ironing per hour service
      Rails.application.load_seed

      # let is lazy
      pending_ironing
      done_ironing
    end

    # should be shown with editables
    let!(:need_extra_info_ironing) do
      i = FactoryBot.create(:ironed_booking, customer: customers[0]).ironing
      i.booking_services.create(service: Service.ironing_per_hour, count: 0)
      i
    end

    it 'Should display done and need_extra_info ironings as editable' do
      visit ironing_index_path

      ([need_extra_info_ironing] + done_ironing).each do |i|
        expect(page).to have_link('Edit', href: edit_ironing_path(i))
      end

      expect(page).to     have_link('Add information', count: 3)
      expect(page).to     have_link('Edit',            count: 3)
      expect(page).not_to have_link('Add extra information')
    end

    it 'Should not display Show and Receipt links' do
      visit ironing_index_path

      expect(page).not_to have_link('Show')
      expect(page).not_to have_link('Receipt')
    end
  end

  feature 'Form' do
    before do
      # for system services and categories
      Rails.application.load_seed
    end

    let!(:category) { FactoryBot.create(:category, name: 'Category') }

    let!(:services) do
      (1..3).map do |idx|
        FactoryBot.create(:service,
                           name: "Service #{idx}",
                           category: category,
                           active: true)
      end
    end

    feature 'Removed fields' do
      it 'Should not display fields' do
        def execute_expects
          expect(page).not_to have_field('Apply minimum charge')
          expect(page).not_to have_field('Date ironed')
          expect(page).not_to have_field('Discount')
        end

        visit new_ironing_path(pending_ironing[0])
        execute_expects

        visit edit_ironing_path(done_ironing[0])
        execute_expects
      end

      it 'Should not display any booking_services for flat_rate' do
        ironing = pending_ironing[0]
        ironing.booking.customer.update payment_scheme: :flat_rate
        visit new_ironing_path(ironing)

        expect(page).not_to have_selector('h4', text: 'Ironing per hour')
        expect(page).not_to have_selector('#existing-booking-services')
        expect(page).not_to have_selector('#extra-booking-services')
        expect(page).not_to have_field('Discount')
      end

      it 'Should not display any booking_services for flat_rate on edit' do
        ironing = done_ironing[0]
        ironing.booking.customer.update payment_scheme: :flat_rate
        ironing.booking_services.create service: Service.extra_charge, count: 2
        visit edit_ironing_path(ironing)

        expect(page).not_to have_selector('h4', text: 'Ironing per hour')
        expect(page).not_to have_selector('#existing-booking-services')
        expect(page).not_to have_selector('#extra-booking-services')
        expect(page).not_to have_field('Discount')
      end

      it 'Should only display non-system active services for itemized' do
        # create an inactive service
        services[2].update active: false
        active_services = services[0..1]

        ironing = pending_ironing[0]
        ironing.booking.customer.update payment_scheme: :itemized
        visit new_ironing_path(ironing)

        select = page.find(
          :css,
          'select#ironing_booking_services_attributes_0_service_id'
        )
        assert_group_select_elements(select,
                                     category.name => active_services)
        expect(page).to     have_selector('#extra-booking-services')
        expect(page).not_to have_field('Discount')
      end

      it 'Should only display non-system active services on itemized edit' do
        ironing = done_ironing[0]
        ironing.booking.customer.update payment_scheme: :itemized

        # system
        ironing.booking_services.create count: 1, service: Service.extra_charge
        # regular - only this should be shown
        ironing.booking_services.create count: 1, service: services[1]
        # inactive
        ironing.booking_services.create count: 1, service: services[2]
        service_name = services[2].name
        services[2].update_price(services[2].price + 1)
        active_services = services[0..1] +
                          [Service.find_by!(name: service_name)]

        visit edit_ironing_path(ironing)

        expect(page).to have_select('Service', selected: services[1].name)
        select = page.find(
          :css,
          'select#ironing_booking_services_attributes_0_service_id'
        )
        assert_group_select_elements(select,
                                     category.name => active_services)

        expect(page).not_to have_selector(
          '#ironing_booking_services_attributes_1_service_id'
        )

        expect(page).to     have_selector('#extra-booking-services')
        expect(page).not_to have_field('Discount')
      end
    end

    feature 'Submitting form' do
      feature 'New flat rate' do
        let(:ironing) { pending_ironing[0] }

        before do
          ironing.booking.customer.update payment_scheme: :flat_rate
          visit new_ironing_path(ironing)

          select 'Ironer', from: 'Ironer account'
          fill_in 'Time ironed (hours:minutes)', with: '01:10'
          fill_in 'Bag count',                   with: '2'

          click_button 'Submit ironing'
          ironing.reload
        end

        it 'Should set fields properly' do
          tasks = ironing.ironing_tasks
          expect(tasks.size).to               eq(1)
          expect(tasks[0].ironer_account).to  eq(
            Staff.find_by(first_name: 'Ironer').ironer_account
          )
          expect(tasks[0].minutes_ironed).to  eq(70)
          expect(ironing.bag_count).to        eq(2)
          expect(ironing.has_hangers).to      eq(false)
        end

        it 'Should set minimum charge and date_ironed' do
          expect(ironing.date_ironed).to            eq(Time.zone.today)
          # including ironing_per_hour
          expect(ironing.booking_services.count).to eq(2)
          ironing.booking_services.find_by!(service: Service.minimum_charge,
                                            count: 1.0)
        end

        it 'Should add an ironing per hour service with count 0' do
          ironing.booking_services.find_by!(service: Service.ironing_per_hour,
                                            count: 0.0)
        end
      end

      feature 'New itemized' do
        it 'Should add the selected booking_service to the DB' do
          ironing = pending_ironing[0]
          ironing.booking.customer.update payment_scheme: :itemized
          visit new_ironing_path(ironing)

          select 'Ironer', from: 'Ironer account'
          fill_in 'Time ironed (hours:minutes)', with: '01:10'
          fill_in 'Bag count',                   with: '2'

          select services[0].name, from: 'Service'
          fill_in 'Count', with: '1.1'

          click_button 'Submit ironing'
          ironing.reload

          ironing.booking_services.find_by!(service: services[0], count: 1.1)
          ironing.booking_services.find_by!(service: Service.minimum_charge)
          expect(
            ironing.booking_services.find_by(service: Service.ironing_per_hour)
          ).to eq(nil)
        end
      end

      feature 'Editing ironing' do
        let(:ironing) { done_ironing[0] }

        it 'Should update minimum charge ' \
         + 'without changing date_ironed or booking_services' do
          ironing.booking.customer.update payment_scheme: :flat_rate
          ironing.update date_ironed: Time.zone.today - 33
          count = Service.minimum_charge.price / 2 + 1
          ironing.booking_services.create(service: Service.ironing_per_hour,
                                          count: 0.0)
          ironing.booking_services.create(service: Service.minimum_charge,
                                          count: 0.5)
          ironing.booking_services.create(service: Service.extra_charge,
                                          count: count)

          visit edit_ironing_path(ironing)
          click_button 'Submit ironing'
          ironing.reload

          expect(ironing.date_ironed).to eq(Time.zone.today - 33)
          expect(ironing.booking_services.count).to eq(3)
          # decreased by 1
          expect(ironing.total_cost).to(
            be_within(0.001).of(Service.minimum_charge.price)
          )

          # existing booking_services are unchanged
          ironing.booking_services.find_by!(service: Service.extra_charge,
                                            count: count)
          ironing.booking_services.find_by!(service: Service.ironing_per_hour,
                                            count: 0.0)
        end

        it 'Should update booking_services with itemized' do
          ironing.booking.customer.update payment_scheme: :itemized
          ironing.booking_services.create(service: services[0],
                                          count: 1.0)
          ironing.booking_services.create(service: services[1],
                                          count: 3.0)
          ironing.booking_services.create(service: Service.extra_charge,
                                          count: 30.2)

          visit edit_ironing_path(ironing)

          xpath_query = '//div[contains(' \
                        + '@class, "ironing_booking_services_count"' \
                      + ')]/input[@value="1.0"]'
          field = page.find(:xpath, xpath_query)
          field.set('4.0')

          click_button 'Submit ironing'
          ironing.reload

          bs = ironing.booking_services
          expect(bs.count).to eq(3)
          bs.find_by!(service: services[0],          count: 4.0)
          bs.find_by!(service: services[1],          count: 3.0)
          bs.find_by!(service: Service.extra_charge, count: 30.2)
        end
      end
    end
  end
end
