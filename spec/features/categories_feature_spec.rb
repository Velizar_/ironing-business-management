require 'spec_helper'

describe 'Category pages', type: :feature do
  before { log_in }

  let!(:system) do
    c = Category.create name: 'System'
    c.services.create(name: 'Ironing per hour', price: 10)
    c
  end

  let(:category) do
    c = Category.create name: 'spec_category'
    c.services.create(name: 'service 1', price: 1)
    c
  end

  let(:category2) { Category.create name: 'spec_category_empty' }

  def visit_index
    # let is lazy
    category
    category2

    visit categories_path
  end

  feature 'Index' do
    before { visit_index }

    scenario 'should list all three' do
      [system, category, category2].each do |cat|
        expect(page).to have_selector('td', text: cat.name)
      end
    end
  end

  feature 'Delete' do
    before do
      # let is lazy
      category
      category2

      visit categories_path
    end

    scenario 'only category2 should be shown as deletable' do
      expect(page).not_to have_link('Delete', href: category_path(system))
      expect(page).not_to have_link('Delete', href: category_path(category))
      expect(page).to     have_link('Delete', href: category_path(category2))
    end
  end

  feature 'New' do
    before { visit new_category_path }

    scenario 'Page includes a text field' do
      expect(page).to have_field('Name')
    end

    scenario 'Creating' do
      fill_in 'Name', with: 'created category'
      click_button 'Create Category'
      Category.find_by!(name: 'created category')
    end
  end

  feature 'Rename' do
    before { visit_index }

    scenario 'System is not editable, everything else is' do
      expect(page).not_to have_link('Rename',
                                    href: edit_category_path(system))
      expect(page).to     have_link('Rename',
                                    href: edit_category_path(category))
      expect(page).to     have_link('Rename',
                                    href: edit_category_path(category2))
    end

    scenario 'Page includes filled in text field' do
      visit edit_category_path(category)
      expect(page).to have_field('Name', with: category.name)
    end

    scenario 'Submit rename' do
      new_name = 'renamed category'
      visit edit_category_path(category)
      fill_in 'Name', with: new_name
      click_button 'Update Category'
      category.reload

      expect(category.name).to eq(new_name)
    end
  end

  feature 'Show' do
    scenario 'Loads and shows name' do
      visit category_path(category)
      expect(page).to have_selector('p', text: category.name)
    end
  end
end
