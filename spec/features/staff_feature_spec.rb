require 'spec_helper'

describe 'Staff pages' do
  before do
    FactoryBot.create(:customer, nickname: 'System')
    FactoryBot.create(:owner, first_name: 'Owner', last_name: '')
    log_in
  end

  feature 'Index' do
    it 'Should list all accounts' do
      staff1 = FactoryBot.create(:staff,
                                  first_name: 'Iron',
                                  last_name: 'Man',
                                  active: true)
      staff2 = FactoryBot.create(:staff,
                                  first_name: 'Driver',
                                  last_name: 'Person',
                                  active: true)
      staff3 = FactoryBot.create(:staff,
                                  first_name: 'Admin',
                                  last_name: 'Human',
                                  active: true)

      ironer  = staff1.ironer_accounts.create(active: true, pay_rate: 7)
      _driver = staff2.driver_accounts.create(active: true, pay_rate: 35)

      visit staff_index_path

      # Staff
      expect(page).to have_content('Iron')
      expect(page).to have_content('Driver')
      expect(page).to have_content('Admin')

      # Accounts
      expect(page).to have_content('Iron Man')
      expect(page).to have_content('Driver Person')
      expect(page).to have_content('Admin Human')

      # Ironer
      expect(page).to have_link(
        'Show weekly earnings',
        href: ironer_weekly_wage_path(
          ironer,
          date: Time.zone.today.beginning_of_week
        )
      )
    end
  end

  feature 'Create staff' do
    it 'Should save input to DB' do
      # Sanity check
      expect(Staff.count).to eq(1) # from log_in

      visit new_staff_path

      fill_in 'Username',              with: 'user'
      fill_in 'staff_password',        with: 'pass123'
      fill_in 'Password confirmation', with: 'pass123'
      fill_in 'First name',            with: 'Staff'
      fill_in 'Last name',             with: 'Member'
      # Email is empty
      fill_in 'Phone number', with: '12-34'
      click_button 'Create Staff'

      s = Staff.order(:created_at).last

      expect(Staff.count).to eq(2)
      expect(s.username).to                   eq('user')
      expect(s.valid_password?('pass123')).to eq(true)
      expect(s.first_name).to                 eq('Staff')
      expect(s.last_name).to                  eq('Member')
      expect(s.email.blank?).to               eq(true)
      expect(s.phone_number).to               eq('12-34')
    end
  end

  feature 'Edit staff' do
    let!(:staff) do
      FactoryBot.create(:staff,
                         active: true,
                         username: 'user',
                         password: 'pass123',
                         first_name: 'Staff',
                         last_name: 'Member',
                         email: 'a@bc.de')
      #                  phone_number is nil
    end

    it 'Should fill in fields from DB' do
      visit edit_staff_path(staff)

      expect(page).to have_field 'Username',                 with: 'user'
      expect(find('#staff_password')['value'].blank?).to     eq(true)
      expect(page).to have_field 'First name',               with: 'Staff'
      expect(page).to have_field 'Last name',                with: 'Member'
      expect(page).to have_field 'Email',                    with: 'a@bc.de'
      expect(find('#staff_phone_number')['value'].blank?).to eq(true)
    end

    it 'Should save input to DB' do
      # Sanity check
      expect(Staff.count).to eq(2) # from log_in and staff

      visit edit_staff_path(staff)

      fill_in 'Username',              with: 'userer'
      fill_in 'Password',              with: '123456'
      fill_in 'Password confirmation', with: '123456'
      fill_in 'First name',            with: 'Quarterstaff'
      fill_in 'Phone number',          with: '4321'
      click_button 'Update Staff'

      s = staff
      s.reload

      expect(Staff.count).to eq(2)
      expect(s.username).to                  eq('userer')
      expect(s.valid_password?('123456')).to eq(true)
      expect(s.first_name).to                eq('Quarterstaff')
      expect(s.last_name).to                 eq('Member')
      expect(s.email).to                     eq('a@bc.de')
      expect(s.phone_number).to              eq('4321')
    end

    it 'Does not change the password if the field was left empty' do
      visit edit_staff_path(staff)

      fill_in 'First name', with: 'Quarterstaff'
      # leave Password blank
      click_button 'Update Staff'
      staff.reload

      expect(staff.first_name).to                 eq('Quarterstaff')
      expect(staff.valid_password?('pass123')).to eq(true)
    end
  end

  feature 'Appoint' do
    before do
      s1  = FactoryBot.create(:staff,
                               active: true,
                               first_name: '1',
                               last_name: '')
      s2  = FactoryBot.create(:staff,
                               active: true,
                               first_name: '2',
                               last_name: '')
      _s3 = FactoryBot.create(:staff,
                               active: true,
                               first_name: '3',
                               last_name: '')

      s1.ironer_accounts.create        active: true, pay_rate: 1
      s1.driver_accounts.create        active: true, pay_rate: 2

      s2.ironer_accounts.create        active: true, pay_rate: 3
    end

    feature 'Ironer' do
      before { visit appoint_index_path(role: 'ironer') }

      it 'Shows the correct options in the select box' do
        expect(page).to have_select('Staff', options: %w(3 Owner))
      end

      it 'Fills in the DB on submit' do
        # sanity check
        expect(Staff.count).to         eq(4) # + 1 used in log_in
        expect(IronerAccount.count).to eq(2)

        select '3',             from: 'Staff'
        fill_in 'Pay rate (£)', with: 3
        click_button 'Appoint'

        ironer = Staff.find_by!(first_name: '3').ironer_account

        expect(Staff.count).to         eq(4)
        expect(IronerAccount.count).to eq(3)
        expect(ironer.pay_rate).to     eq(3.0)
        expect(ironer.active).to       eq(true)
      end
    end

    feature 'Driver' do
      before { visit appoint_index_path(role: 'driver') }

      it 'Shows the correct options in the select box' do
        expect(page).to have_select('Staff', options: %w(2 3 Owner))
      end

      it 'Fills in the DB on submit' do
        # sanity check
        expect(Staff.count).to         eq(4) # + 1 used in log_in
        expect(DriverAccount.count).to eq(1)

        select '2',             from: 'Staff'
        fill_in 'Pay rate (£)', with: 44.3
        click_button 'Appoint'

        driver = Staff.find_by!(first_name: '2').driver_account

        expect(Staff.count).to         eq(4)
        expect(DriverAccount.count).to eq(2)
        expect(driver.pay_rate).to     eq(44.3)
        expect(driver.active).to       eq(true)
      end
    end
  end

  feature 'Weekly wage' do
    let!(:staff) do
      s = FactoryBot.create(:staff, active: true)
      s.ironer_accounts.create(active: true, pay_rate: 10)
      s
    end
    let(:ironer) { staff.ironer_account }
    let(:monday) { '2016-02-08'.to_date }

    it 'Is calculated correctly' do
      def to_min(time_string)
        time = Time.zone.parse(time_string)
        time.hour * 60 + time.min
      end
      FactoryBot.create(:ironing,
                         date_ironed: monday,
                         ironing_tasks_attributes: [{
                           minutes_ironed: to_min('03:00'),
                           ironer_account: ironer
                         }])
      FactoryBot.create(:ironing,
                         date_ironed: monday + 1,
                         ironing_tasks_attributes: [{
                           minutes_ironed: to_min('01:00'),
                           ironer_account: ironer
                         }])
      FactoryBot.create(:ironing,
                         date_ironed: monday + 2,
                         ironing_tasks_attributes: [{
                           minutes_ironed: to_min('02:00'),
                           ironer_account: ironer
                         }])
      FactoryBot.create(:ironing,
                         date_ironed: monday + 2,
                         ironing_tasks_attributes: [{
                           minutes_ironed: to_min('04:30'),
                           ironer_account: ironer
                         }])
      i = FactoryBot.create(:ironing,
                             date_ironed: monday + 5,
                             ironing_tasks_attributes: [{
                               minutes_ironed: to_min('08:30'),
                               ironer_account: ironer
                             }])
      # multiple ironing_tasks per ironing
      i.ironing_tasks.create(minutes_ironed: to_min('00:30'),
                             ironer_account: ironer)
      # multiple ironing_tasks, wrong ironer
      i.ironing_tasks.create(minutes_ironed: to_min('00:45'))

      visit ironer_weekly_wage_path(ironer, date: monday)

      expect(page).to have_content('£30.00')
      expect(page).to have_content('£10.00')
      expect(page).to have_content('£65.00')
      expect(page).to have_content('£0.00')
      # GBP 0.00 again
      expect(page).to have_content('£90.00')
      # GBP 0.00 again
      expect(page).to have_content('£195.00') # total
    end
  end
end
