require 'spec_helper'

describe 'Customers', type: :feature do
  before do
    FactoryBot.create(:customer, nickname: 'System')
  end

  feature 'Customer pages' do
    before { log_in }

    # used in searching at index
    let(:nicknames) { %w(foo bar baz foofoo barbaz) }

    let(:customer) do
      c = FactoryBot.create(:customer, nickname: nicknames[0])
      c.addresses.each(&:destroy)

      c.addresses.create!(address_name: 'Address name',
                          post_code: 'Postcode',
                          geocode: '2.0,3.4')
      c
    end

    let!(:customer2) do
      c = FactoryBot.create(:customer,
                             nickname: nicknames[1],
                             email: 'bar@foo.com')
      c.addresses.each(&:destroy)
      c.addresses.create!(address_name: 'Address2 name',
                          post_code: 'Postcode2',
                          geocode: '-2.3,-5.4')

      c.addresses.create!(address_name: 'Address3 name',
                          post_code: 'Postcode3',
                          notes: 'addrnotes3',
                          geocode: '-33.4,5.6')
      c
    end

    feature 'index' do
      before do
        # let is lazy
        customer

        FactoryBot.create(:customer, nickname: nicknames[2])
        FactoryBot.create(:customer, nickname: nicknames[3])
        FactoryBot.create(:customer, nickname: nicknames[4])
        visit customers_path
      end

      scenario 'list each user' do
        expected_nicknames = [customer.nickname, customer2.nickname,
                              nicknames[2], nicknames[3], nicknames[4]]
        expected_nicknames.each do |nickname|
          expect(page).to have_selector('td', text: nickname)
        end
      end

      # also unit tested in the Customer test
      feature 'Searching' do
        # tests for substring match in nickname
        scenario 'should show customers 2, 5 when searching for "bar"' do
          fill_in 'search', with: 'bar'
          click_button 'Search'
          nicknames.values_at(1, 4).each do |correct|
            expect(page).to have_selector('td', text: exact(correct))
          end
          nicknames.values_at(0, 2, 3).each do |incorrect|
            expect(page).not_to have_selector('td', text: exact(incorrect))
          end
        end

        # tests for substring in column nickname OR email
        scenario 'should show customers 1, 2, 4 when searching for "foo"' do
          fill_in 'search', with: 'foo'
          click_button 'Search'
          nicknames.values_at(0, 1, 3).each do |correct|
            expect(page).to have_selector('td', text: exact(correct))
          end
          nicknames.values_at(2, 4).each do |incorrect|
            expect(page).not_to have_selector('td', text: exact(incorrect))
          end
        end

        # tests for case-insensitive substring match in address.post_code
        scenario 'should show customers 1, 2 when searching for "postc"' do
          fill_in 'search', with: 'postc'
          click_button 'Search'
          nicknames.values_at(0, 1).each do |correct|
            expect(page).to have_selector('td', text: exact(correct))
          end
          nicknames.values_at(2, 3, 4).each do |incorrect|
            expect(page).not_to have_selector('td', text: exact(incorrect))
          end
        end

        scenario 'should show all customers when searching for empty string' do
          fill_in 'search', with: ''
          click_button 'Search'
          nicknames.values_at(0, 1, 2, 3, 4).each do |correct|
            expect(page).to have_selector('td', text: exact(correct))
          end
        end
      end
    end

    feature 'create new customer' do
      before { visit new_customer_path }

      feature 'with valid arguments' do
        before do
          fill_in 'Nickname',        with: 'Nick 1'
          fill_in 'First name',      with: 'John'
          fill_in 'Phone home',      with: '111-222'
          choose 'Itemized'
          fill_in 'Address name',    with: 'addr1'
          fill_in 'Post code',       with: 'PC1 1PC'
          fill_in 'Address notes',   with: 'notenote'
          fill_in 'Address geocode', with: '11.22,33.44'
        end

        scenario 'create a new user' do
          expect do
            click_button 'Create Customer'
          end.to change(Customer, :count).by(1)
        end

        scenario 'create a new address' do
          expect do
            click_button 'Create Customer'
          end.to change(Address, :count).by(1)
        end

        scenario 'the new address is linked to the correct customer' do
          click_button 'Create Customer'
          address = Address.find_by!(address_name: 'addr1',
                                     post_code: 'PC1 1PC')
          customer = Customer.find_by!(nickname: 'Nick 1')
          expect(address.customer_id).to eq(customer.id)
        end

        scenario 'the new address has the correct notes attribute' do
          click_button 'Create Customer'
          address = Address.find_by!(address_name: 'addr1',
                                     post_code: 'PC1 1PC')
          expect(address.notes).to eq('notenote')
        end
      end

      feature 'with invalid arguments' do
        before do
          fill_in 'Address name',    with: 'addr1'
          # missing post code
          fill_in 'Address notes',   with: 'notenote'
          fill_in 'Address geocode', with: '1.42,33.44'
          # missing nickname
          fill_in 'First name',      with: 'John'
          fill_in 'Phone home',      with: '111-222'
        end

        scenario 'does not create a new user' do
          expect do
            click_button 'Create Customer'
          end.not_to change(Customer, :count)
        end

        scenario 'does not create a new address' do
          expect do
            click_button 'Create Customer'
          end.not_to change(Address, :count)
        end
      end

      feature 'with valid customer but invalid address params' do
        before do
          fill_in 'Address name',    with: 'addr1'
          # missing post code
          fill_in 'Address notes',   with: 'notenote'
          fill_in 'Address geocode', with: '2.41,35.84'
          fill_in 'Nickname',        with: 'Nick 1'
          fill_in 'First name',      with: 'John'
          fill_in 'Phone home',      with: '111-222'
        end

        scenario 'does not create a new user' do
          expect do
            click_button 'Create Customer'
          end.not_to change(Customer, :count)
        end

        scenario 'does not create a new address' do
          expect do
            click_button 'Create Customer'
          end.not_to change(Address, :count)
        end
      end

      feature 'with valid address but invalid customer params' do
        before do
          fill_in 'Address name',    with: 'addr1'
          fill_in 'Post code',       with: 'PC1 1PC'
          fill_in 'Address notes',   with: 'notenote'
          fill_in 'Address geocode', with: '2.43,33.44'
          # missing nickname
          fill_in 'First name',      with: 'Johnson'
          fill_in 'Phone home',      with: '111-222'
        end

        scenario 'does not create a new user' do
          expect do
            click_button 'Create Customer'
          end.not_to change(Customer, :count)
        end

        scenario 'does not create a new address' do
          expect do
            click_button 'Create Customer'
          end.not_to change(Address, :count)
        end
      end
    end

    feature 'edit customer' do
      before do
        visit edit_customer_path(customer2.id)
      end

      feature 'with valid params' do
        before do
          fill_in 'Nickname',   with: 'Nick 11'
          fill_in 'First name', with: 'John'
          fill_in 'Last name',  with: 'John'
          fill_in 'Phone home', with: '111-222'
          fill_in 'customer_addresses_attributes_0_post_code',
                  with: 'PC1 1PC'
          fill_in 'customer_addresses_attributes_0_geocode',
                  with: '13.45,64.23'

          fill_in 'customer_addresses_attributes_1_address_name',
                  with: 'addr2'
          fill_in 'customer_addresses_attributes_1_post_code',
                  with: 'PC2 2PC'
          fill_in 'customer_addresses_attributes_1_notes',
                  with: 'w22'
          fill_in 'customer_addresses_attributes_1_geocode',
                  with: '28.65,-2.02'
        end

        scenario 'successfully edit customer nickname and last name' do
          click_button 'Update Customer'
          expect(
            Customer.find_by!(nickname: 'Nick 11').last_name
          ).to eq('John')
        end

        scenario 'successfully edit address 2 post code and notes' do
          click_button 'Update Customer'
          expect(
            Address.find_by!(post_code: 'PC2 2PC').notes
          ).to eq('w22')
        end
      end

      feature 'with invalid params' do
        before do
          # nickname cannot be empty
          fill_in 'Nickname',   with: ''
          fill_in 'First name', with: 'John'
          fill_in 'Last name',  with: 'John'
          fill_in 'Phone home', with: '111-222'
          fill_in 'customer_addresses_attributes_0_post_code',
                  with: 'PC1 1PC'
          fill_in 'customer_addresses_attributes_0_geocode',
                  with: '13.45,64.23'

          fill_in 'customer_addresses_attributes_1_address_name',
                  with: 'addr2'
          fill_in 'customer_addresses_attributes_1_post_code',
                  with: 'PC2 2PC'
          fill_in 'customer_addresses_attributes_1_notes',
                  with: 'w22'
          fill_in 'customer_addresses_attributes_1_geocode',
                  with: '28.65,-2.02'
        end

        scenario 'successfully keep customer last name' do
          click_button 'Update Customer'
          expect(
            Customer.find_by!(nickname: 'bar').last_name
          ).to eq(nil)
        end

        scenario 'successfully keep both address notes unchanged' do
          click_button 'Update Customer'
          expect(
            Address.find_by!(address_name: 'Address2 name').notes
          ).to eq(nil)

          expect(
            Address.find_by!(address_name: 'Address3 name').notes
          ).to eq('addrnotes3')
        end
      end
    end

    feature 'show customer' do # not an important feature
      scenario 'page loads' do
        visit customer_path(customer.id)
        expect(page).to have_selector('p', text: customer.nickname)
      end
    end

    feature 'delete customer' do
      before do
        customer.bookings.create(collection_trip_item:
                               TripItem.create(trip_date: '2016-01-12',
                                               address: customer.addresses[0]))
        customer2 # let is lazy
      end

      scenario 'not listed as deletable on main page' do
        visit customers_path
        expect(page).not_to have_link('Delete', href: customer_path(customer))
        expect(page).to     have_link('Delete', href: customer_path(customer2))
      end
    end
  end
end
