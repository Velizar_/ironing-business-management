require 'rails_helper'

RSpec.describe 'staff/new', type: :view do
  before(:each) do
    assign(:staff,
           Staff.new(
             username: 'MyString',
             password: 'MyString',
             first_name: 'MyString',
             last_name: 'MyString',
             email: 'MyString',
             phone_number: 'MyString'
           ))
  end

  it 'renders new staff form' do
    render

    assert_select 'form[action=?][method=?]', staff_index_path, 'post' do
      assert_select 'input#staff_username[name=?]', 'staff[username]'
      assert_select 'input#staff_password[name=?]', 'staff[password]'
      assert_select 'input#staff_first_name[name=?]', 'staff[first_name]'
      assert_select 'input#staff_last_name[name=?]', 'staff[last_name]'
      assert_select 'input#staff_email[name=?]', 'staff[email]'
      assert_select 'input#staff_phone_number[name=?]', 'staff[phone_number]'
    end
  end
end
