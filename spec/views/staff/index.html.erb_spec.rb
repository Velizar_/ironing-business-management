require 'rails_helper'

RSpec.describe 'staff/index', type: :view do
  before(:each) do
    staff = assign(:staff,
                   [
                     Staff.create!(
                       username: 'Username',
                       password: 'Password',
                       password_confirmation: 'Password',
                       first_name: 'FirstName',
                       last_name: 'LastName',
                       email: 'Email',
                       phone_number: '123'
                     ),
                     Staff.create!(
                       username: 'Username2',
                       password: 'Password',
                       password_confirmation: 'Password',
                       first_name: 'FirstName2',
                       last_name: 'LastName2',
                       email: 'Email2',
                       phone_number: '124'
                     )
                   ])
    ia = staff[0].ironer_accounts.create(active: true, pay_rate: 1.5)
    da = staff[1].driver_accounts.create(active: true, pay_rate: 4.9)

    assign(:ironers, [ia])
    assign(:drivers, [da])
  end

  it 'renders a list of staff' do
    render
    assert_select 'h1', text: 'Listing staff', count: 1
    # username is lowercased
    assert_select 'tr>td', text: 'username', count: 1
    assert_select 'tr>td', text: 'username2', count: 1

    assert_select 'tr>td', text: 'FirstName', count: 1
    assert_select 'tr>td', text: 'FirstName2', count: 1
    assert_select 'tr>td', text: 'LastName', count: 1
    assert_select 'tr>td', text: 'LastName2', count: 1
    assert_select 'tr>td', text: 'Email', count: 1
    assert_select 'tr>td', text: 'Email2', count: 1
    assert_select 'tr>td', text: '123', count: 1
    assert_select 'tr>td', text: '124', count: 1

    # as listed on ironers/drivers
    assert_select 'tr>td', text: 'FirstName LastName', count: 1
    assert_select 'tr>td', text: 'FirstName2 LastName2', count: 1

    assert_select 'h2', text: 'Listing ironers', count: 1
    assert_select 'tr>td', text: '£1.50', count: 1

    assert_select 'h2', text: 'Listing drivers', count: 1
    assert_select 'tr>td', text: '£4.90', count: 1
  end
end
