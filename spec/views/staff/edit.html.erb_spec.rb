require 'rails_helper'

RSpec.describe 'staff/edit', type: :view do
  before(:each) do
    @staff = assign(:staff,
                    Staff.create!(
                      username: 'MyString',
                      password: 'MyString',
                      password_confirmation: 'MyString',
                      first_name: 'MyString',
                      last_name: 'MyString',
                      email: 'MyString',
                      phone_number: 'MyString'
                    ))
  end

  it 'renders the edit staff form' do
    render

    assert_select 'form[action=?][method=?]', staff_path(@staff), 'post' do
      assert_select 'input#staff_username[name=?]', 'staff[username]'
      assert_select 'input#staff_password[name=?]', 'staff[password]'
      assert_select 'input#staff_first_name[name=?]', 'staff[first_name]'
      assert_select 'input#staff_last_name[name=?]', 'staff[last_name]'
      assert_select 'input#staff_email[name=?]', 'staff[email]'
      assert_select 'input#staff_phone_number[name=?]', 'staff[phone_number]'
    end
  end
end
