require 'rails_helper'

RSpec.describe 'staff/show', type: :view do
  before(:each) do
    @staff = assign(:staff, Staff.create!(
                              username: 'Username',
                              password: 'Password',
                              password_confirmation: 'Password',
                              first_name: 'First Name',
                              last_name: 'Last Name',
                              email: 'Email',
                              phone_number: 'Phone Number'
    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Username/)
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Phone Number/)
  end
end
