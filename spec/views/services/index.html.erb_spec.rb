require 'rails_helper'

RSpec.describe 'services/index', type: :view do
  before(:each) do
    Category.create!(name: 'System')

    cat = Category.create!(name: 'Category')
    assign(:editable_system_services, [])
    assign(:services,
           [
             Service.create!(
               name: 'Service',
               price: 1.5,
               active: true,
               category: cat
             ),
             Service.create!(
               name: 'Service2',
               price: 1.5,
               active: true,
               category: cat
             )
           ])
  end

  it 'renders a list of services' do
    render
    assert_select 'tr>td', text: 'Service', count: 1
    assert_select 'tr>td', text: 'Service2', count: 1
    assert_select 'tr>td', text: '£1.50', count: 2
    assert_select 'tr>td', text: 'Category', count: 2
  end
end
