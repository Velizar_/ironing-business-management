require 'rails_helper'

RSpec.describe 'services/new', type: :view do
  before(:each) do
    assign(:service,
           Service.new(
             name: 'MyString',
             price: 1.5,
             active: false
           ))
  end

  it 'renders new service form' do
    render

    assert_select 'form[action=?][method=?]', services_path, 'post' do
      assert_select 'input#service_name[name=?]', 'service[name]'
      assert_select 'input#service_price[name=?]', 'service[price]'
      assert_select 'input#service_active[name=?]', 'service[active]'
    end
  end
end
