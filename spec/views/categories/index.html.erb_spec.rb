require 'rails_helper'

RSpec.describe 'categories/index', type: :view do
  before do
    assign(:categories,
           [
             Category.create!(name: 'Name'),
             Category.create!(name: 'System')
           ])
  end

  it 'renders a list of categories' do
    render
    assert_select 'tr>td', text: 'System', count: 1
    assert_select 'tr>td', text: 'Name',   count: 1
  end
end
