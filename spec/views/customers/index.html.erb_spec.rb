require 'spec_helper'

describe 'customers/index', type: :view do
  before do
    customer1 = stub_model(Customer,
                           nickname: 'Nickname',
                           email: 'Email',
                           first_name: 'First Name',
                           last_name: 'Last Name',
                           phone_home: '111',
                           phone_mobile: '222',
                           notes: 'Notes',
                           payment_scheme: :itemized,
                           active: true)
    customer2 = stub_model(Customer,
                           nickname: 'Nickname2',
                           email: 'Email',
                           first_name: 'First Name',
                           last_name: 'Last Name',
                           phone_home: '111',
                           phone_mobile: '222',
                           notes: 'Notes',
                           payment_scheme: :itemized,
                           active: true)

    customer1.addresses.create(address_name: 'Address1',
                               post_code: 'Postcode1', active: true)
    customer1.addresses.create(address_name: 'Address2',
                               post_code: 'Postcode2', active: true)
    customer1.addresses.create(address_name: 'Address3',
                               post_code: 'Postcode3', active: true)
    customer2.addresses.create(address_name: 'Address4',
                               post_code: 'Postcode4', active: true)

    customers = [customer1, customer2]
    # for will_paginate
    customers.define_singleton_method(:total_pages) { 1 }

    assign(:customers, customers)
    assign(:addresses, customers.flat_map(&:addresses))
    assign(:base_address, FactoryBot.build(:blank_address))
    allow(view).to receive(:current_staff).and_return(Staff.new)
  end

  it 'renders a list of customers' do
    render
    assert_select 'tr>td', text: 'Nickname',   count: 1
    assert_select 'tr>td', text: 'Nickname2',  count: 1
    assert_select 'tr>td', text: 'Email',      count: 2
    assert_select 'tr>td', text: 'First Name', count: 2
    assert_select 'tr>td', text: 'Last Name',  count: 2
    assert_select 'tr>td', text: '111',        count: 2
    assert_select 'tr>td', text: '222',        count: 2
    assert_select 'tr>td', text: 'Itemized',   count: 2
    assert_select 'tr>td', text: 'Notes',      count: 2
  end

  it 'renders their addresses' do
    render
    assert_select 'tr>td', text: 'Address1, Postcode1 / ' \
                               + 'Address2, Postcode2 / ' \
                               + 'Address3, Postcode3', count: 1
    assert_select 'tr>td', text: 'Address4, Postcode4', count: 1
  end
end
