require 'spec_helper'

describe 'customers/edit', type: :view do
  before do
    @customer = assign(:customer,
                       stub_model(Customer,
                                  nickname: 'MyString',
                                  email: 'MyString',
                                  first_name: 'MyString',
                                  last_name: 'MyString',
                                  phone_home: '',
                                  phone_mobile: '',
                                  notes: 'MyString'))
    assign(:base_address, FactoryBot.build(:blank_address))
  end

  it 'renders the edit customer form' do
    render

    assert_select 'form[action=?][method=?]',
                  customer_path(@customer),
                  'post' do
      assert_select 'input#customer_nickname[name=?]',
                    'customer[nickname]'
      assert_select 'input#customer_email[name=?]',
                    'customer[email]'
      assert_select 'input#customer_first_name[name=?]',
                    'customer[first_name]'
      assert_select 'input#customer_last_name[name=?]',
                    'customer[last_name]'
      assert_select 'input#customer_phone_home[name=?]',
                    'customer[phone_home]'
      assert_select 'input#customer_phone_mobile[name=?]',
                    'customer[phone_mobile]'
      assert_select 'textarea#customer_notes[name=?]',
                    'customer[notes]'
    end
  end
end
