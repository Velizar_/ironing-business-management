# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'factory_bot_rails'
require 'rspec-activemodel-mocks'
require File.expand_path('../test/test_helper', __dir__)

include Warden::Test::Helpers

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

# FactoryBot.find_definitions

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'

  # Include Devise helpers for user authentication stubbing
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include ControllerHelpers, type: :controller
  config.include Capybara::DSL
  # config.include FactoryBot::Syntax::Methods

  config.include Rails.application.routes.url_helpers

  # regex which only matches the exact text
  def exact(text)
    /\A#{Regexp.escape(text)}\Z/
  end

  def new_valid_session
    sign_in(Staff.first || FactoryBot.create(:owner))
  end

  def log_in
    login_as(Staff.where(permissions: 1).first || FactoryBot.create(:owner))
  end

  # requires Capybara selectors
  # select is obtained from page selectors
  # expected_services is hash like {'Names' => ['Steve', 'Stevette']}
  # This is an equality test -
  #   the select may not have additional or missing elements
  def assert_group_select_elements(select, expected_services)
    expected_services.each do |cat, services|
      cat_selector = select.all(:css, "optgroup[label=\"#{cat}\"]").first
      services.each do |svc|
        cat_selector.assert_selector("/option[contains('#{svc.name}')]") # xpath
        cat_selector.assert_selector("option[value=\"#{svc.id}\"]")      # css
      end
      cat_selector.assert_selector('option', count: services.size)
    end
    select.assert_selector('optgroup', count: expected_services.keys.size)
  end

  # generates distances for all addresses on a trip
  def gen_address_distances(trip, distance = 10)
    address_ids = trip.trip_items.pluck(:address_id)
    address_ids.product(address_ids).each do |addr1, addr2|
      AddressDistance.create(from_address_id: addr1, to_address_id: addr2, distance_seconds: distance)
      if addr1 != addr2
        AddressDistance.create(from_address_id: addr2, to_address_id: addr1, distance_seconds: distance)
      end
    end
  end
end
