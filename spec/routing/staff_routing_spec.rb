require 'rails_helper'

RSpec.describe StaffController, type: :routing do
  describe 'routing' do
    # useful

    it 'routes to #appoint_index' do
      expect(get: '/staff/appoint/ironer').to route_to('staff#appoint_index',
                                                       role: 'ironer')
    end

    it 'routes to #appoint' do
      expect(post: '/staff/appoint').to route_to('staff#appoint')
    end

    it "doesn't route to #destroy" do
      expect(delete: '/staff/1').not_to route_to('staff#destroy',
                                                 id: '1')
    end

    # not very useful

    it 'routes to #index' do
      expect(get: '/staff').to route_to('staff#index')
    end

    it 'routes to #new' do
      expect(get: '/staff/new').to route_to('staff#new')
    end

    it 'routes to #show' do
      expect(get: '/staff/1').to route_to('staff#show',
                                          id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/staff/1/edit').to route_to('staff#edit',
                                               id: '1')
    end

    it 'routes to #create' do
      expect(post: '/staff').to route_to('staff#create')
    end

    it 'routes to #update' do
      expect(put: '/staff/1').to route_to('staff#update',
                                          id: '1')
    end
  end
end
