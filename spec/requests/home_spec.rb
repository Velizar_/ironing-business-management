require 'spec_helper'

describe 'Home page', type: :request do
  it 'should load properly' do
    log_in
    get '/'
    expect(response.status).to eq(200)
  end
end
