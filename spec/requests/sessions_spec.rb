require 'spec_helper'

# assumes '/' requires authentication

describe 'Sessions pages', type: :request do
  let(:password) { 'validPass' }
  let(:owner) { FactoryBot.create(:owner, password: password) }

  def sign_in_with(user: owner.username, pass: password)
    visit '/staff/sign_in'
    fill_in 'staff_username', with: user
    fill_in 'staff_password', with: pass
    click_button 'Sign in'
  end

  describe 'sign_in page' do
    it 'should load properly' do
      visit '/'
      expect(current_path).to eq('/staff/sign_in')
    end
  end

  describe 'login via sign_in page' do
    it 'should authenticate correctly' do
      sign_in_with
      visit '/'
      expect(current_path).to eq('/')
    end

    it 'should reject invalid credentials' do
      sign_in_with(pass: 'wrongPass')
      visit '/'
      expect(current_path).to eq('/staff/sign_in')
    end
  end

  describe 'logout via logout button' do
    before(:each) do
      sign_in_with
    end

    it 'should redirect / to /staff_sign_in' do
      visit '/'
      click_button 'Log Out'
      visit '/'
      expect(current_path).to eq('/staff/sign_in')
    end
  end
end
