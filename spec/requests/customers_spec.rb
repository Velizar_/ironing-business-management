require 'spec_helper'

describe 'Customer pages', type: :request do
  before do
    FactoryBot.create(:customer, nickname: 'System')
    log_in
  end

  let!(:customer) do
    c = FactoryBot.create(:customer, nickname: 'undeletable')
    c.bookings.create(collection_trip_item:
                        TripItem.create(trip_date: '2016-01-12',
                                        address: c.addresses[0]))
    c
  end

  let!(:customer2) { FactoryBot.create(:customer, nickname: 'deletable') }

  describe 'delete' do
    it 'works correctly' do
      expect do
        delete customer_path(customer2)
      end.to change(Customer, :count).by(-1)
    end

    it 'correctly refuses to work' do
      expect do
        delete customer_path(customer)
      end.not_to change(Customer, :count)
    end
  end

  describe 'check_unique' do
    it 'works with existing, is case-insensitive' do
      get check_unique_path, params: {
        customer: { nickname: 'Deletable' }, format: :json
      }
      expect(response.body).to eq('false')
    end

    it 'works with unexisting' do
      get check_unique_path, params: {
        customer: { nickname: 'unexisting' }, format: :json
      }
      expect(response.body).to eq('true')
    end
  end
end
