require 'spec_helper'

describe 'Trip pages', type: :request do
  scenario 'PATCH /trips updates' do
    Rails.application.load_seed # for System address
    log_in

    trip = FactoryBot.create(:trip,
                              date: (Time.zone.today + 3),
                              completed: false,
                              start_time: '18:30')
    base_ti = trip.trip_items.first
    trip_items = (1..5).map do |_|
      FactoryBot.create(:trip_item, trip: trip)
    end
    gen_address_distances(trip)
    trip.reload.fix_sequences

    ti_attributes = (1..5).map do |i|
      ti = trip_items[i - 1]
      {
        id: ti.id,
        sequence: ti.id # intentional
      }
    end

    trip_attributes = { trip: {
      id: trip.id,
      start_time: '17:55',
      trip_items_attributes: ti_attributes
    } }
    patch trips_path(trip_attributes)
    trip.reload

    expect(trip.start_time.hour).to eq(17)
    expect(trip.start_time.min).to  eq(55)
    trip_items.each do |ti|
      ti.reload
      expect(ti.sequence).to eq(ti.id)
    end
    expect(base_ti.sequence).to eq(0)
  end
end
