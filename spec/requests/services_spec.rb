require 'spec_helper'

describe 'Service pages', type: :request do
  before do
    Rails.application.load_seed
    log_in
  end

  let(:category) { Category.create(name: 'category') }

  describe 'Create' do
    it 'Should work normally' do
      expect do
        post services_path(service: { name: 'new_service',
                                      category_id: category.id,
                                      price: 1.0,
                                      active: true })
      end.to change(Service, :count).by(1)
    end

    it 'Should not create with system as category' do
      expect do
        post services_path(service: { name: 'new_service',
                                      category_id: Category.system.id,
                                      price: 1.0,
                                      active: true })
      end.not_to change(Service, :count)
    end
  end

  describe 'Edit' do
    let(:service) { FactoryBot.create(:service, category: category) }

    it 'Should work normally' do
      c2 = Category.create(name: 'c2')
      patch service_path(service, service: { name: 'edited_name',
                                             category_id: c2.id })
      service.reload
      expect(service.category).to eq(c2)
    end

    it 'Should not work with system as category' do
      suppress(Exception) do
        patch service_path(service, service: {
                             name: 'edited_name',
                             category_id: Category.system.id
                           })
      end

      service.reload
      expect(service.category).to eq(category)
    end

    it 'Should not work for system services' do
      svc = Service.ironing_per_hour

      suppress(Exception) do
        patch service_path(svc, service: { name: 'Tarot reading per hour' })
      end
      svc.reload

      expect(svc.name).to eq('Ironing per hour')
    end
  end

  describe 'Delete' do
    let!(:service) do
      s = Service.create name: 'service',
                         category: category,
                         price: 1,
                         active: true
      FactoryBot.create(:booking_service, service: s)
      s
    end

    let!(:service_2) do
      Service.create name: 'service_2',
                     category: category,
                     price: 2,
                     active: true
    end

    it 'works correctly' do
      expect do
        delete service_path(service_2)
      end.to change(Service, :count).by(-1)
    end

    it 'correctly refuses to work' do
      expect do
        suppress(Exception) do
          delete service_path(service)
        end
      end.not_to change(Service, :count)

      expect do
        suppress(Exception) do
          delete service_path(Service.minimum_charge)
        end
      end.not_to change(Service, :count)
    end
  end
end
