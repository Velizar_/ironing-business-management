require 'spec_helper'

describe 'Driver location URLs', type: :request do
  let(:driver_username) { 'driver username' }
  let(:driver_password) { '123654' }

  before do
    staff = FactoryBot.create(:staff,
                               username: driver_username,
                               password: driver_password)
    FactoryBot.create(:driver_account, staff: staff)
    Rails.application.load_seed # for System address and services/category
  end

  scenario 'Receive writes the values to the DB' do
    # sanity check
    expect(DriverLocation.count).to eq(0)

    # create trip item with trip and address
    trip_item = FactoryBot.create(:trip_item)

    lat = '51.4175'
    long = '0.0218'
    expect(MapsWrapper).to receive(:distance).and_return 587
    post(driver_api_receive_path(trip_id: trip_item.trip_id,
                                 trip_item_id: trip_item.id,
                                 lat: lat,
                                 long: long,
                                 username: driver_username,
                                 password: driver_password))

    expect(DriverLocation.count).to eq(1)
    dl = DriverLocation.first

    expect(dl.trip_id).to          eq(trip_item.trip_id)
    expect(dl.trip_item_id).to     eq(trip_item.id)
    expect(dl.lat).to              eq(lat)
    expect(dl.long).to             eq(long)
    expect(dl.distance_seconds).to eq(587)
  end

  scenario 'get_daily_trip reads from the DB', type: :request do
    trip = Trip.today # create
    trip.update(start_time: '18:15',
                driver_account:
                  Staff.find_by(username: driver_username).driver_account)

    ### 1
    customer1 = FactoryBot.create(:addressless_customer,
                                   nickname: 'Nickname 1',
                                   active: true,
                                   payment_scheme: :flat_rate)

    address1 = FactoryBot.create(:address,
                                  geocode: '32,14.3',
                                  address_name: 'name',
                                  post_code: 'pc',
                                  customer: customer1)

    trip_item1 = FactoryBot.create(:trip_item,
                                    trip: trip,
                                    sequence: 1,
                                    address: address1)

    booking1 = FactoryBot.create(:ironed_booking,
                                  customer: customer1,
                                  delivery_trip_item: trip_item1)

    ironing1 = booking1.ironing
    ironing1.update(bag_count: 2,
                    has_hangers: true,
                    date_ironed: Time.zone.today - 1)

    ironing1.booking_services.create(service: Service.ironing_per_hour,
                                     count: 2)

    ### 2
    customer2 = FactoryBot.create(:addressless_customer,
                                   nickname: 'Nickname 2',
                                   active: true,
                                   payment_scheme: :itemized)

    address2 = FactoryBot.create(:address,
                                  geocode: '42.2,-14.3',
                                  address_name: 'name2',
                                  post_code: 'pc2',
                                  customer: customer2)

    trip_item2 = FactoryBot.create(:trip_item,
                                    trip: trip,
                                    sequence: 2,
                                    address: address2)

    booking2 = FactoryBot.create(:ironed_booking,
                                  customer: customer2,
                                  delivery_trip_item: trip_item2)

    ironing2 = booking2.ironing
    ironing2.update(bag_count: 0,
                    has_hangers: true,
                    date_ironed: Time.zone.today - 1)

    ironing2.booking_services.create(service: Service.extra_charge,
                                     count: 5)

    ### 3
    customer3 = FactoryBot.create(:addressless_customer,
                                   nickname: 'Nickname 3',
                                   active: true)

    address3 = FactoryBot.create(:address,
                                  geocode: '12,34',
                                  address_name: 'name3',
                                  post_code: 'pc3',
                                  customer: customer3)

    trip_item3 = FactoryBot.create(:trip_item,
                                    trip: trip,
                                    sequence: 3,
                                    address: address3,
                                    notes: 'Beware of cat')

    _booking3 = FactoryBot.create(:booked_booking,
                                   customer: customer3,
                                   collection_trip_item: trip_item3)

    gen_address_distances(trip, 120)

    get driver_api_get_daily_trip_path(:format => :json,
        username: driver_username, password: driver_password)

    json = JSON.parse(response.body)
    ti = json['tripItems']

    expect(json['id']).to             eq(trip.id)

    expect(ti[0]['id']).to            eq(trip_item1.id)
    expect(ti[0]['geocode']).to       eq('32,14.3')
    expect(ti[0]['customerName']).to  eq('Nickname 1')
    expect(ti[0]['fullAddress']).to   eq('name, pc')
    expect(ti[0]['totalCost']).to     eq(2 * Service.ironing_per_hour.price)
    expect(ti[0]['numberOfBags']).to  eq(2)
    expect(ti[0]['hasHangers']).to    eq(true)
    expect(ti[0]['visitAt']).to       eq('18:17')
    expect(ti[0]['notes']).to         eq(nil)

    expect(ti[1]['id']).to            eq(trip_item2.id)
    expect(ti[1]['geocode']).to       eq('42.2,-14.3')
    expect(ti[1]['customerName']).to  eq('Nickname 2')
    expect(ti[1]['fullAddress']).to   eq('name2, pc2')
    expect(ti[1]['totalCost']).to     eq(5)
    expect(ti[1]['numberOfBags']).to  eq(0)
    expect(ti[1]['hasHangers']).to    eq(true)
    expect(ti[1]['visitAt']).to       eq('18:24')
    expect(ti[1]['notes']).to         eq(nil)

    expect(ti[2]['id']).to            eq(trip_item3.id)
    expect(ti[2]['geocode']).to       eq('12,34')
    expect(ti[2]['customerName']).to  eq('Nickname 3')
    expect(ti[2]['fullAddress']).to   eq('name3, pc3')
    expect(ti[2]['totalCost']).to     eq(0)
    expect(ti[2]['numberOfBags']).to  eq(0)
    expect(ti[2]['hasHangers']).to    eq(false)
    expect(ti[2]['visitAt']).to       eq('18:31')
    expect(ti[2]['notes']).to         eq('Beware of cat')
  end
end
