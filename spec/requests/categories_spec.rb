require 'spec_helper'

describe 'Category pages', type: :request do
  before { log_in }

  let!(:system_category) do
    c = Category.create name: 'System'
    c.services.create(name: 'Ironing per hour', price: 10)
    c
  end

  let!(:category) do
    c = Category.create name: 'spec_category'
    c.services.create(name: 'service 1', price: 1)
    c
  end

  let!(:category2) { Category.create name: 'spec_category_empty' }

  describe 'delete' do
    it 'works correctly' do
      expect do
        delete category_path(category2)
      end.to change(Category, :count).by(-1)
    end

    it 'correctly refuses to work' do
      expect do
        delete category_path(category)
      end.not_to change(Category, :count)

      expect do
        delete category_path(system_category)
      end.not_to change(Category, :count)
    end
  end

  describe 'rename' do
    it 'works correctly' do
      new_name = 'new name'
      patch category_path(category, category: { name: new_name })
      category.reload

      expect(category.name).to eq(new_name)
    end

    it 'correctly refuses to work' do
      suppress(Exception) do
        patch category_path(system_category, category: { name: 'something' })
      end
      system_category.reload

      expect(system_category.name).to eq('System')
    end
  end
end
