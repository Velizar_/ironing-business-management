FactoryBot.define do
  factory :ironing_task do
    ironer_account
    minutes_ironed { 60 }
    ironing
  end
end
