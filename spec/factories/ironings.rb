FactoryBot.define do
  factory :ironing do
    bag_count { nil }
    has_hangers { nil }
    date_ironed { nil }
    booking
    # no booking services, total cost 0

    factory :blank_ironing do
    end
  end

  factory :blank_completed_ironing, class: Ironing do
    bag_count { 1 }
    has_hangers { true }
    date_ironed { Time.zone.today }
    booking
    # no booking services, total cost 0

    after(:create) do |i|
      create_list(:ironing_task, 1, ironing: i) if i.ironing_tasks.empty?
    end

    factory :itemized_ironing do
      booking { nil }

      after(:build) do |i|
        c = FactoryBot.create(:itemized_scheme_customer)
        i.booking = FactoryBot.build(:booking, customer: c)
      end

      after(:create) do |i|
        svc = FactoryBot.create(:service)
        i.booking_services.create(
          [{ service: svc,                         count: 2, discount: 0.0 },
           { service: Service.washing_per_laundry, count: 3, discount: 0.0 }]
        )
      end
    end

    factory :flat_rate_ironing do
      booking { nil }

      after(:build) do |i|
        c = FactoryBot.create(:flat_rate_customer)
        i.booking = FactoryBot.build(:booking, customer: c)
      end

      after(:create) do |i|
        i.booking_services.create(
          [{ service: Service.ironing_per_hour,    count: 0.25, discount: 0.0 },
           { service: Service.washing_per_laundry, count: 3,    discount: 0.0 },
           { service: Service.extra_charge,        count: -1,   discount: 0.0 }]
        )
      end
    end
  end
end
