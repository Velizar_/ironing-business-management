FactoryBot.define do
  factory :booking do
    customer
    association :collection_trip_item,
                factory: :pending_trip_item,
                strategy: :build
    association :delivery_trip_item,
                factory: :pending_trip_item,
                strategy: :build
    discount { 0.0 }

    after(:build) do |b|
      b.collection_trip_item.address = b.customer.addresses[0]
      b.delivery_trip_item&.address = b.customer.addresses[0]
    end

    factory :booked_booking do # alias
    end

    factory :collected_booking do
      association :collection_trip_item,
                  factory: :completed_trip_item,
                  strategy: :build

      factory :ironed_booking do
        after(:build) do |b, _|
          create_list(:blank_completed_ironing, 1, booking: b)
        end

        factory :delivered_booking do
          association :delivery_trip_item,
                      factory: :completed_trip_item,
                      strategy: :build
        end
      end
    end
  end
end
