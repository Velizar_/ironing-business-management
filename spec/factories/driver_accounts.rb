FactoryBot.define do
  factory :driver_account do
    pay_rate { 1.5 }
    active { true }
    association :staff, strategy: :build
  end
end
