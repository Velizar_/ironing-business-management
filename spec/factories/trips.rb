FactoryBot.define do
  factory :trip do
    date { Time.zone.today + 1 }
    start_time { '18:07' }
    sched_deadline { '17:01' }
    completed { false }

    factory :completed_trip do
      completed { true }
    end
  end
end
