FactoryBot.define do
  factory :address do
    sequence(:address_name) { |n| "Factory Address #{n} name" }
    sequence(:post_code) { |n| "Factory Post code #{n}" }
    geocode { '51.4158321,0.0240333' }
    association :customer, strategy: :build
    active { true }
  end

  factory :blank_address, class: Address do
    post_code {  '.' }
    geocode {  '.' }
    customer
    active {  true }
  end
end
