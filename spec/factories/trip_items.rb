FactoryBot.define do
  factory :trip_item do
    add_attribute(:sequence) { 1 }
    association :address, strategy: :build
    trip_date { Time.zone.today + 7 }

    after(:build) do |ti, _|
      # trip overrides trip_date
      # this way, if only one of them is specified, it is used
      ti.trip_date = nil if ti.trip.present?
    end

    factory :pending_trip_item do
    end

    factory :completed_trip_item do
      trip_date { nil }
      association :trip, factory: :completed_trip, strategy: :build
    end

    factory :trip_item_with_booking do
      after(:create) do |ti, _|
        FactoryBot.create(:booked_booking,
                           customer: ti.address.customer,
                           collection_trip_item: ti,
                           delivery_trip_item: nil)
      end
    end
  end
end
