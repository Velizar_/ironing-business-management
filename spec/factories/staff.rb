FactoryBot.define do
  factory :staff, class: Staff do
    sequence(:username) { |n| "Staff #{n}" }
    sequence(:email) { |n| "staff_#{n}@example.com" }
    password { '123456' }
    password_confirmation { '123456' }
    permissions { 0 }
    first_name { 'fname' }
    last_name { 'lname' }
    active { true }
    before(:create) do |staff|
      staff.password_confirmation = staff.password
    end

    factory :owner do
      sequence(:username) { |n| "Owner #{n}" }
      sequence(:email) { |n| "owner_#{n}@example.com" }
      permissions { 1 }
    end
  end
end
