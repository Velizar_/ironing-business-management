FactoryBot.define do
  factory :addressless_customer, class: Customer do
    sequence(:nickname) { |n| "Customer #{n}" }
    sequence(:email) { |n| "customer_#{n}@example.com" }
    payment_scheme { :itemized }
    active { true }
    discount { 0.0 }

    factory :customer do
      after(:create) do |c, _|
        create_list(:blank_address, 1, customer: c)
      end
    end

    factory :itemized_scheme_customer do
    end

    factory :flat_rate_customer do
      payment_scheme { :flat_rate }
    end
  end
end
