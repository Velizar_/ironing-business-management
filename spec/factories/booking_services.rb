FactoryBot.define do
  factory :booking_service do
    count { 1.5 }
    ironing
    service
  end
end
