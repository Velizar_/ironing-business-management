FactoryBot.define do
  factory :service do
    sequence(:name) { |n| "Service #{n}" }
    price { 1.0 }
    active { true }
    category # non-system
  end
end
