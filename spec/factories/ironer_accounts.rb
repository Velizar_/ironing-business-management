FactoryBot.define do
  factory :ironer_account do
    pay_rate { 1.0 }
    active { true }
    staff
  end
end
