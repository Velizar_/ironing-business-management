class PhoneAuthStrategy < Warden::Strategies::Base
  def valid?
    params&.is_a?(Hash) &&
      params.dig(:customer, :phone).present? &&
      params.dig(:customer, :password).present?
  end

  def authenticate!
    customer = Customer.find_by_phone_number(phone)
    if customer&.valid_password?(password)
      customer.update(first_login: DateTime.now) if customer.first_login.nil?
      success!(customer)
    else
      fail!('Invalid password or nonexistent phone number')
    end
  end

  def phone
    params['customer']['phone']
  end

  def password
    params['customer']['password']
  end
end
