require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_view/railtie"
require "active_job/railtie"
require "rails/test_unit/railtie"
require "action_mailer/railtie" # Required by Devise, not used in our app
# require "active_storage/engine"
# require "action_mailbox/engine"
# require "action_text/engine"
# require "action_cable/engine"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module IroningBusiness
  class Application < Rails::Application
    # Initialize configuration defaults for Rails version.
    config.load_defaults 7.0

    config.active_job.queue_adapter = :good_job
    config.good_job.execution_mode = :async
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
