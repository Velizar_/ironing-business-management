Rails.application.routes.draw do

  resources :customer_registrations, only: [:index, :destroy]

  get 'notifications/' => 'notifications#index'

  post 'push_notification_address' => 'push_notification_address#create'

  put 'requests/save_booking_availability' => 'requests#save_booking_availability', as: :booking_availabilities
  get 'requests/add_booking_availability' => 'requests#add_booking_availability', as: :add_availability

  get 'requests/' => 'requests#index'
  put 'requests/toggle_active/:id' => 'requests#toggle_active', as: :toggle_active_request

  delete 'customer_app/request/:id' => 'customer_app#delete_request'
  delete 'customer_app/booking/:id' => 'customer_app#delete_booking'
  post 'customer_app/register'
  post 'customer_app/request_code'
  get 'customer_app/check_code'
  post 'customer_app/complete_account'
  post 'customer_app/request' => 'customer_app#create_request'
  get 'customer_app/booking_status'
  get 'customer_app/booking_availability'
  post 'customer_app/register_push_notification'

  get 'customer_debts/index'
  get 'customer_debts/increase'
  post 'customer_debts/increase_debt'
  get 'customer_debts/decrease/:id' => 'customer_debts#decrease', as: :customer_debts_decrease
  post 'customer_debts/decrease_debt/:id' => 'customer_debts#decrease_debt', as: :decrease_debt

  resources :expenses, except: [:show]
  resources :expense_types, except: [:show]
  get 'expenses/new_periodic' => 'expenses#new_periodic', as: :new_periodic_expense
  get 'expenses/edit_periodic/:id' => 'expenses#edit_periodic', as: :edit_periodic_expense

  get 'statistics/' => 'statistics#index'
  get 'statistics/general'
  get 'statistics/customers'
  get 'statistics/finance'

  resources :trips, only: [:index, :edit, :destroy]
  patch 'trips/:id/update_active_base' => 'trips#update_active_base', as: :update_active_base
  delete 'trips/wipe_distances/:id' => 'trips#wipe_distances', as: :wipe_distances
  post 'trips/create/:date' => 'trips#create', as: :create_trip
  patch 'trips/move_item/:id/:trip_id' => 'trips#move_item', as: :trip_move_item
  patch 'trips' => 'trips#update', as: :update_trip
  get 'trips/printable/:id' => 'trips#printable', as: :printable_trip
  get 'trips/status/:id' => 'trips#status', as: :trip_status
  post 'trips/notify_customers' => 'trips#notify_customers'

  get 'ironing/past' => 'ironing#past', as: :past_ironing
  resources :ironing, except: [:destroy, :update, :new]
  get 'ironing/new/:id' => 'ironing#new', as: :new_ironing
  get 'ironing/receipt/:id' => 'ironing#receipt', as: :ironing_receipt

  resources :categories

  resources :bookings, except: [:show]
  get 'bookings/delivery/:id' => 'bookings#delivery', as: :bookings_delivery
  post 'bookings/new_delivery/:id' => 'bookings#new_delivery', as: :new_delivery
  get 'bookings/past' => 'bookings#past', as: :past_bookings

  resources :services
  get 'services/edit_price/:id' => 'services#edit_price', as: :edit_price
  post 'services/update_price/:id' => 'services#update_price', as: :update_price

  patch 'staff/toggle_active/:id/:account_type' => 'staff#toggle_active', as: :toggle_active_account
  devise_for :staff
  resources :staff, except: [:destroy]
  get 'staff/appoint/:role/' => 'staff#appoint_index', as: :appoint_index
  post 'staff/appoint' => 'staff#appoint'
  get 'staff/edit_wage/:id/:account_type/' => 'staff#edit_wage', as: :edit_wage
  post 'staff/:id/:account_type/update_wage' => 'staff#update_wage', as: :update_wage
  get 'staff/:ironer_id/ironer_weekly_wage' => 'staff#ironer_weekly_wage', as: :ironer_weekly_wage

  root 'home#index'

  resources :settings, only: :index

  resources :addresses, except: [:destroy, :new]
  get 'customers/:id/add_address' => 'addresses#new', as: :add_address
  get 'customers/:id/edit_active_address' => 'addresses#edit_active', as: :edit_active_address
  post 'customers/:id/update_active_address' => 'addresses#update_active', as: :update_active_address

  post 'customers/:id/create_code' => 'customers#create_code', as: :create_code
  patch 'customers/update_customers_per_page' => 'customers#update_customers_per_page', as: :update_customers_per_page
  get 'customers/check_unique' => 'customers#check_unique', as: :check_unique
  get 'customers/inactive' => 'customers#inactive', as: :inactive_customers
  devise_for :customers, only: :sessions
  resources :customers

  post 'driver_api/upload_trip_status'
  post 'driver_api/receive'
  get 'driver_api/get_daily_trip'
end
